<?php
/**
 * Ce script définit la classe 'courrier'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../gen/obj/courrier.class.php";

/**
 * Définition de la classe 'courrier' (om_dbform).
 */
class courrier extends courrier_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        courrier_gen::init_class_actions();
        // ACTION - 001 - modifier
        // On ne peut pas modifier le courrier si il est finalisé
        // sinon il va être possible de désynchroniser le champ corps
        // affiché à l'écran et le fichier pdf téléchargeable
        $this->class_actions[1]["condition"][] = "is_not_finalized";
        // ACTION - 004 - finalise
        // Finalise le courrier : stockage
        $this->class_actions[4] = array(
            "identifier" => "finalise",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("finaliser"),
                "order" => 100,
                "class" => "finalize-16",
            ),
            "view" => "formulaire",
            "method" => "finalize",
            "button" => "finalise",
            "permission_suffix" => "finalise",
            "condition" => "is_not_finalized",
        );
        // ACTION - 005 - definalise
        // Définalise le courrier
        $this->class_actions[5] = array(
            "identifier" => "definalise",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("définaliser"),
                "order" => 110,
                "class" => "unfinalize-16",
            ),
            "view" => "formulaire",
            "method" => "unfinalize",
            "button" => "definalise",
            "permission_suffix" => "definalise",
            "condition" => "is_finalized",
        );
        // ACTION - 006 - edition pdf
        // Retourne le pdf soit celui généré soit celui stocké
        $this->class_actions[6] = array(
            "identifier" => "edition pdf",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("édition pdf"),
                "order" => 120,
                "class" => "pdf-16",
            ),
            "view" => "view_edition",
            "permission_suffix" => "fichier_telecharger",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_lettre_type() {
        return "SELECT om_lettretype.id, (om_lettretype.id||' '||om_lettretype.libelle) as libelle FROM ".DB_PREFIXE."om_lettretype WHERE om_lettretype.actif is TRUE ORDER BY id";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_lettre_type_by_id() {
        return "SELECT om_lettretype.id, (om_lettretype.id||' '||om_lettretype.libelle) as libelle FROM ".DB_PREFIXE."om_lettretype WHERE om_lettretype.id='<idx>'";
    }

    /**
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $crud = $this->get_action_crud($maj);
        //
        $form->setType("modele_lettre_type", "hiddenstatic");
        $form->setType("fichier_finalise", "hidden");
        $form->setType("fichier", "hidden");
        //
        if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
            $form->setType("etablissement", "hidden");
        }
        // CREATE
        if ($crud == "create" || $crud == "update") {
            $form->setType("modele_lettre_type", "select");
        }
    }

    /**
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "modele_lettre_type",
            $this->get_var_sql_forminc__sql("modele_lettre_type"),
            $this->get_var_sql_forminc__sql("modele_lettre_type_by_id"),
            false
        );
    }

    /**
     * VIEW - view_edition
     *
     * @return void
     */
    function view_edition() {
        $this->checkAccessibility();
        //
        if ($this->getVal("fichier_finalise") == "t") {
            $redirect = sprintf(
                "%s&snippet=file&obj=courrier&champ=fichier&id=%s",
                OM_ROUTE_FORM,
                $this->getVal($this->clePrimaire)
            );
            header("location:".$redirect);
            return;
        }
        // Génération du PDF
        $pdfedition = $this->compute_pdf_output(
            "lettretype",
            $this->getVal("modele_lettre_type"),
            null,
            $this->getVal($this->clePrimaire),
            array(
                "watermark" => true, 
                "specific" => array(
                    "mode" => "previsualisation",
                ),
            )
        );
        // Affichage du PDF
        $this->expose_pdf_output(
            $pdfedition['pdf_output'], 
            $pdfedition['filename']
        );
    }

    /**
     * TREATMENT - finalize.
     * 
     * Permet de finaliser un enregistrement
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function finalize($val = array()) {
        $this->begin_treatment(__METHOD__);
        $ret = $this->manage_finalizing("finalize", $val);
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - unfinalize.
     * 
     * Permet de definaliser un enregistrement
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function unfinalize($val = array()) {
        $this->begin_treatment(__METHOD__);
        $ret = $this->manage_finalizing("unfinalize", $val);
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Effectue le traitement de finalisation et définalisation.
     *
     * @param string $mode finalize/unfinalize
     * @param array  $val  valeurs du formulaire
     *
     * @return boolean
     */
    function manage_finalizing($mode = null, $val = array()) {
        // Logger
        $this->addToLog("manage_finalizing() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "finalize" && $mode != "unfinalize") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "finalize") {
            // Valeurs à modifier
            $valF = array(
                "fichier_finalise" => true,
            );
            //
            $valid_message = __("Finalisation correctement effectuée.");
            // Génération du PDF
            $pdfedition = $this->compute_pdf_output(
                "lettretype",
                $this->getVal("modele_lettre_type"),
                null,
                $this->getVal($this->clePrimaire)
            );
            // Composition des métadonnées du document.
            $metadata = array(
                    'filename' => $pdfedition["filename"],
                    'mimetype' => 'application/pdf',
                    'size' => strlen($pdfedition["pdf_output"]),
            );
            if ($this->getVal("fichier_finalise") != "f") {
                $uid = $this->f->storage->update(
                    $this->getVal("fichier"),
                    $pdfedition["pdf_output"],
                    $metadata
                );
                if ($uid == OP_FAILURE) {
                    $this->addToLog(
                        __METHOD__ . "(): Erreur lors de la mise à jour du document sur le système de stockage des fichiers [courrier.fichier]",
                        DEBUG_MODE
                    );
                    $this->correct = false;
                    $this->cleanMessage();
                    $this->addToMessage(__("Une erreur s'est produite lors de la finalisation. Contactez votre administrateur."));
                    return $this->end_treatment(__METHOD__, false);
                }
            } else { // Sinon, ajoute le document et récupère son uid
                $uid = $this->f->storage->create(
                    $pdfedition["pdf_output"],
                    $metadata
                );
                if ($uid == OP_FAILURE) {
                    $this->addToLog(
                        __METHOD__ . "(): Erreur lors de la création du document sur le système de stockage des fichiers [courrier.fichier]",
                        DEBUG_MODE
                    );
                    $this->correct = false;
                    $this->cleanMessage();
                    $this->addToMessage(__("Une erreur s'est produite lors de la finalisation. Contactez votre administrateur."));
                    return $this->end_treatment(__METHOD__, false);
                }
                $valF["fichier"] = $uid;
            }
        } elseif ($mode == "unfinalize") {
            // Valeurs à modifier
            $valF = array(
                "fichier_finalise" => false,
            );
            //
            $valid_message = __("Définalisation correctement effectuée.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(__("Requête exécutée"), VERBOSE_MODE);
            // Log
            $message = __("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= __("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= __("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(
                    __("Attention vous n'avez fait aucune modification.")
                );
            } else {
                $this->addToMessage($valid_message);
            }
        }
        // Logger
        $this->addToLog("manage_finalizing() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * CONDITION - is_finalized
     *
     * Condition pour afficher le bouton de définalisation.
     *
     * @return boolean
     */
    function is_finalized() {
        if ($this->getVal("fichier_finalise") == "t") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_finalized
     *
     * Condition pour afficher le bouton de finalisation.
     *
     * @return boolean
     */
    function is_not_finalized() {
        return !$this->is_finalized();
    }

    /**
     * @return string
     */
    function getSubFormTitle($ent) {
        if ($this->getParameter("maj") == 0) {
            return "-> ".__("courriers");
        }
        return "-> ".__("courriers")." -> ".$this->get_default_libelle();
    }

    /**
     * OVERLOAD - om_dbform->get_values_merge_fields
     *
     * Récupération des valeurs des champs de fusion
     *
     * @return array         tableau associatif
     */
    function get_values_merge_fields() {
        // récupération de la table de la classe instanciée
        $table = $this->table;
        $classe = get_class($this);
        // récupération des clés étrangères
        $foreign_keys = array();
        foreach ($this->foreign_keys_extended as $foreign_key => $values) {
            $foreign_keys[] = $foreign_key;
        }
        // initialisation du tableau de valeurs
        $values = array();
        // pour chaque champ de l'objet on crée un champ de fusion
        foreach ($this->champs as $key => $champ) {
            //
            if (in_array($champ, $this->get_merge_fields_to_avoid())) {
                continue;
            }
            // récupération de la valeur
            $value = $this->getVal($champ);
            // si c'est un booléen on remplace par oui/non
            if ($this->type[$key] == 'bool') {
                switch ($value) {
                    case 't':
                    case 'true':
                    case 1:
                        $value = __("oui");
                        break;
                    case 'f':
                    case 'false':
                    case 0:
                        $value = __("non");
                        break;
                }
            }
            // si c'est une date anglosaxonne on la formate en FR
            if (DateTime::createFromFormat('Y-m-d', $value) !== FALSE) {
                $dateFormat = new DateTime($value);
                $value = $dateFormat->format('d/m/Y');
            }
            // si c'est une clé étrangère avec une valeur valide
            // on remplace par le libellé
            if (in_array($champ, $foreign_keys)
                && $value != null && $value != '') {
                // construction variable sql
                $var_sql = $champ."_by_id";
                // si la variable existe
                $sql = $this->get_var_sql_forminc__sql($var_sql);
                if ($sql != "") {
                    // remplacement de l'id par sa valeur dans la condition
                    $sql = str_replace('<idx>', $value, $sql);
                    // exécution requete
                    $res = $this->f->db->query($sql);
                    $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
                    // Si la récupération de la description de l'avis échoue
                    if ($this->f->isDatabaseError($res, true)) {
                        // Appel de la methode de recuperation des erreurs
                        $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                        $this->correct = false;
                        return false;
                    }
                    $row = &$res->fetchRow();
                    // récupération libellé
                    $value = $row[1];
                }
            }
            $values[$table.".".$champ] = $value;

            // OVERLOAD
            // Permet d'ajouter un champ de fusion qui supprime les balises HTML
            // du champ corps du courrier
            if ($champ === 'corps_om_html') {
                $values[$table.".".'corps_texte'] = strip_tags($value);
            }
        }
        return $values;
    }

    /**
     * Récupération des libellés des champs de fusion
     *
     * @return array         tableau associatif
     */
    function get_labels_merge_fields() {
        // récupération de la table de la classe instanciée
        $table = $this->table;
        // récupération du nom de la clé primaire
        $clePrimaire = __($this->clePrimaire);
        // initialisation du tableau de libellés
        $labels = array();
        // pour chaque champ de l'objet on crée un champ de fusion
        foreach ($this->champs as $key => $champ) {
            //
            if (in_array($champ, $this->get_merge_fields_to_avoid())) {
                continue;
            }
            //
            $labels[$clePrimaire][$table.".".$champ] = __($champ);

            // OVERLOAD
            // Permet d'ajouter un champ de fusion qui supprime les balises HTML
            // du champ corps du courrier
            if ($champ === 'corps_om_html') {
                $labels[$clePrimaire][$table.".".'corps_texte'] = __($champ).' '.__("sans style d'affichage");
            }
        }
        return $labels;
    }
}
