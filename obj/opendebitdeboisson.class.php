<?php
/**
 * Ce script définit la classe 'opendebitdeboisson'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

/**
 *
 */
if (file_exists("../dyn/locales.inc.php") === true) {
    require_once "../dyn/locales.inc.php";
}

/**
 * Définition de la constante représentant le chemin d'accès au framework
 */
define("PATH_OPENMAIRIE", getcwd()."/../core/");

/**
 * TCPDF specific config
 */
define("K_TCPDF_EXTERNAL_CONFIG", true);
define("K_TCPDF_CALLS_IN_HTML", true);

/**
 * Dépendances PHP du framework
 * On modifie la valeur de la directive de configuration include_path en
 * fonction pour y ajouter les chemins vers les librairies dont le framework
 * dépend.
 */
set_include_path(
    get_include_path().PATH_SEPARATOR.implode(
        PATH_SEPARATOR,
        array(
            getcwd()."/../php/pear",
            getcwd()."/../php/db",
            getcwd()."/../php/fpdf",
            getcwd()."/../php/phpmailer",
            getcwd()."/../php/tcpdf",
        )
    )
);

/**
 *
 */
if (file_exists("../dyn/debug.inc.php") === true) {
    require_once "../dyn/debug.inc.php";
}

/**
 *
 */
require_once PATH_OPENMAIRIE."om_application.class.php";

/**
 * Surcharges du framework à réintégrer
 */
require_once "../obj/om_application_override.class.php";

/**
 * Définition de la classe 'opendebitdeboisson' (om_application).
 */
class opendebitdeboisson extends om_application_override {

    /**
     * Gestion du nom de l'application.
     *
     * @var mixed Configuration niveau application.
     */
    protected $_application_name = "openDébitDeBoisson";

    /**
     * Titre HTML.
     *
     * @var mixed Configuration niveau application.
     */
    protected $html_head_title = ":: openMairie :: openDébitDeBoisson";

    /**
     * Gestion du mode de gestion des permissions.
     *
     * @var mixed Configuration niveau framework.
     */
    protected $config__permission_by_hierarchical_profile = false;

    /**
     *
     * @return void
     */
    function setDefaultValues() {
        $this->addHTMLHeadCss(
            array(
                "../lib/om-theme/jquery-ui-theme/jquery-ui.custom.css",
                "../lib/om-theme/om.css",
            ),
            21
        );
        $this->addHTMLHeadJs(
            array(
                "../app/lib/Chart.bundle.min.js",
            ),
            11
        );
    }

    /**
     * Surcharge - getParameter().
     *
     * Force les paramètres suivants aux valeurs suivantes :
     * - option_localisation = "sig_interne"
     * - is_settings_view_enabled = true
     *
     * @return mixed
     */
    function getParameter($param = null) {
        // openDébitDeBoisson est fait pour fonctionner seulement avec
        // l'option_localisation à sig_interne. On force donc le paramètre.
        if ($param == "option_localisation") {
            return "sig_interne";
        }
        // openDébitDeBoisson est fait pour fonctionner avec la view 'settings'
        // pour gérer le menu 'Administration & Paramétrage'. On force donc le
        // paramètre.
        if ($param == "is_settings_view_enabled") {
            return true;
        }
        return parent::getParameter($param);
    }

    /**
     * Accesseur à des paramètres métier.
     *
     * Permet d'accéder rapidement à des paramètres métier.
     *
     * Exemple pour récupérer l'identifiant du type de demande correspondant au type
     * du DEMANDE D'OUVERTURE :
     * ->get_param_metier("type_demande", "OUV", "id_from_code");
     * 
     * @return mixed
     */
    function get_param_metier($keyword = null, $value = null, $mode = null) {
        //
        if ($keyword === "type_demande") {
            if ($mode === "id_from_code") {
                $query = sprintf(
                    'SELECT type_demande from %1$stype_demande WHERE code=\'%2$s\'',
                    DB_PREFIXE,
                    $this->db->escapeSimple($value)
                );
                $id_from_code = $this->db->getone($query);
                $this->addToLog(__METHOD__."(): db->getone(\"".$query."\");", VERBOSE_MODE);
                $this->isDatabaseError($id_from_code);
                return $id_from_code;
            }
            return null;
        }
        return null;
    }

    /**
     * Surcharge - set_config__footer().
     *
     * @return void
     */
    protected function set_config__footer() {
        $footer = array();
        // Documentation du site
        $footer[] = array(
            "title" => __("Documentation"),
            "description" => __("Accéder à l'espace documentation de l'application"),
            "href" => "http://docs.openmairie.org/?project=opendebitdeboisson&version=2.2",
            "target" => "_blank",
            "class" => "footer-documentation",
        );

        // Forum openMairie
        $footer[] = array(
            "title" => __("Forum"),
            "description" => __("Espace d'échange ouvert du projet openMairie"),
            "href" => "https://communaute.openmairie.org/c/autres-applications-openmairie",
            "target" => "_blank",
            "class" => "footer-forum",
        );

        // Portail openMairie
        $footer[] = array(
            "title" => __("openMairie.org"),
            "description" => __("Site officiel du projet openMairie"),
            "href" => "http://www.openmairie.org/catalogue/opendebitdeboisson",
            "target" => "_blank",
            "class" => "footer-openmairie",
        );
        //
        $this->config__footer = $footer;
    }

    /**
     * Surcharge - set_config__menu().
     *
     * @return void
     */
    protected function set_config__menu() {
        parent::set_config__menu();
        $parent_menu = $this->config__menu;
        //
        $menu = array();
        $rubrik = array(
            "title" => __("Application"),
            "class" => "application",
            "right" => "menu_application",
        );
        //
        $links = array();
        $links[] = array(
            "href" => OM_ROUTE_DASHBOARD,
            "class" => "dashboard",
            "title" => __("Tableau de bord"),
            "open" => array(
                "index.php|[module=dashboard]",
                "index.php|[module=map][mode=tab_sig][obj=carte_globale]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_permanent",
            "class" => "etablissement_permanent",
            "title" => __("établissements"),
            "right" => array(
                "etablissement_permanent",
                "etablissement_permanent_tab",
            ),
            "open" => array(
                "index.php|etablissement_permanent[module=tab]",
                "index.php|etablissement_permanent[module=form]",
                "index.php|[module=map][mode=tab_sig][obj=etablissement]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_temporaire",
            "class" => "etablissement_temporaire",
            "title" => __("établissements temporaires"),
            "right" => array(
                "etablissement_temporaire",
                "etablissement_temporaire_tab",
            ),
            "open" => array(
                "index.php|etablissement_temporaire[module=tab]",
                "index.php|etablissement_temporaire[module=form]",
                "index.php|[module=map][mode=tab_sig][obj=etablissement]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=perimetre",
            "class" => "perimetre",
            "title" => __("périmètres d'exclusion"),
            "right" => array(
                "perimetre",
                "perimetre_tab",
            ),
            "open" => array(
                "index.php|perimetre[module=tab]",
                "index.php|perimetre[module=form]",
                "index.php|[module=map][mode=tab_sig][obj=perimetre]",
            ),
        );
        $rubrik['links'] = $links;
        $menu[] = $rubrik;
        //
        $rubrik = array(
            "title" => __("Paramétrage Métier"),
            "class" => "parametrage",
            "right" => "menu_parametrage",
            "links" => array(),
            "parameters" => array("is_settings_view_enabled" => false, ),
        );
        $rubrik["links"][] = array(
            "class" => "category",
            "title" => __("métier"),
            "right" => array(
                "terme",
                "terme_tab",
                "type_demande",
                "type_demande_tab",
                "type_licence",
                "type_licence_tab",
            ),
        );
        $rubrik["links"][] = array(
            "href" => OM_ROUTE_TAB."&obj=terme",
            "class" => "terme",
            "title" => __("termes"),
            "description" => __("Paramétrage des termes de licence (Permanente, Temporaire, ...)."),
            "right" => array(
                "terme",
                "terme_tab",
            ),
            "open" => array(
                "index.php|terme[module=tab]",
                "index.php|terme[module=form]",
            ),
        );
        $rubrik["links"][] = array(
            "href" => OM_ROUTE_TAB."&obj=type_demande",
            "class" => "type_demande",
            "title" => __("types de demandes"),
            "description" => __("Paramétrage des types de demandes (D'ouverture, de mutation, ...)."),
            "right" => array(
                "type_demande",
                "type_demande_tab",
            ),
            "open" => array(
                "index.php|type_demande[module=tab]",
                "index.php|type_demande[module=form]",
            ),
        );
        $rubrik["links"][] = array(
            "href" => OM_ROUTE_TAB."&obj=type_licence",
            "class" => "type_licence",
            "title" => __("types de licences"),
            "description" => __("Paramétrage des types de licence (3ème catégorie, Restaurant, ...)."),
            "right" => array(
                "type_licence",
                "type_licence_tab",
            ),
            "open" => array(
                "index.php|type_licence[module=tab]",
                "index.php|type_licence[module=form]",
            ),
        );
        //
        $rubrik["links"][] = array(
            "class" => "category",
            "title" => __("géolocalisation"),
            "right" => array(
                "adresse_postale",
                "adresse_postale_tab",
                "rivoli",
                "rivoli_tab",
            ),
        );
        $rubrik["links"][] = array(
            "href" => OM_ROUTE_TAB."&obj=adresse_postale",
            "class" => "adresse_postale",
            "title" => __("adresses postales"),
            "description" => __("Toutes les adresses de la commune."),
            "right" => array(
                "adresse_postale",
                "adresse_postale_tab",
            ),
            "open" => array(
                "index.php|adresse_postale[module=tab]",
                "index.php|adresse_postale[module=form]",
            ),
        );
        $rubrik["links"][] = array(
            "href" => OM_ROUTE_TAB."&obj=rivoli",
            "class" => "rivoli",
            "title" => __("rivoli"),
            "description" => __("Tous les codes RIVOLI de la commune."),
            "right" => array(
                "rivoli",
                "rivoli_tab",
            ),
            "open" => array(
                "index.php|rivoli[module=tab]",
                "index.php|rivoli[module=form]",
            ),
        );
        $rubrik["links"][] = array(
            "class" => "category",
            "title" => __("tables de références"),
            "right" => array(
                "titre_de_civilite",
                "titre_de_civilite_tab",
                "qualite_exploitant",
                "qualite_exploitant_tab",
                "statut_demande",
                "statut_demande_tab",
                "type_etablissement",
                "type_etablissement_tab",
            ),
        );
        $rubrik["links"][] = array(
            "href" => OM_ROUTE_TAB."&obj=titre_de_civilite",
            "class" => "titre_de_civilite",
            "title" => __("titres de civilité"),
            "description" => __("Paramétrage du vocabulaire 'Titres de civilité'."),
            "right" => array(
                "titre_de_civilite",
                "titre_de_civilite_tab",
            ),
            "open" => array(
                "index.php|titre_de_civilite[module=tab]",
                "index.php|titre_de_civilite[module=form]",
            ),
        );
        $rubrik["links"][] = array(
            "href" => OM_ROUTE_TAB."&obj=qualite_exploitant",
            "class" => "qualite_exploitant",
            "title" => __("qualités de l'exploitant"),
            "description" => __("Paramétrage du vocabulaire 'Qualités de l'exploitant'."),
            "right" => array(
                "qualite_exploitant",
                "qualite_exploitant_tab",
            ),
            "open" => array(
                "index.php|qualite_exploitant[module=tab]",
                "index.php|qualite_exploitant[module=form]",
            ),
        );
        $rubrik["links"][] = array(
            "href" => OM_ROUTE_TAB."&obj=statut_demande",
            "class" => "statut_demande",
            "title" => __("statuts de la demande"),
            "description" => __("Paramétrage du vocabulaire 'Statuts de la demande'."),
            "right" => array(
                "statut_demande",
                "statut_demande_tab",
            ),
            "open" => array(
                "index.php|statut_demande[module=tab]",
                "index.php|statut_demande[module=form]",
            ),
        );
        $rubrik["links"][] = array(
            "href" => OM_ROUTE_TAB."&obj=type_etablissement",
            "class" => "type_etablissement",
            "title" => __("types d'établissements"),
            "description" => __("Paramétrage du vocabulaire 'Types d'établissements'."),
            "right" => array(
                "type_etablissement",
                "type_etablissement_tab",
            ),
            "open" => array(
                "index.php|type_etablissement[module=tab]",
                "index.php|type_etablissement[module=form]",
            ),
        );
        $menu[] = $rubrik;
        //
        $this->config__menu = array_merge(
            $menu,
            $parent_menu
        );
    }
}
