<?php
/**
 * Ce script définit la classe 'dossier'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../gen/obj/dossier.class.php";

/**
 * Définition de la classe 'dossier' (om_dbform).
 */
class dossier extends dossier_gen {

    /**
     * @return string
     */
    function getSubFormTitle($ent) {
        if ($this->getParameter("maj") == 0) {
            return "-> ".__("dossiers");
        }
        return "-> ".__("dossiers")." -> ".$this->get_default_libelle();
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $crud = $this->get_action_crud($maj);
        //
        if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
            $form->setType("etablissement", "hidden");
        }
    }
}
