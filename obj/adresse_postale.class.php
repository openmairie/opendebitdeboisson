<?php
/**
 * Ce script définit la classe 'adresse_postale'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../gen/obj/adresse_postale.class.php";

/**
 * Définition de la classe 'adresse_postale' (om_dbform).
 */
class adresse_postale extends adresse_postale_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        //
        $this->class_actions[11] = array(
            "identifier" => "view_adresses_postale_values_json",
            "view" => "view_adresses_postale_values_json",
            "permission_suffix" => "consulter",
        );
    }

    /**
     * VIEW - view_adresses_postale_values_json
     *
     * @return void
     */
    function view_adresses_postale_values_json() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // La désactivation des logs est obligatoire pour une vue JSON.
        $this->f->disableLog();

        // Récupère les valeurs de l'enregistrement
        $result = array();
        foreach ($this->champs as $key => $value) {
            $result[$value] = $this->getVal($value);
        }

        // Affiche les valeurs en JSON
        echo json_encode($result);
        return;
    }

}
