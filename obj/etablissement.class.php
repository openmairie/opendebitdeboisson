<?php
/**
 * Ce script définit la classe 'etablissement'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../gen/obj/etablissement.class.php";

/**
 * Définition de la classe 'etablissement' (om_dbform).
 */
class etablissement extends etablissement_gen {

    /**
     * @return void
     */
    function init_class_actions() {
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "etablissement",
            "raison_sociale",
            "enseigne",
            "no_siret",
            "forme_juridique",
            "nature",
            "type_etablissement",
            "permis_exploitation",
            "date_permis_exploitation",
            "permis_exploitation_nuit",
            "date_permis_exploitation_nuit",
            "date_fermeture",
            "date_liquidation",
            "'' as adresse_postale",
            "'' as saisie_manuelle",
            "numero_voie",
            "complement",
            "voie",
            "libelle_voie",
            "complement_voie",
            "cp_etablissement",
            "ville_etablissement",
            "geom",
            "civilite_exploitant",
            "nom_exploitant",
            "nom_marital_exploitant",
            "prenom_exploitant",
            "date_naissance_exploitant",
            "cp_naissance_exploitant",
            "ville_naissance_exploitant",
            "adresse1_exploitant",
            "adresse2_exploitant",
            "cp_exploitant",
            "ville_exploitant",
            "qualite_exploitant",
            "nationalite_exploitant",
            "particularite_exploitant",
            "civilite_co_exploitant",
            "nom_co_exploitant",
            "nom_marital_co_exploitant",
            "prenom_co_exploitant",
            "date_naissance_co_exploitant",
            "cp_naissance_co_exploitant",
            "ville_naissance_co_exploitant",
            "adresse1_co_exploitant",
            "adresse2_co_exploitant",
            "cp_co_exploitant",
            "ville_co_exploitant",
            "qualite_co_exploitant",
            "nationalite_co_exploitant",
            "particularite_co_exploitant",
            "'' as idem_exploitant_proprietaire",
            "civilite_proprietaire",
            "nom_proprietaire",
            "prenom_proprietaire",
            "adresse1_proprietaire",
            "adresse2_proprietaire",
            "cp_proprietaire",
            "ville_proprietaire",
            "profession_proprietaire",
            "nom_ancien_exploitant",
            "prenom_ancien_exploitant",
            "qualite_ancien_exploitant",
            "ancien_proprietaire",
            "observation",
        );
    }

    /**
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $crud = $this->get_action_crud($maj);
        //
        $form->setType("geom", "hidden");
        // CREATE
        if ($crud == 'create') {
            $form->setType("nature", "select");
        }
        // UPDATE
        if ($crud == 'update') {
            $form->setType("nature", "hiddenstatic");
        }
        // Gestion de l'adresse postale
        $form->setType("adresse_postale", "hidden");
        $form->setType("saisie_manuelle", "hidden");
        // Gestion de la récupération des informations de l'exploitant
        $form->setType("idem_exploitant_proprietaire", "hidden");
    }

    /**
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        $form->setSelect("nature", array(
            array("permanent", "temporaire", ),
            array(__("permanent"), __("temporaire"), ),
        ));
    }

    /**
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        //
        $form->setBloc("etablissement", "D", __("caractéristiques"), "etablissement-bloc-caracteristiques col_6");
        $form->setBloc("type_etablissement", "F");
        //
        $form->setBloc("permis_exploitation", "D", __("permis d'exploitation"), "etablissement-bloc-permis-d-exploitation col_4");
        $form->setBloc("date_permis_exploitation_nuit", "F");
        //
        $form->setBloc("date_fermeture", "D", __("inactivité"), "etablissement-bloc-inactivite col_4");
        $form->setBloc("date_liquidation", "F");
        //
        $form->setBloc("adresse_postale", "D", __("adresse"), "etablissement-bloc-adresse col_12");
        $form->setBloc("ville_etablissement", "F");
        //
        $form->setBloc("civilite_exploitant", "D", __("exploitant"), "etablissement-bloc-exploitant col_6");
        $form->setBloc("particularite_exploitant", "F");
        //
        $form->setBloc("civilite_co_exploitant", "D", __("co-exploitant"), "etablissement-bloc-co-exploitant col_6");
        $form->setBloc("particularite_co_exploitant", "F");
        //
        $form->setBloc("idem_exploitant_proprietaire", "D", __("propriétaire"), "etablissement-bloc-proprietaire col_6");
        $form->setBloc("profession_proprietaire", "F");
        //
        $form->setBloc("nom_ancien_exploitant", "D", __("ancien exploitant"), "etablissement-bloc-ancien-exploitant col_6");
        $form->setBloc("qualite_ancien_exploitant", "F");
        //
        $form->setBloc("ancien_proprietaire", "DF", __("ancien propriétaire"), "etablissement-bloc-ancien-proprietaire col_6");
        //
        $form->setBloc("observation", "DF", __("observation"), "etablissement-bloc-observation col_12");
    }

    /**
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("etablissement", __("id"));
        //
        $form->setLib("adresse_postale", __("adresse_postale"));
        $form->setLib("saisie_manuelle", __("saisie_manuelle"));
        $form->setLib("numero_voie", __("numéro"));
        $form->setLib("complement", __("complément de numéro"));
        $form->setLib("voie", __("code RIVOLI"));
        $form->setLib("libelle_voie", __("libellé de la voie"));
        $form->setLib("complement_voie", __("complément"));
        $form->setLib("cp_etablissement", __("code postal"));
        $form->setLib("ville_etablissement", __("ville"));
        //
        $form->setLib("nom_exploitant", __("nom de naissance"));
        $form->setLib("nom_co_exploitant", __("nom de naissance"));
        $form->setLib("nom_proprietaire", __("nom de naissance"));
        $form->setLib("nom_ancien_exploitant", __("nom de naissance"));
        //
        $form->setLib("nom_marital_exploitant", __("nom d'usage"));
        $form->setLib("nom_marital_co_exploitant", __("nom d'usage"));
        //$form->setLib("nom_marital_proprietaire", __("nom d'usage"));
        //
        $form->setLib("prenom_exploitant", __("prénom"));
        $form->setLib("prenom_co_exploitant", __("prénom"));
        $form->setLib("prenom_proprietaire", __("prénom"));
        $form->setLib("prenom_ancien_exploitant", __("prénom"));
        //
        $form->setLib("civilite_exploitant", __("civilité"));
        $form->setLib("civilite_co_exploitant", __("civilité"));
        $form->setLib("civilite_proprietaire", __("civilité"));
        //
        $form->setLib("date_naissance_exploitant", __("date de naissance"));
        $form->setLib("date_naissance_co_exploitant", __("date de naissance"));
        //
        $form->setLib("ville_naissance_exploitant", __("ville de naissance"));
        $form->setLib("ville_naissance_co_exploitant", __("ville de naissance"));
        //
        $form->setLib("cp_naissance_exploitant", __("code postal de naissance"));
        $form->setLib("cp_naissance_co_exploitant", __("code postal de naissance"));
        //
        $form->setLib("adresse1_exploitant", __("adresse (ligne 1)"));
        $form->setLib("adresse1_co_exploitant", __("adresse (ligne 1)"));
        $form->setLib("adresse1_proprietaire", __("adresse (ligne 1)"));
        //
        $form->setLib("adresse2_exploitant", __("adresse (ligne 2)"));
        $form->setLib("adresse2_co_exploitant", __("adresse (ligne 2)"));
        $form->setLib("adresse2_proprietaire", __("adresse (ligne 2)"));
        //
        $form->setLib("cp_exploitant", __("code postal"));
        $form->setLib("cp_co_exploitant", __("code postal"));
        $form->setLib("cp_proprietaire", __("code postal"));
        //
        $form->setLib("ville_exploitant", __("ville"));
        $form->setLib("ville_co_exploitant", __("ville"));
        $form->setLib("ville_proprietaire", __("ville"));
        //
        $form->setLib("qualite_exploitant", __("qualité"));
        $form->setLib("qualite_co_exploitant", __("qualité"));
        $form->setLib("qualite_ancien_exploitant", __("qualité"));
        //
        $form->setLib("nationalite_exploitant", __("nationalité"));
        $form->setLib("nationalite_co_exploitant", __("nationalité"));
        //
        $form->setLib("particularite_exploitant", __("particularité"));
        $form->setLib("particularite_co_exploitant", __("particularité"));
        //
        $form->setLib("idem_exploitant_proprietaire", __("identique à l'exploitant"));
        //
        $form->setLib("profession_proprietaire", __("profession"));
        //
        $form->setLib("ancien_proprietaire", "nom et prénom");
        //
        $form->setLib("observation", "");
    }


    /**
     * CONDITION - is_permanent.
     *
     * @return boolean
     */
    function is_permanent() {
        if ($this->getVal("nature") === "permanent") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_temporaire.
     *
     * @return boolean
     */
    function is_temporaire() {
        if ($this->getVal("nature") === "temporaire") {
            return true;
        }
        return false;
    }

    /**
     * Retourne la dernière date de début de validité de toutes les demandes de licence de l'établissement.
     *
     * @return string '' si aucune demande n'est trouvée ou la date au format YYYY-MM-DD
     */
    function get_date_ancienne_demande() {
        $query = sprintf(
            'SELECT date_debut_validite FROM %1$sdemande_licence WHERE etablissement=%2$s ORDER BY date_debut_validite DESC LIMIT 1',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        );
        $date = $this->f->db->getone($query);
        $this->f->addToLog(__METHOD__."(): db->getone(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($date);
        return $date;
    }

    function get_titre_de_civilite_libelle($titre_de_civilite_id) {
        $inst_titre_de_civilite = $this->f->get_inst__om_dbform(array(
            "obj" => "titre_de_civilite",
            "idx" => $titre_de_civilite_id,
        ));
        //
        return $inst_titre_de_civilite->getVal('libelle');
    }

    /**
     * VIEW - view_summary.
     *
     * @return boolean
     */
    function view_summary() {
        $this->checkAccessibility();

        // Template d'un bloc d'information
        $template_bloc = '<div class="bloc %s-bloc-%s col_12"><div class="bloc-titre"><span class="text">%s</span></div>%s</div>';

        // Template d'un champ d'information de l'établissement
        $template_field_etab = '<div class="field field-type-static"><div class="form-libelle"><label for="%1$s" class="libelle-%1$s" id="lib-%1$s">%2$s</label></div><div class="form-content"><span class="field_value" id="%1$s">%3$s</span></div></div>';

        // Template d'un tableau à 6 colonnes
        $template_table_6_columns = '<table id="%s" class="tab-tab %s"><thead><tr class="ui-tabs-nav ui-accordion ui-state-default tab-title"><th class="title">%s</th><th class="title">%s</th><th class="title">%s</th><th class="title">%s</th><th class="title">%s</th><th class="title">%s</th></tr></thead><tbody>%s</tbody></table>';
        // Template d'un tableau à 3 colonnes
        $template_table_3_columns = '<table id="%s" class="tab-tab %s"><thead><tr class="ui-tabs-nav ui-accordion ui-state-default tab-title"><th class="title">%s</th><th class="title">%s</th><th class="title">%s</th></tr></thead><tbody>%s</tbody></table>';
        // Template ligne et cellule d'un tableau
        $template_table_td = '<td class="%s">%s</td>';
        $template_table_tr = '<tr class="tab-data %s">%s</tr>';
        // Template lorsqu'il n'y a pas de resultat
        $template_colspan = '<td colspan="%s" align="center">%s</td>';

        // Template d'un lien
        $template_link_edition = '<a id="%1$s_%3$s" target="blank" href="../app/index.php?module=form&obj=%2$s&idx=%3$s%4$s" title="%5$s"><span class="om-prev-icon om-icon-16 pdf-16">%5$s</span></a>';

        // Bloc n°1 - Informations de l'établissement
        // Liste des champs composant les caractéristiques de l'établissement
        $etab_fields = sprintf($template_field_etab, 'etablissement', __('id'), $this->getVal("etablissement"));
        $etab_fields .= sprintf($template_field_etab, 'enseigne', __('raison_sociale'), $this->getVal("raison_sociale"));
        $etab_fields .= sprintf($template_field_etab, 'enseigne', __('enseigne'), $this->getVal("enseigne"));
        $etab_fields .= sprintf($template_field_etab, 'exploitant', __('exploitant'), trim($this->get_titre_de_civilite_libelle($this->getVal("civilite_exploitant"))." ".$this->getVal("nom_exploitant")." ".$this->getVal("prenom_exploitant")));
        $etab_fields .= sprintf($template_field_etab, 'proprietaire', __('propriétaire'), trim($this->get_titre_de_civilite_libelle($this->getVal("civilite_proprietaire"))." ".$this->getVal("nom_proprietaire")." ".$this->getVal("prenom_proprietaire")));
        $bloc_etab = sprintf($template_bloc, 'etablissement', 'caracteristiques', __('caractéristiques'), $etab_fields);

        // Bloc n°2 - Liste des demandes de licence
        $sql = sprintf('
            SELECT
                to_char(demande_licence.date_debut_validite ,\'DD/MM/YYYY\') as date_deb,
                to_char(demande_licence.date_fin_validite ,\'DD/MM/YYYY\') as date_fin,
                terme.libelle as terme_libelle,
                type_licence.libelle as type_licence_libelle,
                demande_licence.demande_licence as demande_licence_id
            FROM %1$sdemande_licence
            INNER JOIN %1$stype_licence ON demande_licence.type_licence = type_licence.type_licence
            INNER JOIN %1$sterme ON demande_licence.terme = terme.terme
            WHERE demande_licence.etablissement = %2$s',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        $demande_licence_lines = '';
        $odd_even = 0;
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            $demande_licence_cells = sprintf($template_table_td, '', $row['type_licence_libelle']);
            $demande_licence_cells .= sprintf($template_table_td, '', $row['date_deb']);
            $demande_licence_cells .= sprintf($template_table_td, '', $row['date_fin']);
            $demande_licence_cells .= sprintf($template_table_td, '', $row['terme_libelle']);
            $demande_licence_edition_recepisse = sprintf($template_link_edition, 'edition_recepisse', 'demande_licence', $row['demande_licence_id'], '&action=12', __('récépissé'));
            $demande_licence_cells .= sprintf($template_table_td, '', $demande_licence_edition_recepisse);
            $demande_licence_edition_temp = sprintf($template_link_edition, 'edition_temp', 'demande_licence', $row['demande_licence_id'], '&action=14', __('demande de licence temporaire'));
            $demande_licence_cells .= sprintf($template_table_td, '', $demande_licence_edition_temp);
            $demande_licence_lines .= sprintf($template_table_tr, ($odd_even % 2 == 0 ? 'odd' : 'even'), $demande_licence_cells);
            $odd_even++;
        }
        //
        if ($demande_licence_lines === '') {
            $demande_licence_lines = sprintf($template_colspan, 6, __('Aucun enregistrement.'));
        }
        //
        $table_demande_licence = sprintf($template_table_6_columns, 'tab-demande_licence', 'ui-widget', __('type_licence'), __('date_debut_validite'), __('date_fin_validite'), __('terme'), '', '', $demande_licence_lines);
        $bloc_demande_licence = sprintf($template_bloc, 'demande_licence', 'demande_licence', __('demande de licence'), $table_demande_licence);

        // Bloc n°3 - Liste des courriers
        $sql = sprintf('
            SELECT
                courrier,
                objet
            FROM %1$scourrier
            WHERE etablissement = %2$s',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        $courrier_lines = '';
        $odd_even = 0;
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            $courrier_cells = sprintf($template_table_td, '', $row['courrier']);
            $courrier_cells .= sprintf($template_table_td, '', $row['objet']);
            $courrier_edition = sprintf($template_link_edition, 'edition_courrier', 'courrier', $row['courrier'], '&action=6', __("voir l'édition"));
            $courrier_cells .= sprintf($template_table_td, '', $courrier_edition);
            $courrier_lines .= sprintf($template_table_tr, ($odd_even % 2 == 0 ? 'odd' : 'even'), $courrier_cells);
            $odd_even++;
        }
        //
        if ($courrier_lines === '') {
            $courrier_lines = sprintf($template_colspan, 3, __('Aucun enregistrement.'));
        }
        //
        $table_courrier = sprintf($template_table_3_columns, 'tab-courrier', 'ui-widget', __('courrier'), __('objet'), '', $courrier_lines);
        $bloc_courrier = sprintf($template_bloc, 'courrier', 'courrier', __('courrier'), $table_courrier);

        // Bloc n°4 - Liste des dossiers
        $sql = sprintf('
            SELECT
                dossier,
                observation
            FROM %1$sdossier
            WHERE etablissement = %2$s',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        $dossier_lines = '';
        $odd_even = 0;
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            $dossier_cells = sprintf($template_table_td, '', $row['dossier']);
            $dossier_cells .= sprintf($template_table_td, '', $row['observation']);
            $dossier_edition = sprintf($template_link_edition, 'voir_piece', 'dossier', $row['dossier'], '&snippet=file&champ=fichier&id='.$row['dossier'], __('voir la pièce'));
            $dossier_cells .= sprintf($template_table_td, '', $dossier_edition);
            $dossier_lines .= sprintf($template_table_tr, ($odd_even % 2 == 0 ? 'odd' : 'even'), $dossier_cells);
            $odd_even++;
        }
        //
        if ($dossier_lines === '') {
            $dossier_lines = sprintf($template_colspan, 3, __('Aucun enregistrement.'));
        }
        //
        $table_dossier = sprintf($template_table_3_columns, 'tab-dossier', 'ui-widget', __('dossier'), __('observation'), '', $dossier_lines);
        $bloc_dossier = sprintf($template_bloc, 'dossier', 'dossier', __('dossier'), $table_dossier);

        // Affiche la fiche
        printf('<div class="formEntete ui-corner-all">%s%s%s%s</div>', $bloc_etab, $bloc_demande_licence, $bloc_courrier, $bloc_dossier);
    }

    /**
     * Récupération des valeurs des champs de fusion
     *
     * @return array         tableau associatif
     */
    function get_values_merge_fields() {
        // récupération de la table de la classe instanciée
        $table = $this->table;
        $classe = get_class($this);
        // récupération des clés étrangères
        $foreign_keys = array();
        foreach ($this->foreign_keys_extended as $foreign_key => $values) {
            $foreign_keys[] = $foreign_key;
        }
        // Ajout des liaisons dont le nom diffère de la table cible
        $foreign_keys[] = 'civilite_exploitant';
        $foreign_keys[] = 'civilite_co_exploitant';
        $foreign_keys[] = 'qualite_co_exploitant';
        $foreign_keys[] = 'qualite_ancien_exploitant';
        $foreign_keys[] = 'civilite_proprietaire';
        // initialisation du tableau de valeurs
        $values = array();
        // pour chaque champ de l'objet on crée un champ de fusion
        foreach ($this->champs as $key => $champ) {
            //
            if (in_array($champ, $this->get_merge_fields_to_avoid())) {
                continue;
            }
            // récupération de la valeur
            $value = $this->getVal($champ);
            // si c'est un booléen on remplace par oui/non
            if ($this->type[$key] == 'bool') {
                switch ($value) {
                    case 't':
                    case 'true':
                    case 1:
                        $value = __("oui");
                        break;
                    case 'f':
                    case 'false':
                    case 0:
                        $value = __("non");
                        break;
                }
            }
            // si c'est une date anglosaxonne on la formate en FR
            if (DateTime::createFromFormat('Y-m-d', $value) !== FALSE) {
                $dateFormat = new DateTime($value);
                $value = $dateFormat->format('d/m/Y');
            }
            // si c'est une clé étrangère avec une valeur valide
            // on remplace par le libellé
            if (in_array($champ, $foreign_keys)
                && $value != null && $value != '') {
                // construction variable sql
                $var_sql = $champ."_by_id";
                // si la variable existe
                $sql = $this->get_var_sql_forminc__sql($var_sql);
                if ($sql != "") {
                    // remplacement de l'id par sa valeur dans la condition
                    $sql = str_replace('<idx>', $value, $sql);
                    // exécution requete
                    $res = $this->f->db->query($sql);
                    $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
                    // Si la récupération de la description de l'avis échoue
                    if ($this->f->isDatabaseError($res, true)) {
                        // Appel de la methode de recuperation des erreurs
                        $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                        $this->correct = false;
                        return false;
                    }
                    $row = &$res->fetchRow();
                    // récupération libellé
                    $value = $row[1];
                }
            }
            $values[$table.".".$champ] = $value;
        }
        return $values;
    }
}
