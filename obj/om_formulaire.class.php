<?php
/**
 * Ce script définit la classe 'om_formulaire'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_formulaire.class.php";

/**
 * Définition de la classe 'om_formulaire' (om_formulaire).
 *
 * Cette classe est destinée à permettre la surcharge de certaines méthodes de
 * la classe 'om_formulaire' du framework pour des besoins spécifiques de
 * l'application.
 */
class om_formulaire extends formulaire {

    /**
     * WIDGET_FORM - file.
     *
     * Le bouton 'Voir' présent dans le widget de formulaire du framework est très souvent
     * perturbant pour l'utilisateur. L'objet de la surcharge est donc de le supprimer et
     * de laisser uniquement le bouton 'Télécharger'.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function file($champ, $validation, $DEBUG = false) {
        // Récupération du paramétrage si renseigné
        $obj = (isset($this->select[$champ]['obj'])) ? $this->select[$champ]['obj'] : $this->getParameter("obj");
        $idx = (isset($this->select[$champ]['idx'])) ? $this->select[$champ]['idx'] : $this->getParameter("idx");
        $val = (isset($this->select[$champ]['val'])) ? $this->select[$champ]['val'] : $this->val[$champ];
        $field = (isset($this->select[$champ]['champ'])) ? $this->select[$champ]['champ'] : $champ;
        // Si le storage n'est pas configuré, alors on affiche un message
        // d'erreur clair pour l'utilisateur
        echo "<div id=\"".$champ."\">";
        if ($this->f->storage === null) {
            // Message d'erreur
            echo __("Le système de stockage n'est pas accessible. Erreur de ".
                   "paramétrage. Contactez votre administrateur.");
            echo "</div>";
            // On sort de la méthode
            return -1;
        }
        //
        if ($val !== "" && $val !== null) {
            //
            $filename = $this->f->storage->getFilename($val);
            //
            if ($filename !== ""
                && $filename !== null
                && $filename !== 'OP_FAILURE') {
                //
                echo $filename;
                //
                echo " <span class=\"om-prev-icon reqmo-16\" title=\"".__("Enregistrer le fichier")."\">";
                echo "<a href=\"".OM_ROUTE_FORM."&snippet=file&obj=".$obj."&amp;champ=".$field.
                        "&amp;id=".$idx."\" target=\"_blank\">";
                echo __("Télécharger");
                echo "</a>";
                echo "</span>";
            } else {
                echo __("Le fichier n'existe pas ou n'est pas accessible.");
            }
        }
        echo "</div>";
    }
}
