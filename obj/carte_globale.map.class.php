<?php
/**
 * Ce script définit la classe 'om_map_obj'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_map.class.php";

/**
 * Définition de la classe 'om_map_obj' (om_map).
 */
class om_map_obj extends om_map {

    /**
     * @return void
     */
    function prepareJS() {
        //
        printf('
<script type="text/javascript">
function zoomZoom() {
    var vraiZoom=map.getZoom();
    if (osm.visibility == false) {
        vraiZoom += 10;
    }
    return vraiZoom;
}

// Chargement des marqueurs aux formats geojson
function map_load_geojson_markers() {
    map.removeLayer(markersLayer);
    fichier_jsons = base_url_map_get_geojson_markers+"&obj="+obj+"&idx="+idx+"&etendue="+etendue+"&reqmo="+reqmo+
        "&premier="+premier+"&tricol="+tricol+"&advs_id="+advs_id+
        "&valide="+valide+"&style="+style+"&onglet="+onglet;
    markersLayer = new OpenLayers.Layer.Vector(
        "Marqueurs",
        {
            projection: displayProjection,
            strategies: [new OpenLayers.Strategy.Fixed()],
            protocol: new OpenLayers.Protocol.HTTP(
                {
                    url: fichier_jsons,
                    format: new OpenLayers.Format.GeoJSON()
                }
            )
        }
    );
    map.addLayer(markersLayer);

    if (sm_sld_marqueur!="") {
        OpenLayers.Request.GET(
            {
                url: url_sld_marqueur,
                success: map_successGetSld_geojson_markers
            }
        );
    } else  {
        markersLayer.styleMap = new OpenLayers.StyleMap(
            {
                "default": {
                    externalGraphic: img_maj,
                    graphicWidth:img_w,
                    graphicHeight: img_h,
                    graphicYOffset: -img_h
                },
                "select": {
                    externalGraphic: img_maj_hover,
                    graphicWidth:  img_w,
                    graphicHeight: img_h,
                    graphicYOffset: -img_h
                }
            }
        );
        markersLayer.redraw();
    }
    map_empty_layer_list("markers");
    map_add_layer_to_list(markersLayer, "markers", 100);
    markersLayer.events.on({
        "featureselected": map_on_select_feature_marker,
        "featureunselected": map_on_unselect_feature_marker
    });
    selectControl_layers.push(markersLayer);
    //
    var styleCircle = new OpenLayers.Style(
        {
            pointRadius : "${radius}",
            graphicName: "circle",
            fillColor: "indigo",
            fillOpacity: 0.1,
            strokeWidth: 1,
            strokeOpacity: 10,
            strokeColor: "indigo"
        },
        {
            context: {
                radius: function(feature) {
                    surf = feature.attributes.rayon*0.00000882;  // 1 = 100 KM / 0.0007 = 70metre // le 0.000 008 85 a été étalonnée à laide de la fonction distance() de postgis
                    surf = surf*Math.pow(2, zoomZoom());
                    return surf;
                }
            }
        }
    );
    perimetres_jsons = base_url_map_get_geojson_markers+"&obj=perimetre&etendue="+etendue+"&advs_id=";
    zone_layer = new OpenLayers.Layer.Vector(
        "Périmètres",
        {
            projection: displayProjection,
            strategies: [new OpenLayers.Strategy.Fixed()],
            protocol: new OpenLayers.Protocol.HTTP(
                {
                    url: perimetres_jsons,
                    format: new OpenLayers.Format.GeoJSON()
                }
            ),
            styleMap: new OpenLayers.StyleMap({
                "default": styleCircle
            })
        }
    );
    map.addLayer(zone_layer);
    map_add_layer_to_list(zone_layer, "markers", 101);
}</script>'
        );
        parent::prepareJS();
    }
}
