<?php
/**
 * Ce script définit la classe 'terme'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../gen/obj/terme.class.php";

/**
 * Définition de la classe 'terme' (om_dbform).
 */
class terme extends terme_gen {

    /**
     * @return boolean
     */
    function is_terme__perm() {
        if ($this->getVal("code") !== "PERM") {
            return false;
        }
        return true;
    }
}
