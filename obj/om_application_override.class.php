<?php
/**
 * Ce script définit la classe 'om_application_override'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

/**
 * Définition de la classe 'om_application_override' (om_application).
 *
 * Cette classe a pour vocation de recevoir les surcharges suceptibles
 * (car suffisament générique) d'intégrer le framework.
 */
class om_application_override extends application {

    /**
     * Surcharge - view_main().
     *
     * Ajoute la vue 'settings' comme un nouveau module.
     *
     * @return void
     */
    function view_main() {
        if ($this->get_submitted_get_value("module") === "settings") {
            $this->view_module_settings();
            return;
        }
        parent::view_main();
    }

    /**
     * VIEW - view_module_settings.
     *
     * Gère le menu 'Administration & Paramétrage'.
     *
     * @return void
     */
    function view_module_settings() {
        //
        $this->isAuthorized("settings");
        $this->setTitle(__("Administration & Paramétrage")." -> ".__("menu"));
        $this->setFlag(null);
        $this->display();
        //
        printf('
        <form id="settings-live-search" action="" class="styled" method="post">
        <div class="filter-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24px" viewBox="0 0 24 24" height="24px"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/><path fill="none" d="M0 0h24v24H0z"/></svg>
        <input type="text" class="text-input" id="filter" value="" placeholder="Rechercher" />
        </div>
        </form>');
        //
        $list = array();
        $new_list = null;
        //
        $menu_to_display = array();

        // On inclut le fichier de configuration du menu
        $menu = $this->get_config__menu();

        // Recuperation des variables
        $scriptAppele = explode("/", $_SERVER["PHP_SELF"]);
        $scriptAppele = $scriptAppele[ count($scriptAppele) - 1 ];
        $obj = (isset($_GET['obj']) ? $_GET['obj'] : "");

        //
        foreach ($menu as $m => $rubrik) {
            // Gestion des paramètres
            if (isset($rubrik["parameters"])
                && is_array($rubrik["parameters"])) {
                //
                $flag_parameter = false;
                //
                foreach ($rubrik["parameters"] as $parameter_key => $parameter_value) {
                    //
                    if ($parameter_key == "is_settings_view_enabled") {
                        continue;
                    }
                    //
                    if ($this->getParameter($parameter_key) != $parameter_value) {
                        //
                        $flag_parameter = true;
                        break;
                    }
                }
                //
                if ($flag_parameter == true) {
                    // On passe directement a l'iteration suivante de la boucle
                    continue;
                }
            }
            // Gestion des droits d'acces : si l'utilisateur n'a pas la
            // permission necessaire alors la rubrique n'est pas affichee
            if (!isset($rubrik['right'])
                || ( $rubrik['right'] != "menu_parametrage"
                    && $rubrik['right'] != "menu_administration")) {
                // On passe directement a l'iteration suivante de la boucle
                continue;
            }
            // Initialisation
            $rubrik_to_display = $rubrik;
            $elems_in_rubrik_to_display = array();
            $cpt_links = 0;


            // Test des criteres pour determiner si la rubrique est active
            if (isset($rubrik['open'])) {
                foreach ($rubrik['open'] as $scriptobj) {
                    // separation du nom de fichier et du obj
                    $scriptobjarray = explode("|", $scriptobj);
                    $cle_script = $scriptobjarray[0];
                    $cle_obj = $scriptobjarray[1];

                    $cle_script_ok = true;
                    if ($cle_script != "" and $cle_script != $scriptAppele) {
                        $cle_script_ok = false;
                    }
                    $cle_obj_ok = true;
                    if ($cle_obj != "" and $cle_obj != $obj) {
                        $cle_obj_ok = false;
                    }
                    if ($cle_obj_ok and $cle_script_ok) {
                        $rubrik_to_display["selected"] = "selected";
                    }
                }
            }

            // Boucle sur les entrees de menu
            foreach ($rubrik['links'] as $link) {
                // Gestion des paramètres
                if (isset($link["parameters"])
                    && is_array($link["parameters"])) {
                    //
                    $flag_parameter = false;
                    //
                    foreach ($link["parameters"] as $parameter_key => $parameter_value) {
                        //
                        if ($this->getParameter($parameter_key) != $parameter_value) {
                            //
                            $flag_parameter = true;
                            break;
                        }
                    }
                    //
                    if ($flag_parameter == true) {
                        // On passe directement a l'iteration suivante de la boucle
                        continue;
                    }
                }
                // Gestion des droits d'acces : si l'utilisateur n'a pas la
                // permission necessaire alors l'entree n'est pas affichee
                if (isset($link['right'])
                    && !$this->isAccredited($link['right'], "OR")) {
                    // On passe directement a l'iteration suivante de la boucle
                    continue;

                }
                //
                $cpt_links++;

                // Entree de menu
                if (trim($link['title']) != "<hr />" and trim($link['title']) != "<hr/>"
                    and trim($link['title']) != "<hr>") {
                    // MENU OPEN
                    $link_actif = "";
                    if (isset($link['open'])) {
                        if (gettype($link['open']) == "string") {
                            $link['open'] = array($link['open'],);
                        }

                        foreach ($link['open'] as $scriptobj) {
                            // separation du nom de fichier et du obj
                            $scriptobjarray = explode("|", $scriptobj);
                            $cle_script = $scriptobjarray[0];
                            $cle_obj = $scriptobjarray[1];

                            $cle_script_ok = true;
                            if ($cle_script != "" and $cle_script != $scriptAppele) {
                                $cle_script_ok = false;
                            }
                            $cle_obj_ok = true;
                            if ($cle_obj != "" and $cle_obj != $obj) {
                                $cle_obj_ok = false;
                            }
                            if ($cle_obj_ok and $cle_script_ok) {
                                $rubrik_to_display["selected"] = "selected";
                                $link["selected"] = "selected";
                            }
                        }
                    }
                }
                $elems_in_rubrik_to_display[] = $link;
            }

            //
            $rubrik_to_display["links"] = $elems_in_rubrik_to_display;
            // Si des liens ont ete affiches dans la rubrique alors on
            // affiche la rubrique
            if ($cpt_links != 0) {
                //
                $menu_to_display[] = $rubrik_to_display;
            }
        }
        //

        //

        foreach ($menu_to_display as $key => $value) {
            //
            if ($value["right"] == "menu_parametrage" || $value["right"] == "menu_administration") {
                //
                foreach ($value["links"] as $key1 => $value1) {
                    //
                    if (isset($value1["title"]) && $value1["title"] == "<hr/>") {
                        continue;
                    }
                    //
                    if (isset($value1["class"]) && $value1["class"] == "category") {
                        if ($new_list != null) {
                            $list[] = $new_list;
                        }
                        //
                        $new_list = array(
                            "title" => $value1,
                            "list" => array(),
                        );
                        //
                        continue;
                    }
                    //
                    if ($new_list === null) {
                        //
                        $new_list = array(
                            "title" => "",
                            "list" => array(),
                        );
                    } else {
                        $new_list["list"][] = $value1;
                    }
                }
            }
        }
        if ($new_list !== null) {
            $list[] = $new_list;
        }

        $settings = "";
        foreach ($list as $key => $value) {
            //
            $title = sprintf(
                '<div class="list-group-title">%s</div>',
                $value["title"]["title"]
            );
            $description = "";
            if (isset($value["title"]["description"])) {
                $description = $value["title"]["description"];
            }
            //
            $items = "";
            foreach ($value["list"] as $key2 => $value2) {
                //
                $items .= sprintf(
                    '
                  <a href="%s" class="list-group-item %s">
                    <h4 class="list-group-item-heading">%s</h4>
                    <p class="list-group-item-text">%s</p>
                  </a>',
                    $value2["href"],
                    $value2["class"],
                    $value2["title"],
                    (isset($value2["description"]) ? $value2["description"] : "")
                );
            }
            //
            $settings .= sprintf(
                '<div class="item">
                    %s
                    %s
                    <div class="list-group">
                        %s
                    </div>
                </div>',
                $title,
                $description,
                $items
            );
        }
        //
        printf(
            '
        <div id="settings" class="container-fluid">
        <div class="row">
        %s
        </div>
        </div>
            ',
            $settings
        );
    }

    /**
     * Surcharge - set_config__shortlinks().
     *
     * Ajout du lien vers la view 'settings' pour le menu 'Administration & Paramétrage'.
     *
     * @return void
     */
    protected function set_config__shortlinks() {
        parent::set_config__shortlinks();
        //
        $shortlinks = array();
        $shortlinks[] = array(
            "title" => __("Administration & Paramétrage"),
            "description" => __("Menu de tous les écrans d'administration et de paramétrage"),
            "href" => "../app/index.php?module=settings",
            "class" => "shortlinks-settings",
            "right" => "settings",
            "parameters" => array("is_settings_view_enabled" => true, ),
        );
        //
        $this->config__shortlinks = array_merge(
            $shortlinks,
            $this->config__shortlinks
        );
    }
}
