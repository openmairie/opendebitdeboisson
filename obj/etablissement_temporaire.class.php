<?php
/**
 * Ce script définit la classe 'etablissement_temporaire'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../obj/etablissement.class.php";

/**
 * Définition de la classe 'etablissement_temporaire' (om_dbform).
 */
class etablissement_temporaire extends etablissement {

    /**
     *
     */
    var $_absolute_class_name = "etablissement_temporaire";

    /**
     * @return void
     */
    function init_class_actions() {
        etablissement_gen::init_class_actions();
        //
        $this->class_actions[1]["condition"][] = "is_temporaire";
        $this->class_actions[2]["condition"][] = "is_temporaire";
        $this->class_actions[3]["condition"][] = "is_temporaire";
    }

    /**
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $crud = $this->get_action_crud($maj);
        // CREATE
        if ($crud == 'create') {
            $form->setType("nature", "hiddenstatic");
        }
        //
        $form->setType("geom", "hidden");
        // L'établissement temporaire ne gère qu'une partie des champs de l'établissement
        $hidden_fields = array(
            "permis_exploitation",
            "date_permis_exploitation",
            "permis_exploitation_nuit",
            "date_permis_exploitation_nuit",
            "civilite_co_exploitant",
            "nom_co_exploitant",
            "nom_marital_co_exploitant",
            "prenom_co_exploitant",
            "date_naissance_co_exploitant",
            "ville_naissance_co_exploitant",
            "cp_naissance_co_exploitant",
            "adresse1_co_exploitant",
            "adresse2_co_exploitant",
            "cp_co_exploitant",
            "ville_co_exploitant",
            "nationalite_co_exploitant",
            "qualite_co_exploitant",
            "particularite_co_exploitant",
            "nom_ancien_exploitant",
            "prenom_ancien_exploitant",
            "qualite_ancien_exploitant",
            "civilite_proprietaire",
            "nom_proprietaire",
            "prenom_proprietaire",
            "adresse1_proprietaire",
            "adresse2_proprietaire",
            "cp_proprietaire",
            "ville_proprietaire",
            "profession_proprietaire",
            "date_fermeture",
            "date_liquidation",
            "ancien_proprietaire",
        );
        foreach ($hidden_fields as $field) {
            $form->setType($field, "hidden");
        }
    }

    /**
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        $form->setVal("nature", "temporaire");
    }
}
