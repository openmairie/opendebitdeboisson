<?php
/**
 * Ce script définit la classe 'om_dbform'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_dbform.class.php";

/**
 * Définition de la classe 'om_dbform' (om_dbform).
 *
 * Cette classe permet la surcharge de certaines methodes de
 * la classe om_dbform pour des besoins specifiques de l'application.
 */
class om_dbform extends dbform {

    /**
     * @return string
     */
    function get_default_om_sig_map() {
        return $this->table;
    }

    /**
     * VIEW - view_om_sig_map.
     *
     * @return void
     */
    function view_om_sig_map() {
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "top",
        ));
        $this->retour();
        $this->f->layout->display__form_controls_container__end();
        //
        $form = $this->f->get_inst__om_formulaire();
        $form->entete();
        $this->form_specific_content_before_portlet_actions(11);
        printf(
            '<iframe style="width:100%%;border: 0 none; height: 780px;" src="%s"></iframe>',
            OM_ROUTE_MAP."&mode=tab_sig&obj=".$this->get_default_om_sig_map()."&idx=".$this->getVal($this->clePrimaire)."&popup=1"
        );
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->retour();
        $this->f->layout->display__form_controls_container__end();
    }

    /**
     * @return void
     */
    function setvalF_adresse_postale($val = array(), $msg = "") {
        //
        if (isset($val['saisie_manuelle']) === true
            && $val['saisie_manuelle'] !== 'Oui'
            && isset($val['adresse_postale']) === true
            && $val['adresse_postale'] !== ''
            && $val['adresse_postale'] !== null) {
            //
            $inst_adresse_postale = $this->f->get_inst__om_dbform(array(
                "obj" => "adresse_postale",
                "idx" => $val['adresse_postale']
            ));
            $geom = $inst_adresse_postale->getVal('geom');
            //
            if ($geom !== '' && $geom !== null) {
                $this->valF['geom'] = $geom;
            }
        }

        // Message d'avertissement à l'utilisateur en cas de modification du
        // geom par une nouvelle adresse postale
        if ($this->get_action_crud() === 'update'
            && $this->getVal('geom') !== ''
            && $this->getVal('geom') !== null
            && $this->valF['geom'] !== $this->getVal('geom')) {
            //
            $this->addToMessage($msg);
        }
    }

    /**
     * @return array
     */
    function get_generic_widget_config__adresse_postale__autocomplete() {
        return array(
            // Surcharge visée pour l'ajout
            "obj" => "adresse_postale",
            // Table de l'objet
            "table" => "adresse_postale",
            // Permission d'ajouter : doit toujours être positionné à false
            "droit_ajout" => false,
            // Critères de recherche
            "criteres" => array(
                "adresse_postale.numero" => __("numero"),
                "adresse_postale.complement" => __("complement"),
                "adresse_postale.libelle" => __("libelle")
            ),
            // Tables liées
            "jointures" => array(),
            // Colonnes ID et libellé du champ
            // (si plusieurs pour le libellé alors une concaténation est faite)
            "identifiant" => "adresse_postale.adresse_postale",
            "libelle" => array(
                "adresse_postale.numero",
                "adresse_postale.complement",
                "adresse_postale.libelle",
            ),
        );
    }

}
