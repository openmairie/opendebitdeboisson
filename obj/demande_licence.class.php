<?php
/**
 * Ce script définit la classe 'demande_licence'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../gen/obj/demande_licence.class.php";

/**
 * Définition de la classe 'demande_licence' (om_dbform).
 */
class demande_licence extends demande_licence_gen {

    /**
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        //
        $this->class_actions[11] = array(
            "identifier" => "cerfa_declaration",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("CERFA de déclaration"),
                "order" => 11,
                "class" => "pdf-16",
            ),
            "view" => "view_cerfa_declaration",
            "permission_suffix" => "consulter",
        );
        //
        $this->class_actions[12] = array(
            "identifier" => "recepisse",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Récépissé"),
                "order" => 12,
                "class" => "pdf-16",
            ),
            "view" => "view_recepisse",
            "permission_suffix" => "consulter",
        );
        //
        $this->class_actions[13] = array(
            "identifier" => "recepisse_co_exploitant",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Récépissé avec co-exploitant"),
                "order" => 13,
                "class" => "pdf-16",
            ),
            "view" => "view_recepisse_co_exploitant",
            "condition" => array("etablissement_is_permanent"),
            "permission_suffix" => "consulter",
        );
        //
        $this->class_actions[14] = array(
            "identifier" => "demande_temp_reponse",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Licence temporaire avec réponse"),
                "order" => 14,
                "class" => "pdf-16",
            ),
            "view" => "view_demande_temp_reponse",
            "permission_suffix" => "consulter",
        );
    }

    /**
     * @return mixed
     */
    function display_infos_demande_licence() {
        // Récupère le compte rendu de traitement
        $cr = json_decode($this->getVal('compte_rendu'));
        // S'il n'y a pas de compte rendu sur la demande de licence
        if ($cr === null) {
            $this->f->displayMessage('error', __("Aucun traitement n'a été effectué sur cette demande de licence."));
            return false;
        }
        // Affiche le compte rendu concernant le permis d'exploitation si son
        // statut est en erreur
        if ($cr->verifier_permis_exploitation->statut === 'error') {
            $this->f->displayMessage(
                $cr->verifier_permis_exploitation->statut,
                $cr->verifier_permis_exploitation->message
            );
        }
        // Affiche le compte rendu concernant les distance si son statut est en
        // erreur
        if ($cr->verifier_distance->statut === 'error') {
            $this->f->displayMessage(
                $cr->verifier_distance->statut,
                $cr->verifier_distance->message
            );
        }
        // Affiche le compte rendu concernant le nombre de licence si son statut
        // est en erreur
        if ($cr->verifier_nb_licence->statut === 'error') {
            $this->f->displayMessage(
                $cr->verifier_nb_licence->statut,
                $cr->verifier_nb_licence->message
            );
        }
    }

    /**
     * CONDITION - etablissement_is_permanent.
     *
     * @return boolean
     */
    function etablissement_is_permanent() {
        //
        if ($this->retourformulaire !== "etablissement_temporaire") {
            return true;
        }
        //
        return false;
    }

    /**
     * @return void
     */
    function form_specific_content_before_portlet_actions($maj) {
        if ($maj == 3) {
            $this->display_infos_demande_licence();
        }
    }

    /**
     * @return void
     */
    function sousform_specific_content_before_portlet_actions($maj) {
        if ($maj == 3) {
            $this->display_infos_demande_licence();
        }
    }

    /**
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        if ($maj == 0) {
            $form->setVal("date_demande_licence", date("d/m/Y"));
            $form->setVal("heure_debut", "00:00:00");
            $form->setVal("heure_fin", "23:59:59");
            // Si on se trouve dans le contexte d'un établissement, on l'instancie pour récupérer
            // des informations et pré-remplir certains champs
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $inst_etablissement = $this->f->get_inst__om_dbform(array(
                    "obj" => "etablissement",
                    "idx" => intval($this->getParameter("idxformulaire")),
                ));
                if ($inst_etablissement !== null) {
                    // On positionne la particularité de l'exploitant de l'établissement dans le champ
                    // 'particularité' de la demande. Il reste modifiable.
                    $form->setVal("particularite", $inst_etablissement->getVal("particularite_exploitant"));
                    // On positionne la date de dernière demande de licence sur l'établissement dans le champ
                    // ' date_ancienne_demande ' de la demande. Il reste modifiable.
                    $form->setVal("date_ancienne_demande", $inst_etablissement->get_date_ancienne_demande());
                }
            }
            //
            if ($this->retourformulaire == "etablissement_temporaire") {
                $form->setVal(
                    "type_demande",
                    $this->f->get_param_metier("type_demande", "OUV", "id_from_code")
                );
            }
        }
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_terme() {
        //
        if ($this->retourformulaire == "etablissement_temporaire") {
            return "SELECT terme.terme, terme.libelle FROM ".DB_PREFIXE."terme WHERE terme.code != 'PERM' ORDER BY terme.libelle ASC";
        }
        return "SELECT terme.terme, terme.libelle FROM ".DB_PREFIXE."terme ORDER BY terme.libelle ASC";
    }

    /**
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $crud = $this->get_action_crud($maj);
        //
        if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
            $form->setType("etablissement", "hidden");
        }
        //
        if ($this->retourformulaire == "etablissement_temporaire") {
            $form->setType("type_demande", "selecthiddenstatic");
        }
        // Gestion du compte rendu des traitements
        $form->setType('compte_rendu', 'hidden');
    }

    /**
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        $form->setOnchange("heure_debut", "ftime(this);");
        $form->setOnchange("heure_fin", "ftime(this);");
        //
        if ($maj === '0'
            && $this->retourformulaire === "etablissement_permanent") {
            // La valeur de la date de fin de validité sera la valeur date de
            // début de validité + 100 ans
            $form->setOnchange('date_debut_validite','fdate(this),add_years_to_date(this, date_fin_validite, 100)');
        }
    }

    /**
     * Mutateur pour la propriété 'valF' en mode CREATE & UPDATE.
     *
     * @param array $val Tableau des valeurs brutes.
     *
     * @return void
     */
    function setvalF($val = array()) {
        parent::setvalF($val);
        // Traitements des demande de licence
        if (($this->get_action_crud() === 'create'
                || $this->get_action_crud() === 'update')
            && $val['terme'] !== '' && $val['terme'] !== null) {
            // Instanciation de l'établissement
            $inst_etablissement = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $val['etablissement'],
            ));
            $etablissement_id = $inst_etablissement->getVal('etablissement');
            // Intanciation du type de licence
            $inst_type_licence = $this->f->get_inst__om_dbform(array(
                "obj" => "type_licence",
                "idx" => $val['type_licence'],
            ));
            // Instanciation du type de demande
            $inst_type_demande = $this->f->get_inst__om_dbform(array(
                "obj" => "type_demande",
                "idx" => $val['type_demande'],
            ));
            // Instanciation du terme
            $inst_terme = $this->f->get_inst__om_dbform(array(
                "obj" => "terme",
                "idx" => $val['terme'],
            ));
            $terme_code = $inst_terme->getVal('code');

            // Statut et message par défaut des différents traitements
            $statut_permis_exploitation = '';
            $message_permis_exploitation = '';
            $statut_distance = '';
            $message_distance = '';
            $perimetres = array();
            $etablissements = array();
            $statut_nb_licence = '';
            $message_nb_licence = '';
            $count_nb_licence = '';

            // Concernant les établissements permanent
            if ($this->etablissement_is_permanent() === true) {

                // Vérification du permis d'exploitation
                $permis_exploitation = $inst_etablissement->getVal('permis_exploitation');
                $statut_permis_exploitation = 'valid';
                $message_permis_exploitation = __("L'établissement possède un permis d'exploitation.");
                if ($permis_exploitation !== 't') {
                    $statut_permis_exploitation = 'error';
                    $message_permis_exploitation = __("L'établissement ne possède pas de permis d'exploitation.");
                }

                // Vérification des distances des périmètres d'exclusion et des
                // autres établissements possédant les mêmes contraintes de
                // proximités
                $geom = $inst_etablissement->getVal('geom');
                $statut_distance = 'valid';
                $message_distance = __("Il n'y a pas de périmètres d'exclusion ou d'établissements possédant une contrainte de proximité aux alentours de l'établissement.");
                if ($terme_code === 'PERM'
                    && $geom !== ''
                    && $geom !== null
                    && $inst_type_licence->getVal('contrainte_proximite') === 't'
                    && $inst_type_demande->getVal('code') !== 'TRS') {
                    // Récupère la liste des périmètres d'exclusion
                    $perimetres = array();
                    $perimetres = $this->get_distance_perimetre($geom);
                    // Récupère la liste des établissements à proximités
                    $etablissements = array();
                    $etablissements = $this->get_distance_etablissement($geom, $etablissement_id);
                    // Si le traitement retourne un ou plusieurs périmètres
                    // d'exclusion et/ou établissements
                    if($perimetres !== array() || $etablissements !== array()) {
                        $statut_distance = 'error';
                        $message_distance = '';
                        $link_template = '<a style="font-weight: bold;text-decoration: underline;" href="../app/index.php?module=form&obj=%s&action=3&idx=%s&advs_id=&premier=0&tricol=&valide=&retour=tab">%s</a>';
                        //
                        if ($perimetres !== array()) {
                            foreach ($perimetres as $value) {
                                $link = sprintf(
                                    $link_template,
                                    'perimetre',
                                    $value['perimetre'],
                                    $value['libelle']
                                );
                                $message_distance .= '<br/>';
                                $message_distance .= sprintf(
                                    __("La distance est non respectée avec le périmètre d'exclusion %s avec %.2f mètres."),
                                    $link,
                                    round($value['distance'], 2)
                                );
                            }
                        }
                        if ($etablissements !== array()) {
                            foreach ($etablissements as $value) {
                                $link = sprintf(
                                    $link_template,
                                    $this->retourformulaire,
                                    $value['etablissement'],
                                    $value['enseigne']
                                );
                                $message_distance .= '<br/>';
                                $message_distance .= sprintf(
                                    __("La distance est non respectée avec l'établissement possédant une contrainte de proximité %s avec %.2f mètres."),
                                    $link,
                                    round($value['distance'], 2)
                                );
                            }
                        }
                        // Supprime le premier saut de ligne '<br/>'
                        $message_distance = substr_replace($message_distance, '', 0, 5);
                    }
                }
            }

            // Vérification du nombre de licence temporaire ou liée à un terrain
            // de sport
            if ($terme_code !== 'PERM') {
                $statut_nb_licence = 'valid';
                $message_nb_licence = __("Le nombre maximum de demande de licence temporaire ou liée à un terrain de sport pour cet établissement n'est pas dépassé.");
                // Récupère le nombre de licence
                $count_nb_licence = $this->get_count_licence_by_terme($etablissement_id, $terme_code, $val['date_debut_validite']);
                // S'il s'agit d'une licence temporaire, la limite est de 5
                // licences par année
                if ($terme_code === 'TEMP') {
                    if ($count_nb_licence > 5) {
                        $statut_nb_licence = 'error';
                        $message_nb_licence = sprintf(
                            __('Le nombre maximum de demande de licence temporaire pour cet établissement est dépassé avec %s demandes.'),
                            $count_nb_licence
                        );
                    }
                }
                // S'il s'agit d'une licence liée à un terrain de sport, la
                // limite est de 10 licences par année
                if ($terme_code === 'SPOR') {
                    if ($count_nb_licence > 10) {
                        $statut_nb_licence = 'error';
                        $message_nb_licence = sprintf(
                            __('Nombre maximum de demande liée à un terrain de sport pour cet établissement est dépassé avec %s demandes.'),
                            $count_nb_licence
                        );
                    }
                }
            }

            // Composition du compte rendu
            $cr = array(
                'date' => date('Y-m-d H:i:s'),
                'verifier_permis_exploitation' => array(
                    'statut' => $statut_permis_exploitation,
                    'message' => $message_permis_exploitation,
                ),
                'verifier_distance' => array(
                    'statut' => $statut_distance,
                    'message' => $message_distance,
                    'perimetres' => $perimetres,
                    'etablissements' => $etablissements,
                ),
                'verifier_nb_licence' => array(
                    'statut' => $statut_nb_licence,
                    'message' => $message_nb_licence,
                    'nb' => $count_nb_licence,
                ),
            );
            // Sauvegarde du compte rendu
            $this->valF['compte_rendu'] = json_encode($cr);
        }
    }

    /**
     * @return array
     */
    function get_distance_perimetre($geom) {
        // Récupère les périmètres d'exclusion dans lesquelles se trouve
        // l'établissement
        $perimetres = array();
        $sql = sprintf('
            SELECT ST_Distance(geom, \'%1$s\') AS distance, perimetre, libelle
            FROM %2$sperimetre
            WHERE ST_Distance(geom, \'%1$s\') < longueur_exclusion_metre
            ORDER BY perimetre',
            $geom,
            DB_PREFIXE
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $perimetres[] = array(
                'perimetre' => $row['perimetre'],
                'libelle' => $row['libelle'],
                'distance' => $row['distance']
            );
        }
        //
        return $perimetres;
    }

    /**
     * @return array
     */
    function get_distance_etablissement($geom, $etablissement_id) {
        // Récupère les établissements avec un rayon de 50 mètres dans
        // lesquelles se trouve l'établissement
        $etablissements = array();
        $sql = sprintf('
            SELECT ST_Distance(etablissement.geom, \'%1$s\') AS distance, etablissement.etablissement, etablissement.enseigne
            FROM %2$setablissement
                INNER JOIN %2$sdemande_licence ON demande_licence.etablissement = etablissement.etablissement
                INNER JOIN %2$stype_licence ON type_licence.type_licence = demande_licence.type_licence
            WHERE etablissement.etablissement != %3$s
            AND type_licence.contrainte_proximite = \'t\'
            AND ST_Distance(geom, \'%1$s\') < 50
            GROUP BY etablissement.etablissement
            ORDER BY etablissement.etablissement',
            $geom,
            DB_PREFIXE,
            $etablissement_id
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $etablissements[] = array(
                'etablissement' => $row['etablissement'],
                'enseigne' => $row['enseigne'],
                'distance' => $row['distance']
            );
        }
        //
        return $etablissements;
    }

    /**
     * @return integer
     */
    function get_count_licence_by_terme($etablissement_id, $terme_code, $date_debut_validite) {
        // Récupère le nombre de licence de l'établissement sur l'année
        $sql = sprintf('
            SELECT COUNT(*) AS count
            FROM %1$sdemande_licence
                INNER JOIN %1$sterme ON demande_licence.terme = terme.terme
            WHERE etablissement = %2$s
            AND terme.code = \'%3$s\'
            AND TO_CHAR(date_debut_validite,\'YYYY\') = TO_CHAR(date(\'%4$s\'),\'YYYY\')',
            DB_PREFIXE,
            $etablissement_id,
            $terme_code,
            $date_debut_validite
        );
        $count = $this->f->db->getOne($sql);
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($count);
        //
        return $count;
    }

    /**
     * @return string
     */
    function getSubFormTitle($ent) {
        if ($this->getParameter("maj") == 0) {
            return "-> ".__("demandes de licence");
        }
        return "-> ".__("demandes de licence")." -> ".$this->get_default_libelle();
    }


    /**
     *
     */
    function view_widget_dernieres_demandes() {
        /**
         *
         */
        function sprint_table($table) {
            /**
             *
             */
            $template_table = '
            <table class="table table-condensed table-striped table-hover">
                <thead>%s
                </thead>
                <tbody>%s
                </tbody>
            </table>';
            /**
             *
             */
            $template_table_head = '
                <tr>%s
                </tr>';
            /**
             *
             */
            $template_table_head_cell = '
                    <th>%s</th>';
            /**
             *
             */
            $template_table_line = '
                <tr>%s
                </tr>';
            /**
             *
             */
            $template_table_line_with_class = '
                <tr class="%s">%s
                </tr>';
            /**
             *
             */
            $template_table_line_cell = '
                    <td>%s</td>';
            /**
             *
             */
            $template_table_line_cell_colspan = '
                    <td colspan="%s">%s</td>';


            //
            $table_head_ct = "";
            //
            if (isset($table["attr"]["head"])) {
                //
                foreach ($table["attr"]["head"] as $key => $value) {
                    $table_head_ct .= sprintf(
                        $template_table_head_cell,
                        $value
                    );
                }
            }
            //
            $table_head = sprintf(
                $template_table_head,
                $table_head_ct
            );
            //
            $table_body_ct = "";
            //
            foreach ($table as $key_line => $line) {
                //
                if ($key_line === "attr") {
                    continue;
                }
                //
                $table_line_ct = "";
                //
                foreach ($line as $key_cell => $cell) {
                    //
                    if ($key_cell === "attr") {
                        continue;
                    }
                    //
                    if (is_array($cell)) {
                        //
                        if (isset($cell["colspan"])) {
                            //
                            $table_line_ct .= sprintf(
                                $template_table_line_cell_colspan,
                                $cell["colspan"],
                                $cell["content"]
                            );
                        }
                    } else {
                        //
                        $table_line_ct .= sprintf(
                            $template_table_line_cell,
                            $cell
                        );
                    }

                }
                if (isset($line["attr"])
                    && is_array($line["attr"])
                    && isset($line["attr"]["class"])
                    && $line["attr"]["class"] != "") {
                    //
                    $table_body_ct .= sprintf(
                        //
                        $template_table_line_with_class,
                        $line["attr"]["class"],
                        $table_line_ct
                    );
                } else {
                    //
                    $table_body_ct .= sprintf(
                        //
                        $template_table_line,
                        $table_line_ct
                    );
                }
            }
            //
            return sprintf(
                $template_table,
                $table_head,
                $table_body_ct
            );
        }

        //
        $query = sprintf(
            'SELECT
                demande_licence as demande_licence_id,
                to_char(demande_licence.date_demande_licence, \'DD/MM/YYYY\') as date_demande,
                etablissement.raison_sociale,
                type_licence.libelle as type_licence_libelle,
                etablissement.etablissement as etablissement_id,
                etablissement.nature as etablissement_nature
            FROM
                %1$sdemande_licence
                INNER JOIN %1$setablissement
                    ON demande_licence.etablissement=etablissement.etablissement
                LEFT JOIN %1$stype_licence
                    ON demande_licence.type_licence=type_licence.type_licence
            ORDER BY
                demande_licence.date_demande_licence DESC
            LIMIT 10',
            DB_PREFIXE
        );
        $res = $this->f->db->query($query);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        $results = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
        }
        //
        $table = array(
            "attr" => array(
                "head" => array(
                    "",
                    __("Date"),
                    __("Établissement"),
                    __("Type de licence")
                ),
            ),
        );
        foreach ($results as $key => $value) {
            $link = sprintf(
                '%s&direct_link=true&obj=%s&action=3&idx=%s&direct_form=demande_licence&direct_action=3&direct_idx=%s',
                OM_ROUTE_FORM,
                ($value["etablissement_nature"] == "permanent" ? "etablissement_permanent" : "etablissement_temporaire"),
                $value["etablissement_id"],
                $value["demande_licence_id"]
            );
            $table[] = array(
                sprintf(
                    '<a href="%s" class="om-prev-icon consult-16"></a>',
                    $link
                ),
                sprintf('<a href="%s">%s</a>', $link, $value["date_demande"]),
                sprintf('<a href="%s">%s</a>', $link, $value["raison_sociale"]),
                sprintf('<a href="%s">%s</a>', $link, $value["type_licence_libelle"])
            );
        }
        //
        echo sprint_table($table);
    }

    /**
     * @return void
     */
    function view_cerfa_declaration() {
        //
        $this->checkAccessibility();
        // Génération du PDF
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "cerfa_declaration",
            null,
            $this->getVal($this->clePrimaire),
            array()
        );
        // Affichage du PDF
        $this->expose_pdf_output(
            $pdfedition['pdf_output'],
            $pdfedition['filename']
        );
    }

    /**
     * @return void
     */
    function view_recepisse() {
        //
        $this->checkAccessibility();
        // Génération du PDF
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "recepisse",
            null,
            $this->getVal($this->clePrimaire),
            array()
        );
        // Affichage du PDF
        $this->expose_pdf_output(
            $pdfedition['pdf_output'],
            $pdfedition['filename']
        );
    }

    /**
     * @return void
     */
    function view_recepisse_co_exploitant() {
        //
        $this->checkAccessibility();
        // Génération du PDF
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "recepisse_co_exploitant",
            null,
            $this->getVal($this->clePrimaire),
            array()
        );
        // Affichage du PDF
        $this->expose_pdf_output(
            $pdfedition['pdf_output'],
            $pdfedition['filename']
        );
    }

    /**
     * @return void
     */
    function view_demande_temp_reponse() {
        //
        $this->checkAccessibility();
        // Génération du PDF
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "demande_temp_reponse",
            null,
            $this->getVal($this->clePrimaire),
            array()
        );
        // Affichage du PDF
        $this->expose_pdf_output(
            $pdfedition['pdf_output'],
            $pdfedition['filename']
        );
    }

    /**
     *
     */
    function view_widget_statistiques_licences_par_type() {
        //
        include "../sql/pgsql/statistiques_licences_par_type.reqmo.inc.php";
        $query = $reqmo["sql"];
        $res = $this->f->db->query($query);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        $results = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
        }
        //
        $data = array();
        foreach ($results as $key => $value) {
            $data[$key] = $value["nombre_licence"];
        }
        $data = implode(",", $data);
        //
        $labels = array();
        foreach ($results as $key => $value) {
            $labels[$key] = "\"".$value["type_licence"]."\"";
        }
        $labels = implode(",", $labels);
        //
        printf(
            '
            <h4>Nombre de licences par type</h4>
            <style>
            #canvas-holder{
                width: 100%% !important;
                min-width: 300px;
                height: auto !important;
            }
            </style>
            <script type="text/javascript">
            window.chartColors = {
                red: "rgb(255, 99, 132)",
                orange: "rgb(255, 159, 64)",
                yellow: "rgb(255, 205, 86)",
                green: "rgb(75, 192, 192)",
                blue: "rgb(54, 162, 235)",
                purple: "rgb(153, 102, 255)",
                grey: "rgb(201, 203, 207)"
            };
            var randomScalingFactor = function() {
                return Math.round(Math.random() * 100);
            };
            var config = {
                type: "pie",
                data: {
                    datasets: [{
                        data: [%1$s],
                        backgroundColor: [%2$s
                            window.chartColors.red,
                            window.chartColors.orange,
                            window.chartColors.yellow,
                            window.chartColors.green,
                            window.chartColors.blue,
                            window.chartColors.purple,
                            window.chartColors.grey,
                        ],
                        label: "Dataset 1"
                    }],
                    labels: [%3$s]
                },
                options: {
                    responsive: true,
                    animation: {
                        animateRotate: false
                    },
                    legend: {
                        position: "right"
                    }
                }
            };
            window.addEventListener("load", function() {
                var ctx = document.getElementById("chart-area").getContext("2d");
                window.myPie = new Chart(ctx, config);
            });
            </script>
            <div id="canvas-holder" style="width:100%%">
                <canvas id="chart-area"></canvas>
            </div>
            <a href="../app/index.php?module=reqmo&obj=statistiques_licences_par_type" title="Export CSV"><span class="om-icon om-icon-25 csv-25">Export CSV</span></a>
            ',
            $data,
            "",
            $labels
        );
    }

    /**
     *
     */
    function view_widget_statistiques_licences_par_type_filtre() {
        //
        include "../sql/pgsql/statistiques_licences_par_type_filtre.reqmo.inc.php";
        $query = $reqmo["sql"];
        $res = $this->f->db->query($query);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        $results = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
        }
        //
        $data = array();
        foreach ($results as $key => $value) {
            $data[$key] = $value["nombre_licence"];
        }
        $data = implode(",", $data);
        //
        $labels = array();
        foreach ($results as $key => $value) {
            $labels[$key] = "\"".$value["type_licence"]."\"";
        }
        $labels = implode(",", $labels);
        //
        printf(
            '
            <br/><br/>
            <h4>Nombre de licences par type filtré par la date de fin de validité et sans les demandes de transfert</h4>
            <style>
            #canvas2-holder{
                width: 100%% !important;
                min-width: 300px;
                height: auto !important;
            }
            </style>
            <script type="text/javascript">
            window.chartColors = {
                red: "rgb(255, 99, 132)",
                orange: "rgb(255, 159, 64)",
                yellow: "rgb(255, 205, 86)",
                green: "rgb(75, 192, 192)",
                blue: "rgb(54, 162, 235)",
                purple: "rgb(153, 102, 255)",
                grey: "rgb(201, 203, 207)"
            };
            var randomScalingFactor = function() {
                return Math.round(Math.random() * 100);
            };
            var config2 = {
                type: "pie",
                data: {
                    datasets: [{
                        data: [%1$s],
                        backgroundColor: [%2$s
                            window.chartColors.red,
                            window.chartColors.orange,
                            window.chartColors.yellow,
                            window.chartColors.green,
                            window.chartColors.blue,
                            window.chartColors.purple,
                            window.chartColors.grey,
                        ],
                        label: "Dataset 1"
                    }],
                    labels: [%3$s]
                },
                options: {
                    responsive: true,
                    animation: {
                        animateRotate: false
                    },
                    legend: {
                        position: "right"
                    }
                }
            };
            window.addEventListener("load", function() {
                var ctx2 = document.getElementById("chart2-area").getContext("2d");
                window.myPie2 = new Chart(ctx2, config2);
            });
            </script>
            <div id="canvas2-holder" style="width:100%%">
                <canvas id="chart2-area"></canvas>
            </div>
            <a href="../app/index.php?module=reqmo&obj=statistiques_licences_par_type_filtre" title="Export CSV"><span class="om-icon om-icon-25 csv-25">Export CSV</span></a>
            ',
            $data,
            "",
            $labels
        );
    }
}
