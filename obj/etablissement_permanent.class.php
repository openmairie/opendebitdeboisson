<?php
/**
 * Ce script définit la classe 'etablissement_permanent'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../obj/etablissement.class.php";

/**
 * Définition de la classe 'etablissement_permanent' (om_dbform).
 */
class etablissement_permanent extends etablissement {

    /**
     *
     */
    var $_absolute_class_name = "etablissement_permanent";

    /**
     * @return void
     */
    function init_class_actions() {
        etablissement_gen::init_class_actions();
        //
        $this->class_actions[1]["condition"][] = "is_permanent";
        $this->class_actions[2]["condition"][] = "is_permanent";
        $this->class_actions[3]["condition"][] = "is_permanent";
        //
        $this->class_actions[11] = array(
            "identifier" => "etablissement-map",
            "view" => "view_om_sig_map",
            "permission_suffix" => "consulter",
            "crud" => "read",
            "condition" => array(
                "exists",
                "is_permanent",
            ),
        );
        //
        $this->class_actions[12] = array(
            "identifier" => "etablissement-summary",
            "view" => "view_summary",
            "permission_suffix" => "consulter",
            "crud" => "read",
            "condition" => array(
                "exists",
                "is_permanent",
            ),
        );
    }

    /**
     * @return string
     */
    function get_default_om_sig_map() {
        return "etablissement_permanent";
    }

    /**
     * @return void
     */
    function form_specific_content_before_portlet_actions($maj) {
        if ($maj == 11 || $maj == 3) {
            printf(
                '<div id="switch-geolocaliser-consulter">
                <div class="switcher__label"><a class="om-prev-icon om-icon-16 consult-16 right" href="%s">%s</a></div>
                <div class="switcher__label"><a class="om-prev-icon om-icon-16 sig-16 right" href="%s">%s</a></div>
                <div class="switcher__toggle%s"></div>
                </div>',
                $this->compose_form_url("form", array("maj" => 3, "validation" => 0, )),
                __("consulter"),
                $this->compose_form_url("form", array("maj" => 11, "validation" => 0, )),
                __("géolocaliser"),
                ($maj == 11 ? " geolocaliser" : "")
            );
        }
    }

    /**
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $crud = $this->get_action_crud($maj);
        // Gestion de la nature
        if ($crud == 'create') {
            $form->setType("nature", "hiddenstatic");
        }
        // Gestion de l'adresse postale
        if ($crud == 'create' || $crud == 'update') {
            $form->setType("adresse_postale", "autocomplete");
            $form->setType("saisie_manuelle", "checkbox");
        }
        // Gestion de la récupération des informations de l'exploitant
        if ($crud == 'create' || $crud == 'update') {
            $form->setType("idem_exploitant_proprietaire", "checkbox");
        }
    }

    /**
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);

        // Gestion de l'adresse postale
        $form->setOnchange('adresse_postale', 'treatment_adresse_postale_fields(this, saisie_manuelle)');
        // Message dans la boîte de dialogue de confirmation pour la saisie
        // manuelle
        $title_form_confirmation = __('Revenir en saisie automatique ?');
        $msg_form_confirmation = __("Cette action videra les champs de l'adresse de l'établissement.");
        $form->setOnchange('saisie_manuelle', 'form_confirmation_adresse_postale_on_saisie_manuelle(adresse_postale, this, \''.addslashes($title_form_confirmation).'\', \''.addslashes($msg_form_confirmation).'\')');
        // Gestion de la récupération des informations de l'exploitant
        $form->setOnchange('idem_exploitant_proprietaire', 'treatment_exploitant_proprietaire_fields();clean_exploitant_proprietaire_fields();');
        $form->setOnchange('civilite_exploitant', 'treatment_exploitant_proprietaire_fields()');
        $form->setOnchange('nom_exploitant', 'treatment_exploitant_proprietaire_fields()');
        $form->setOnchange('prenom_exploitant', 'treatment_exploitant_proprietaire_fields()');
        $form->setOnchange('adresse1_exploitant', 'treatment_exploitant_proprietaire_fields()');
        $form->setOnchange('adresse2_exploitant', 'treatment_exploitant_proprietaire_fields()');
        $form->setOnchange('cp_exploitant', 'treatment_exploitant_proprietaire_fields()');
        $form->setOnchange('ville_exploitant', 'treatment_exploitant_proprietaire_fields()');
    }

    /**
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // Gestion de l'adresse postale
        $form->setSelect("adresse_postale", $this->get_widget_config__adresse_postale__autocomplete());
    }

    /**
     * @return array
     */
    function get_widget_config__adresse_postale__autocomplete() {
        return $this->get_generic_widget_config__adresse_postale__autocomplete();
    }

    /**
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        $crud = $this->get_action_crud($maj);
        //
        $form->setVal("nature", "permanent");
        // Gestion de l'adresse postale
        if ($crud == 'update') {
            $form->setVal("saisie_manuelle", "Oui");
        }
    }

    /**
     * Mutateur pour la propriété 'valF' en mode CREATE & UPDATE.
     *
     * @param array $val Tableau des valeurs brutes.
     *
     * @return void
     */
    function setvalF($val = array()) {
        parent::setvalF($val);
        // Gestion de la récupération du geom
        $msg_geom_change = __("Attention, suite au changement de l'adresse postale, la géolocalisation de l'établissement a été modifiée.");
        $this->setvalF_adresse_postale($val, $msg_geom_change);
    }

}
