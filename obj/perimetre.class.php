<?php
/**
 * Ce script définit la classe 'perimetre'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../gen/obj/perimetre.class.php";

/**
 * Définition de la classe 'perimetre' (om_dbform).
 */
class perimetre extends perimetre_gen {

    /**
     * @return void
     */
    function init_class_actions() {
        perimetre_gen::init_class_actions();
        //
        $this->class_actions[11] = array(
            "identifier" => "perimetre-map",
            "view" => "view_om_sig_map",
            "permission_suffix" => "consulter",
            "crud" => "read",
            "condition" => array(
                "exists",
            ),
        );
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "perimetre",
            "libelle",
            "longueur_exclusion_metre",
            "'' as adresse_postale",
            "'' as saisie_manuelle",
            "numero_voie",
            "complement",
            "voie",
            "libelle_voie",
            "complement_voie",
            "geom",
        );
    }

    /**
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $crud = $this->get_action_crud($maj);
        //
        $form->setType("geom", "hidden");
        // Gestion de l'adresse postale
        $form->setType("adresse_postale", "hidden");
        $form->setType("saisie_manuelle", "hidden");
        if ($crud == 'create' || $crud == 'update') {
            $form->setType("adresse_postale", "autocomplete");
            $form->setType("saisie_manuelle", "checkbox");
        }
    }

    /**
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);

        // Gestion de l'adresse postale
        $form->setOnchange('adresse_postale', 'treatment_adresse_postale_fields(this, saisie_manuelle)');
        // Message dans la boîte de dialogue de confirmation pour la saisie
        // manuelle
        $title_form_confirmation = __('Revenir en saisie automatique ?');
        $msg_form_confirmation = __("Cette action videra les champs de l'adresse du périmètre d'exclusion.");
        $form->setOnchange('saisie_manuelle', 'form_confirmation_adresse_postale_on_saisie_manuelle(adresse_postale, this, \''.addslashes($title_form_confirmation).'\', \''.addslashes($msg_form_confirmation).'\')');
    }

    /**
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // Gestion de l'adresse postale
        $form->setSelect("adresse_postale", $this->get_widget_config__adresse_postale__autocomplete());
    }

    /**
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("perimetre", __("id"));
        //
        $form->setLib("adresse_postale", __("adresse_postale"));
        $form->setLib("saisie_manuelle", __("saisie_manuelle"));
        $form->setLib("numero_voie", __("numéro"));
        $form->setLib("complement", __("complément de numéro"));
        $form->setLib("voie", __("code RIVOLI"));
        $form->setLib("libelle_voie", __("libellé de la voie"));
        $form->setLib("complement_voie", __("complément"));
    }

    /**
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        //
        $form->setBloc("perimetre", "D", __("caractéristiques"), "perimetre-bloc-caracteristiques col_12");
        $form->setBloc("longueur_exclusion_metre", "F");
        //
        $form->setBloc("adresse_postale", "D", __("adresse"), "etablissement-bloc-adresse col_12");
        $form->setBloc("complement_voie", "F");
    }

    /**
     * @return array
     */
    function get_widget_config__adresse_postale__autocomplete() {
        return $this->get_generic_widget_config__adresse_postale__autocomplete();
    }

    /**
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        $crud = $this->get_action_crud($maj);
        if ($maj == 0) {
            $form->setVal("longueur_exclusion_metre", 150);
        }
        // Gestion de l'adresse postale
        if ($crud == 'update') {
            $form->setVal("saisie_manuelle", "Oui");
        }
    }

    /**
     * Mutateur pour la propriété 'valF' en mode CREATE & UPDATE.
     *
     * @param array $val Tableau des valeurs brutes.
     *
     * @return void
     */
    function setvalF($val = array()) {
        parent::setvalF($val);
        // Gestion de la récupération du geom
        $msg_geom_change = __("Attention, suite au changement de l'adresse postale, la géolocalisation du périmètre d'exclusion a été modifiée.");
        $this->setvalF_adresse_postale($val, $msg_geom_change);
    }

    /**
     * @return void
     */
    function form_specific_content_before_portlet_actions($maj) {
        if ($maj == 11 || $maj == 3) {
            printf(
                '<div id="switch-geolocaliser-consulter">
                <div class="switcher__label"><a class="om-prev-icon om-icon-16 consult-16 right" href="%s">%s</a></div>
                <div class="switcher__label"><a class="om-prev-icon om-icon-16 sig-16 right" href="%s">%s</a></div>
                <div class="switcher__toggle%s"></div>
                </div>',
                $this->compose_form_url("form", array("maj" => 3, "validation" => 0, )),
                __("consulter"),
                $this->compose_form_url("form", array("maj" => 11, "validation" => 0, )),
                __("géolocaliser"),
                ($maj == 11 ? " geolocaliser" : "")
            );
        }
    }

}
