openDébitDeBoisson
==================

Description
-----------

openDébitDeBoisson est un logiciel destiné au service de gestion des débits de
boisson et a pour objectif de gérer les demandes de licences de débit de
boisson en tenant compte : des périmètres d'exclusion, du type de demande de
licence, de la géolocalisation de l'établissement, des établissements
environnants ayant une même licence, des dates de demande de licence, des
documents fournis, générer les récépissés de ces même demandes et bien plus
encore...


Documentation
-------------

Toutes les instructions pour l'installation, l'utilisation et la prise en main
du logiciel dans la documentation en ligne :
http://docs.openmairie.org/?project=opendebitdeboisson


Licence
-------

Voir le fichier LICENSE.txt.

