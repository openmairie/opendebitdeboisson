<?php
/**
 * Ce fichier permet de faire une redirection vers la page de login de
 * l'application.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

//
$came_from = "";
if (isset($_GET['came_from'])) {
    $came_from = $_GET['came_from'];
}

//
header("Location: app/index.php?module=login&came_from=".urlencode($came_from));
