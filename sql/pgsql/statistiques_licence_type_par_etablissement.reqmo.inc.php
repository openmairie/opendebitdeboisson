<?php
/**
 * ...
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

//
$reqmo['terme'] = sprintf(
    'SELECT terme, libelle 
    FROM %sterme',
    DB_PREFIXE
);
$reqmo["sql"] = sprintf(
    'SELECT
        [etablissement.etablissement as etablissement_id]
        [, etablissement.enseigne as etablissement_enseigne]
        [, etablissement.raison_sociale as etablissement_raison_sociale],
        type_licence.libelle as type_licence_libelle
    FROM
        %1$sdemande_licence
        INNER JOIN %1$setablissement
            ON demande_licence.etablissement = etablissement.etablissement
        LEFT JOIN %1$stype_licence
            ON demande_licence.type_licence = type_licence.type_licence
    WHERE
        demande_licence.terme = [terme]
    ORDER BY
        type_licence_libelle',
    DB_PREFIXE
);
//
$reqmo['etablissement_id'] = 'checked';
$reqmo['etablissement_enseigne'] = 'checked';
$reqmo['etablissement_raison_sociale'] = 'checked';
