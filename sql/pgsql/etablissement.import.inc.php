<?php
//$Id$ 
//gen openMairie le 28/12/2018 17:13

$import= "Insertion dans la table etablissement voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."etablissement";
$id='etablissement'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "etablissement" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
    ),
    "raison_sociale" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "50",
    ),
    "enseigne" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "50",
    ),
    "no_siret" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "14",
    ),
    "forme_juridique" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "20",
    ),
    "nature" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "15",
    ),
    "type_etablissement" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "type_etablissement",
            "foreign_column_name" => "type_etablissement",
            "sql_exist" => "select * from ".DB_PREFIXE."type_etablissement where type_etablissement = '",
            "foreign_key_alias" => array(
                "query" => "select type_etablissement from ".DB_PREFIXE."type_etablissement where libelle = '<SEARCH>'",
                "fields_list" => array("libelle", ),
            ),
        ),
    ),
    "permis_exploitation" => array(
        "notnull" => "1",
        "type" => "bool",
        "len" => "1",
    ),
    "date_permis_exploitation" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "permis_exploitation_nuit" => array(
        "notnull" => "1",
        "type" => "bool",
        "len" => "1",
    ),
    "date_permis_exploitation_nuit" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "numero_voie" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
    ),
    "complement" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "10",
    ),
    "voie" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "libelle_voie" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "complement_voie" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "cp_etablissement" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "ville_etablissement" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "geom" => array(
        "notnull" => "",
        "type" => "geom",
        "len" => "551424",
    ),
    "civilite_exploitant" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "titre_de_civilite",
            "foreign_column_name" => "titre_de_civilite",
            "sql_exist" => "select * from ".DB_PREFIXE."titre_de_civilite where titre_de_civilite = '",
            "foreign_key_alias" => array(
                "query" => "select titre_de_civilite from ".DB_PREFIXE."titre_de_civilite where code = '<SEARCH>'",
                "fields_list" => array("code", ),
            ),
        ),
    ),
    "nom_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "nom_marital_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "prenom_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "date_naissance_exploitant" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "ville_naissance_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "cp_naissance_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "adresse1_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "adresse2_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "cp_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "ville_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "qualite_exploitant" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "qualite_exploitant",
            "foreign_column_name" => "qualite_exploitant",
            "sql_exist" => "select * from ".DB_PREFIXE."qualite_exploitant where qualite_exploitant = '",
            "foreign_key_alias" => array(
                "query" => "select qualite_exploitant from ".DB_PREFIXE."qualite_exploitant where libelle = '<SEARCH>'",
                "fields_list" => array("libelle", ),
            ),
        ),
    ),
    "nationalite_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "particularite_exploitant" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "civilite_co_exploitant" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "titre_de_civilite",
            "foreign_column_name" => "titre_de_civilite",
            "sql_exist" => "select * from ".DB_PREFIXE."titre_de_civilite where titre_de_civilite = '",
            "foreign_key_alias" => array(
                "query" => "select titre_de_civilite from ".DB_PREFIXE."titre_de_civilite where code = '<SEARCH>'",
                "fields_list" => array("code", ),
            ),
        ),
    ),
    "nom_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "nom_marital_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "prenom_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "date_naissance_co_exploitant" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "ville_naissance_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "cp_naissance_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "adresse1_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "adresse2_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "cp_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "ville_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "nationalite_co_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "qualite_co_exploitant" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "qualite_exploitant",
            "foreign_column_name" => "qualite_exploitant",
            "sql_exist" => "select * from ".DB_PREFIXE."qualite_exploitant where qualite_exploitant = '",
            "foreign_key_alias" => array(
                "query" => "select qualite_exploitant from ".DB_PREFIXE."qualite_exploitant where libelle = '<SEARCH>'",
                "fields_list" => array("libelle", ),
            ),
        ),
    ),
    "particularite_co_exploitant" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "nom_ancien_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "prenom_ancien_exploitant" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "qualite_ancien_exploitant" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "qualite_exploitant",
            "foreign_column_name" => "qualite_exploitant",
            "sql_exist" => "select * from ".DB_PREFIXE."qualite_exploitant where qualite_exploitant = '",
            "foreign_key_alias" => array(
                "query" => "select qualite_exploitant from ".DB_PREFIXE."qualite_exploitant where libelle = '<SEARCH>'",
                "fields_list" => array("libelle", ),
            ),
        ),
    ),
    "civilite_proprietaire" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "titre_de_civilite",
            "foreign_column_name" => "titre_de_civilite",
            "sql_exist" => "select * from ".DB_PREFIXE."titre_de_civilite where titre_de_civilite = '",
            "foreign_key_alias" => array(
                "query" => "select titre_de_civilite from ".DB_PREFIXE."titre_de_civilite where code = '<SEARCH>'",
                "fields_list" => array("code", ),
            ),
        ),
    ),
    "nom_proprietaire" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "prenom_proprietaire" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "adresse1_proprietaire" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "adresse2_proprietaire" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "cp_proprietaire" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "ville_proprietaire" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "profession_proprietaire" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "date_fermeture" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "date_liquidation" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "observation" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "ancien_proprietaire" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "250",
    ),
);
