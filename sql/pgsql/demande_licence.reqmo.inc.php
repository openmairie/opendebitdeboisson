<?php
//$Id$ 
//gen openMairie le 11/09/2018 18:57

$reqmo['sql'] = sprintf('
    SELECT
        [demande_licence],
        [numero_licence],
        [type_licence.libelle as type_licence],
        [terme.libelle as terme],
        [date_debut_validite],
        [date_fin_validite],
        [etablissement.etablissement as etablissement_id],
        [etablissement.raison_sociale as etablissement_raison_sociale],
        [etablissement.enseigne as etablissement_enseigne],
        [heure_debut],
        [heure_fin],
        [type_demande.libelle as type_demande]
    FROM %1$sdemande_licence
    INNER JOIN %1$setablissement
        ON demande_licence.etablissement = etablissement.etablissement
    INNER JOIN %1$stype_licence
        ON demande_licence.type_licence = type_licence.type_licence
    INNER JOIN %1$sterme
        ON demande_licence.terme = terme.terme
    INNER JOIN %1$stype_demande
        ON demande_licence.type_demande = type_demande.type_demande
    ORDER BY
        [tri]',
    DB_PREFIXE
);
$reqmo['demande_licence']='checked';
$reqmo['type_demande']='checked';
$reqmo['type_licence']='checked';
$reqmo['numero_licence']='checked';
$reqmo['terme']='checked';
$reqmo['date_debut_validite']='checked';
$reqmo['heure_debut']='checked';
$reqmo['date_fin_validite']='checked';
$reqmo['heure_fin']='checked';
$reqmo['etablissement_id']='checked';
$reqmo['etablissement_raison_sociale']='checked';
$reqmo['etablissement_enseigne']='checked';
$reqmo['tri']=array('demande_licence', 'numero_licence', 'type_licence', 'terme', 'date_debut_validite', 'date_fin_validite', 'etablissement', 'heure_debut', 'heure_fin', 'type_demande');