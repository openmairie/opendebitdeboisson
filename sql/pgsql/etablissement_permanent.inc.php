<?php
/**
 * ...
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/etablissement.inc.php";

//
$champAffiche[] = 'trim(concat_ws(\' \', titre_de_civilite2.libelle, etablissement.prenom_proprietaire, etablissement.nom_proprietaire)) as "'.__("propriétaire").'"';

//
if (trim($selection) !== "") {
    $selection .= " AND etablissement.nature='permanent' ";
} else {
    $selection = " WHERE etablissement.nature='permanent' ";
}
