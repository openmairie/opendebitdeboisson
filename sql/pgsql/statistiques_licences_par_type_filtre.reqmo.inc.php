<?php
/**
 * ...
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

//
$reqmo["sql"] = sprintf(
    'SELECT
        type_licence.libelle as type_licence,
        COUNT(*) as nombre_licence
    FROM
        %1$sdemande_licence
        INNER JOIN %1$setablissement
            ON demande_licence.etablissement = etablissement.etablissement
        LEFT JOIN %1$stype_licence
            ON demande_licence.type_licence = type_licence.type_licence
        INNER JOIN %1$sterme
            ON demande_licence.terme = terme.terme
        INNER JOIN %1$stype_demande
            ON demande_licence.type_demande = type_demande.type_demande
    WHERE
        LOWER(terme.code) = \'perm\'
        AND LOWER(type_demande.code) != \'trs\'
        AND demande_licence.date_fin_validite > CURRENT_DATE
    GROUP BY 
        type_licence.libelle
    ORDER BY
        type_licence',
    DB_PREFIXE
);