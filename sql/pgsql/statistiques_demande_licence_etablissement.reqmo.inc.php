<?php
/**
 * ...
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

$reqmo['sql'] = sprintf('
    SELECT
        [demande_licence],
        [numero_licence],
        [type_licence.libelle as type_licence],
        [terme.libelle as terme],
        [date_debut_validite],
        [date_fin_validite],
        [heure_debut],
        [heure_fin],
        [type_demande.libelle as type_demande],
        [etablissement.etablissement as etablissement_id],
        [etablissement.raison_sociale as etablissement_raison_sociale],
        [etablissement.enseigne as etablissement_enseigne],
        [etablissement.no_siret as etablissement_no_siret],
        [etablissement.forme_juridique as etablissement_forme_juridique],
        [etablissement.nature as etablissement_nature],
        [etablissement.type_etablissement as etablissement_type_etablissement],
        [etablissement.permis_exploitation as etablissement_permis_exploitation],
        [etablissement.date_permis_exploitation as etablissement_date_permis_exploitation],
        [etablissement.permis_exploitation_nuit as etablissement_permis_exploitation_nuit],
        [etablissement.date_permis_exploitation_nuit as etablissement_date_permis_exploitation_nuit],
        [etablissement.numero_voie as etablissement_numero_voie],
        [etablissement.complement as etablissement_complement],
        [etablissement.voie as etablissement_voie],
        [etablissement.libelle_voie as etablissement_libelle_voie],
        [etablissement.complement_voie as etablissement_complement_voie],
        [etablissement.cp_etablissement as etablissement_cp_etablissement],
        [etablissement.ville_etablissement as etablissement_ville_etablissement],
        [etablissement.geom as etablissement_geom],
        [etablissement.civilite_exploitant as etablissement_civilite_exploitant],
        [etablissement.nom_exploitant as etablissement_nom_exploitant],
        [etablissement.nom_marital_exploitant as etablissement_nom_marital_exploitant],
        [etablissement.prenom_exploitant as etablissement_prenom_exploitant],
        [etablissement.date_naissance_exploitant as etablissement_date_naissance_exploitant],
        [etablissement.ville_naissance_exploitant as etablissement_ville_naissance_exploitant],
        [etablissement.cp_naissance_exploitant as etablissement_cp_naissance_exploitant],
        [etablissement.adresse1_exploitant as etablissement_adresse1_exploitant],
        [etablissement.adresse2_exploitant as etablissement_adresse2_exploitant],
        [etablissement.cp_exploitant as etablissement_cp_exploitant],
        [etablissement.ville_exploitant as etablissement_ville_exploitant],
        [etablissement.qualite_exploitant as etablissement_qualite_exploitant],
        [etablissement.nationalite_exploitant as etablissement_nationalite_exploitant],
        [etablissement.particularite_exploitant as etablissement_particularite_exploitant],
        [etablissement.civilite_co_exploitant as etablissement_civilite_co_exploitant],
        [etablissement.nom_co_exploitant as etablissement_nom_co_exploitant],
        [etablissement.nom_marital_co_exploitant as etablissement_nom_marital_co_exploitant],
        [etablissement.prenom_co_exploitant as etablissement_prenom_co_exploitant],
        [etablissement.date_naissance_co_exploitant as etablissement_date_naissance_co_exploitant],
        [etablissement.ville_naissance_co_exploitant as etablissement_ville_naissance_co_exploitant],
        [etablissement.cp_naissance_co_exploitant as etablissement_cp_naissance_co_exploitant],
        [etablissement.adresse1_co_exploitant as etablissement_adresse1_co_exploitant],
        [etablissement.adresse2_co_exploitant as etablissement_adresse2_co_exploitant],
        [etablissement.cp_co_exploitant as etablissement_cp_co_exploitant],
        [etablissement.ville_co_exploitant as etablissement_ville_co_exploitant],
        [etablissement.nationalite_co_exploitant as etablissement_nationalite_co_exploitant],
        [etablissement.qualite_co_exploitant as etablissement_qualite_co_exploitant],
        [etablissement.particularite_co_exploitant as etablissement_particularite_co_exploitant],
        [etablissement.nom_ancien_exploitant as etablissement_nom_ancien_exploitant],
        [etablissement.prenom_ancien_exploitant as etablissement_prenom_ancien_exploitant],
        [etablissement.qualite_ancien_exploitant as etablissement_qualite_ancien_exploitant],
        [etablissement.civilite_proprietaire as etablissement_civilite_proprietaire],
        [etablissement.nom_proprietaire as etablissement_nom_proprietaire],
        [etablissement.prenom_proprietaire as etablissement_prenom_proprietaire],
        [etablissement.adresse1_proprietaire as etablissement_adresse1_proprietaire],
        [etablissement.adresse2_proprietaire as etablissement_adresse2_proprietaire],
        [etablissement.cp_proprietaire as etablissement_cp_proprietaire],
        [etablissement.ville_proprietaire as etablissement_ville_proprietaire],
        [etablissement.profession_proprietaire as etablissement_profession_proprietaire],
        [etablissement.date_fermeture as etablissement_date_fermeture],
        [etablissement.date_liquidation as etablissement_date_liquidation],
        [etablissement.observation as etablissement_observation],
        [etablissement.ancien_proprietaire as etablissement_ancien_proprietaire]
    FROM %1$sdemande_licence
    INNER JOIN %1$setablissement
        ON demande_licence.etablissement = etablissement.etablissement
    INNER JOIN %1$stype_licence
        ON demande_licence.type_licence = type_licence.type_licence
    INNER JOIN %1$sterme
        ON demande_licence.terme = terme.terme
    INNER JOIN %1$stype_demande
        ON demande_licence.type_demande = type_demande.type_demande
    ORDER BY
        [tri]',
    DB_PREFIXE
);
$reqmo['demande_licence']='checked';
$reqmo['type_demande']='checked';
$reqmo['type_licence']='checked';
$reqmo['numero_licence']='checked';
$reqmo['terme']='checked';
$reqmo['date_debut_validite']='checked';
$reqmo['heure_debut']='checked';
$reqmo['date_fin_validite']='checked';
$reqmo['heure_fin']='checked';
$reqmo['etablissement_id']='checked';
$reqmo['etablissement_raison_sociale']='checked';
$reqmo['etablissement_enseigne']='checked';
$reqmo['etablissement_no_siret']='checked';
$reqmo['etablissement_forme_juridique']='checked';
$reqmo['etablissement_nature']='checked';
$reqmo['etablissement_type_etablissement']='checked';
$reqmo['etablissement_permis_exploitation']='checked';
$reqmo['etablissement_date_permis_exploitation']='checked';
$reqmo['etablissement_permis_exploitation_nuit']='checked';
$reqmo['etablissement_date_permis_exploitation_nuit']='checked';
$reqmo['etablissement_numero_voie']='checked';
$reqmo['etablissement_complement']='checked';
$reqmo['etablissement_voie']='checked';
$reqmo['etablissement_libelle_voie']='checked';
$reqmo['etablissement_complement_voie']='checked';
$reqmo['etablissement_cp_etablissement']='checked';
$reqmo['etablissement_ville_etablissement']='checked';
$reqmo['etablissement_geom']='checked';
$reqmo['etablissement_civilite_exploitant']='checked';
$reqmo['etablissement_nom_exploitant']='checked';
$reqmo['etablissement_nom_marital_exploitant']='checked';
$reqmo['etablissement_prenom_exploitant']='checked';
$reqmo['etablissement_date_naissance_exploitant']='checked';
$reqmo['etablissement_ville_naissance_exploitant']='checked';
$reqmo['etablissement_cp_naissance_exploitant']='checked';
$reqmo['etablissement_adresse1_exploitant']='checked';
$reqmo['etablissement_adresse2_exploitant']='checked';
$reqmo['etablissement_cp_exploitant']='checked';
$reqmo['etablissement_ville_exploitant']='checked';
$reqmo['etablissement_qualite_exploitant']='checked';
$reqmo['etablissement_nationalite_exploitant']='checked';
$reqmo['etablissement_particularite_exploitant']='checked';
$reqmo['etablissement_civilite_co_exploitant']='checked';
$reqmo['etablissement_nom_co_exploitant']='checked';
$reqmo['etablissement_nom_marital_co_exploitant']='checked';
$reqmo['etablissement_prenom_co_exploitant']='checked';
$reqmo['etablissement_date_naissance_co_exploitant']='checked';
$reqmo['etablissement_ville_naissance_co_exploitant']='checked';
$reqmo['etablissement_cp_naissance_co_exploitant']='checked';
$reqmo['etablissement_adresse1_co_exploitant']='checked';
$reqmo['etablissement_adresse2_co_exploitant']='checked';
$reqmo['etablissement_cp_co_exploitant']='checked';
$reqmo['etablissement_ville_co_exploitant']='checked';
$reqmo['etablissement_nationalite_co_exploitant']='checked';
$reqmo['etablissement_qualite_co_exploitant']='checked';
$reqmo['etablissement_particularite_co_exploitant']='checked';
$reqmo['etablissement_nom_ancien_exploitant']='checked';
$reqmo['etablissement_prenom_ancien_exploitant']='checked';
$reqmo['etablissement_qualite_ancien_exploitant']='checked';
$reqmo['etablissement_civilite_proprietaire']='checked';
$reqmo['etablissement_nom_proprietaire']='checked';
$reqmo['etablissement_prenom_proprietaire']='checked';
$reqmo['etablissement_adresse1_proprietaire']='checked';
$reqmo['etablissement_adresse2_proprietaire']='checked';
$reqmo['etablissement_cp_proprietaire']='checked';
$reqmo['etablissement_ville_proprietaire']='checked';
$reqmo['etablissement_profession_proprietaire']='checked';
$reqmo['etablissement_date_fermeture']='checked';
$reqmo['etablissement_date_liquidation']='checked';
$reqmo['etablissement_observation']='checked';
$reqmo['etablissement_ancien_proprietaire']='checked';
$reqmo['tri']=array('demande_licence', 'numero_licence', 'type_licence', 'terme', 'date_debut_validite', 'date_fin_validite', 'etablissement', 'heure_debut', 'heure_fin', 'type_demande');
