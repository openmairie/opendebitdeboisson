<?php
/**
 * ...
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/etablissement.inc.php";

//
$tab_title = __("établissement");

//
if (isset($options) !== true) {
    $options = array();
}

//
$champAffiche = array(
    'etablissement.etablissement as "'.__("id").'"',
    'etablissement.raison_sociale as "'.__("raison sociale").'"',
    'etablissement.enseigne as "'.__("enseigne").'"',
    'trim(concat_ws(\' \', etablissement.numero_voie, etablissement.complement, etablissement.libelle_voie)) as "'.__("adresse").'"',
    'trim(concat_ws(\' \', titre_de_civilite1.libelle, etablissement.prenom_exploitant, etablissement.nom_exploitant)) as "'.__("exploitant").'"',
);

//
$sousformulaire = array(
    "demande_licence",
    "dossier",
    "courrier",
);
//
$tab_actions['left']["geolocaliser"] = array(
    'lien' => OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=11&amp;idx=',
    'id' => '&amp;advs_id='.$advs_id.'&amp;premier='.$premier.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('géolocaliser').'">'.__('géolocaliser').'</span>',
    'rights' => array('list' => array('etablissement', 'etablissement_tab'), 'operator' => 'OR'),
    'ordre' => 20,
);

// Recherche avancée
$args = array(
    0 => array('', 'true', 'false'),
    1 => array(__('Tous'), __('Oui'), __('Non')),
);
$subquery = 'SELECT etablissement.etablissement FROM '.DB_PREFIXE.'etablissement WHERE etablissement.geom IS NOT NULL';
$advs_fields = array(
    "identifiant_etablissement" => array(
        "colonne" => "etablissement",
        "table" => "etablissement",
        "type" => "text",
        "libelle" => __("id"),
        "taille" => 10,
        "max" => 8,
    ),
    "raison_sociale" => array(
        "colonne" => "raison_sociale",
        "table" => "etablissement",
        "type" => "text",
        "libelle" => __("raison sociale"),
        "taille" => 10,
        "max" => 8,
    ),
    "enseigne" => array(
        "colonne" => "enseigne",
        "table" => "etablissement",
        "type" => "text",
        "libelle" => __("enseigne"),
        "taille" => 10,
        "max" => 8,
    ),
    "libelle_voie" => array(
        "colonne" => "libelle_voie",
        "table" => "etablissement",
        "type" => "text",
        "libelle" => __("libelle_voie"),
        "taille" => 10,
        "max" => 8,
    ),
    "nom_exploitant" => array(
        "colonne" => "nom_exploitant",
        "table" => "etablissement",
        "type" => "text",
        "libelle" => __("nom_exploitant"),
        "taille" => 10,
        "max" => 8,
    ),
    "nom_proprietaire" => array(
        "colonne" => "nom_proprietaire",
        "table" => "etablissement",
        "type" => "text",
        "libelle" => __("nom_proprietaire"),
        "taille" => 10,
        "max" => 8,
    ),
    "type_etablissement" => array(
        "colonne" => "type_etablissement",
        "table" => "etablissement",
        "type" => "select",
        "libelle" => __("type_etablissement"),
    ),
    "geolocalise" => array(
        "colonne" => "etablissement",
        "table" => "etablissement",
        "type" => "select",
        "subtype" => "manualselect",
        "where" => "insubquery",
        "args" => $args,
        "libelle" => __("géolocalisé"),
        "subquery" => $subquery,
    ),
);
$options[] = array(
    "type" => "search",
    "display" => true,
    "advanced" => $advs_fields,
    "export" => array("csv", ),
    "absolute_object" => "etablissement",
);

// Tri du listing des établissements
$tri="ORDER BY etablissement.etablissement ASC NULLS LAST";
