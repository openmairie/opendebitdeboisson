<?php
/**
 * ...
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/etablissement.inc.php";

//
$ent = __("Application")." -> ".__("Établissements temporaires");

//
$tab_title = __("établissement temporaire");

//
if (trim($selection) !== "") {
    $selection .= " AND etablissement.nature='temporaire' ";
} else {
    $selection = " WHERE etablissement.nature='temporaire' ";
}

// Pas de géolocalisation sur les établissements temporaires
unset($tab_actions['left']["geolocaliser"]);
