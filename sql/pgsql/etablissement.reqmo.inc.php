<?php
//$Id$ 
//gen openMairie le 22/10/2018 17:13

$reqmo['libelle'] = __('reqmo-libelle-etablissement');
$reqmo['reqmo_libelle'] = __('reqmo-libelle-etablissement');
$ent = __('etablissement');
$reqmo['sql']="select  [etablissement], [raison_sociale], [enseigne], [no_siret], [forme_juridique], [nature], [type_etablissement], [permis_exploitation], [date_permis_exploitation], [permis_exploitation_nuit], [date_permis_exploitation_nuit], [numero_voie], [complement], [voie], [libelle_voie], [complement_voie], [cp_etablissement], [ville_etablissement], [geom], [civilite_exploitant], [nom_exploitant], [nom_marital_exploitant], [prenom_exploitant], [date_naissance_exploitant], [ville_naissance_exploitant], [cp_naissance_exploitant], [adresse1_exploitant], [adresse2_exploitant], [cp_exploitant], [ville_exploitant], [qualite_exploitant], [nationalite_exploitant], [particularite_exploitant], [civilite_co_exploitant], [nom_co_exploitant], [nom_marital_co_exploitant], [prenom_co_exploitant], [date_naissance_co_exploitant], [ville_naissance_co_exploitant], [cp_naissance_co_exploitant], [adresse1_co_exploitant], [adresse2_co_exploitant], [cp_co_exploitant], [ville_co_exploitant], [nationalite_co_exploitant], [qualite_co_exploitant], [particularite_co_exploitant], [nom_ancien_exploitant], [prenom_ancien_exploitant], [qualite_ancien_exploitant], [civilite_proprietaire], [nom_proprietaire], [prenom_proprietaire], [adresse1_proprietaire], [adresse2_proprietaire], [cp_proprietaire], [ville_proprietaire], [profession_proprietaire], [date_fermeture], [date_liquidation], [observation], [ancien_proprietaire] from ".DB_PREFIXE."etablissement  order by [tri]";
$reqmo['etablissement']='checked';
$reqmo['raison_sociale']='checked';
$reqmo['enseigne']='checked';
$reqmo['no_siret']='checked';
$reqmo['forme_juridique']='checked';
$reqmo['nature']='checked';
$reqmo['type_etablissement']='checked';
$reqmo['permis_exploitation']='checked';
$reqmo['date_permis_exploitation']='checked';
$reqmo['permis_exploitation_nuit']='checked';
$reqmo['date_permis_exploitation_nuit']='checked';
$reqmo['numero_voie']='checked';
$reqmo['complement']='checked';
$reqmo['voie']='checked';
$reqmo['libelle_voie']='checked';
$reqmo['complement_voie']='checked';
$reqmo['cp_etablissement']='checked';
$reqmo['ville_etablissement']='checked';
$reqmo['geom']='checked';
$reqmo['civilite_exploitant']='checked';
$reqmo['nom_exploitant']='checked';
$reqmo['nom_marital_exploitant']='checked';
$reqmo['prenom_exploitant']='checked';
$reqmo['date_naissance_exploitant']='checked';
$reqmo['ville_naissance_exploitant']='checked';
$reqmo['cp_naissance_exploitant']='checked';
$reqmo['adresse1_exploitant']='checked';
$reqmo['adresse2_exploitant']='checked';
$reqmo['cp_exploitant']='checked';
$reqmo['ville_exploitant']='checked';
$reqmo['qualite_exploitant']='checked';
$reqmo['nationalite_exploitant']='checked';
$reqmo['particularite_exploitant']='checked';
$reqmo['civilite_co_exploitant']='checked';
$reqmo['nom_co_exploitant']='checked';
$reqmo['nom_marital_co_exploitant']='checked';
$reqmo['prenom_co_exploitant']='checked';
$reqmo['date_naissance_co_exploitant']='checked';
$reqmo['ville_naissance_co_exploitant']='checked';
$reqmo['cp_naissance_co_exploitant']='checked';
$reqmo['adresse1_co_exploitant']='checked';
$reqmo['adresse2_co_exploitant']='checked';
$reqmo['cp_co_exploitant']='checked';
$reqmo['ville_co_exploitant']='checked';
$reqmo['nationalite_co_exploitant']='checked';
$reqmo['qualite_co_exploitant']='checked';
$reqmo['particularite_co_exploitant']='checked';
$reqmo['nom_ancien_exploitant']='checked';
$reqmo['prenom_ancien_exploitant']='checked';
$reqmo['qualite_ancien_exploitant']='checked';
$reqmo['civilite_proprietaire']='checked';
$reqmo['nom_proprietaire']='checked';
$reqmo['prenom_proprietaire']='checked';
$reqmo['adresse1_proprietaire']='checked';
$reqmo['adresse2_proprietaire']='checked';
$reqmo['cp_proprietaire']='checked';
$reqmo['ville_proprietaire']='checked';
$reqmo['profession_proprietaire']='checked';
$reqmo['date_fermeture']='checked';
$reqmo['date_liquidation']='checked';
$reqmo['observation']='checked';
$reqmo['ancien_proprietaire']='checked';
$reqmo['tri']=array('etablissement','raison_sociale','enseigne','no_siret','forme_juridique','nature','type_etablissement','permis_exploitation','date_permis_exploitation','permis_exploitation_nuit','date_permis_exploitation_nuit','numero_voie','complement','voie','libelle_voie','complement_voie','cp_etablissement','ville_etablissement','geom','civilite_exploitant','nom_exploitant','nom_marital_exploitant','prenom_exploitant','date_naissance_exploitant','ville_naissance_exploitant','cp_naissance_exploitant','adresse1_exploitant','adresse2_exploitant','cp_exploitant','ville_exploitant','qualite_exploitant','nationalite_exploitant','particularite_exploitant','civilite_co_exploitant','nom_co_exploitant','nom_marital_co_exploitant','prenom_co_exploitant','date_naissance_co_exploitant','ville_naissance_co_exploitant','cp_naissance_co_exploitant','adresse1_co_exploitant','adresse2_co_exploitant','cp_co_exploitant','ville_co_exploitant','nationalite_co_exploitant','qualite_co_exploitant','particularite_co_exploitant','nom_ancien_exploitant','prenom_ancien_exploitant','qualite_ancien_exploitant','civilite_proprietaire','nom_proprietaire','prenom_proprietaire','adresse1_proprietaire','adresse2_proprietaire','cp_proprietaire','ville_proprietaire','profession_proprietaire','date_fermeture','date_liquidation','observation','ancien_proprietaire');
