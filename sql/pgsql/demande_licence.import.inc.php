<?php
//$Id$ 
//gen openMairie le 22/08/2018 15:09

$import= "Insertion dans la table demande_licence voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."demande_licence";
$id='demande_licence'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "demande_licence" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
    ),
    "date_demande_licence" => array(
        "notnull" => "1",
        "type" => "date",
        "len" => "12",
    ),
    "date_ancienne_demande" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "type_demande" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "type_demande",
            "foreign_column_name" => "type_demande",
            "sql_exist" => "select * from ".DB_PREFIXE."type_demande where type_demande = '",
            "foreign_key_alias" => array(
                "query" => "select type_demande from ".DB_PREFIXE."type_demande where code = '<SEARCH>'",
                "fields_list" => array("code", ),
            ),
        ),
    ),
    "type_licence" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "type_licence",
            "foreign_column_name" => "type_licence",
            "sql_exist" => "select * from ".DB_PREFIXE."type_licence where type_licence = '",
            "foreign_key_alias" => array(
                "query" => "select type_licence from ".DB_PREFIXE."type_licence where libelle = '<SEARCH>'",
                "fields_list" => array("libelle", ),
            ),
        ),
    ),
    "compte_rendu" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "statut_demande" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "statut_demande",
            "foreign_column_name" => "statut_demande",
            "sql_exist" => "select * from ".DB_PREFIXE."statut_demande where statut_demande = '",
        ),
    ),
    "numero_licence" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "50",
    ),
    "occasion" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "50",
    ),
    "particularite" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "terme" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "terme",
            "foreign_column_name" => "terme",
            "sql_exist" => "select * from ".DB_PREFIXE."terme where terme = '",
        ),
    ),
    "date_debut_validite" => array(
        "notnull" => "1",
        "type" => "date",
        "len" => "12",
    ),
    "heure_debut" => array(
        "notnull" => "1",
        "type" => "time",
        "len" => "8",
    ),
    "date_fin_validite" => array(
        "notnull" => "1",
        "type" => "date",
        "len" => "12",
    ),
    "heure_fin" => array(
        "notnull" => "1",
        "type" => "time",
        "len" => "8",
    ),
    "etablissement" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "etablissement",
            "foreign_column_name" => "etablissement",
            "sql_exist" => "select * from ".DB_PREFIXE."etablissement where etablissement = '",
        ),
    ),
);
