<?php
/**
 * ...
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier.inc.php";

//
if (isset($options) !== true) {
    $options = array();
}

//
$champAffiche[0] = 'dossier.dossier as "'.__("id").'"';
$champRecherche[0] = 'dossier.dossier as "'.__("id").'"';

//
$tab_actions['left']["telecharger"] = array(
    'lien' => OM_ROUTE_FORM.'&snippet=file&obj='.$obj.'&champ=fichier&id=',
    'id' => '',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix download-16" title="'.__('Télécharger').'">'.__('Télécharger').'</span>',
    'rights' => array('list' => array('dossier', 'dossier_fichier_telecharger'), 'operator' => 'OR'),
    'ordre' => 20,
    "target" => "_blank",
);
