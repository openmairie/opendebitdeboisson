<?php
/**
 * ...
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/courrier.inc.php";

//
if (isset($options) !== true) {
    $options = array();
}

//
$champAffiche[0] = 'courrier.courrier as "'.__("id").'"';
$champRecherche[0] = 'courrier.courrier as "'.__("id").'"';

//
$tab_actions['left']["telecharger"] = array(
    'lien' => OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=6&amp;idx=',
    'id' => '&amp;advs_id='.$advs_id.'&amp;premier='.$premier.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix pdf-16" title="'.__('Télécharger').'">'.__('Télécharger').'</span>',
    'rights' => array('list' => array('courrier', 'courrier_fichier_telecharger'), 'operator' => 'OR'),
    'ordre' => 20,
    "target" => "_blank",
);
