<?php
/**
 * ...
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/perimetre.inc.php";

//
if (isset($options) !== true) {
    $options = array();
}

//
$tab_title = __("périmètre");

//
$champAffiche[0] = 'perimetre.perimetre as "'.__("id").'"';
$champRecherche[0] = 'perimetre.perimetre as "'.__("id").'"';

//
$tab_actions['left']["geolocaliser"] = array(
    'lien' => OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=11&amp;idx=',
    'id' => '&amp;advs_id='.$advs_id.'&amp;premier='.$premier.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('géolocaliser').'">'.__('géolocaliser').'</span>',
    'rights' => array('list' => array('perimetre', 'perimetre_tab'), 'operator' => 'OR'),
    'ordre' => 20,
    'target' => "blank",
    'ajax' => false,
);

// Recherche avancée
$args = array(
    0 => array('', 'true', 'false'),
    1 => array(__('Tous'), __('Oui'), __('Non')),
);
$subquery = 'SELECT perimetre.perimetre FROM '.DB_PREFIXE.'perimetre WHERE perimetre.geom IS NOT NULL';
$advs_fields = array(
    "identifiant_perimetre" => array(
        "colonne" => "perimetre",
        "table" => "perimetre",
        "type" => "text",
        "libelle" => __("id"),
        "taille" => 10,
        "max" => 8,
    ),
    "libelle" => array(
        "colonne" => "libelle",
        "table" => "perimetre",
        "type" => "text",
        "libelle" => __("libelle"),
        "taille" => 10,
        "max" => 8,
    ),
    "libelle_voie" => array(
        "colonne" => "libelle_voie",
        "table" => "perimetre",
        "type" => "text",
        "libelle" => __("libelle_voie"),
        "taille" => 10,
        "max" => 8,
    ),
    "geolocalise" => array(
        "colonne" => "perimetre",
        "table" => "perimetre",
        "type" => "select",
        "subtype" => "manualselect",
        "where" => "insubquery",
        "args" => $args,
        "libelle" => __("géolocalisé"),
        "subquery" => $subquery,
    ),
);
$options[] = array(
    "type" => "search",
    "display" => true,
    "advanced" => $advs_fields,
    "export" => array("csv", ),
    "absolute_object" => "perimetre",
);
