<?php
//$Id$ 
//gen openMairie le 10/08/2018 17:44

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("tables de référence")." -> ".__("types de licences");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."type_licence";
// SELECT 
$champAffiche = array(
    'type_licence.type_licence as "'.__("type_licence").'"',
    'type_licence.libelle as "'.__("libelle").'"',
    "case type_licence.contrainte_proximite when 't' then 'Oui' else 'Non' end as \"".__("contrainte_proximite")."\"",
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'type_licence.type_licence as "'.__("type_licence").'"',
    'type_licence.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY type_licence.libelle ASC NULLS LAST";
$edition="type_licence";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'demande_licence',
);

