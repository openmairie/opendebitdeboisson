<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("tables de référence")." -> ".__("titres de civilités");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."titre_de_civilite";
// SELECT 
$champAffiche = array(
    'titre_de_civilite.titre_de_civilite as "'.__("titre_de_civilite").'"',
    'titre_de_civilite.code as "'.__("code").'"',
    'titre_de_civilite.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'titre_de_civilite.titre_de_civilite as "'.__("titre_de_civilite").'"',
    'titre_de_civilite.code as "'.__("code").'"',
    'titre_de_civilite.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY titre_de_civilite.libelle ASC NULLS LAST";
$edition="titre_de_civilite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement',
);

