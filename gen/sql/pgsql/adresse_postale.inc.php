<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("géolocalisation")." -> ".__("adresses postales");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."adresse_postale";
// SELECT 
$champAffiche = array(
    'adresse_postale.adresse_postale as "'.__("adresse_postale").'"',
    'adresse_postale.rivoli as "'.__("rivoli").'"',
    'adresse_postale.numero as "'.__("numero").'"',
    'adresse_postale.complement as "'.__("complement").'"',
    'adresse_postale.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    'adresse_postale.geom as "'.__("geom").'"',
    );
//
$champRecherche = array(
    'adresse_postale.adresse_postale as "'.__("adresse_postale").'"',
    'adresse_postale.rivoli as "'.__("rivoli").'"',
    'adresse_postale.numero as "'.__("numero").'"',
    'adresse_postale.complement as "'.__("complement").'"',
    'adresse_postale.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY adresse_postale.libelle ASC NULLS LAST";
$edition="adresse_postale";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

