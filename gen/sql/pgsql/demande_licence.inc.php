<?php
//$Id$ 
//gen openMairie le 10/08/2018 17:44

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("demandes de licence");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."demande_licence
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON demande_licence.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."statut_demande 
        ON demande_licence.statut_demande=statut_demande.statut_demande 
    LEFT JOIN ".DB_PREFIXE."terme 
        ON demande_licence.terme=terme.terme 
    LEFT JOIN ".DB_PREFIXE."type_demande 
        ON demande_licence.type_demande=type_demande.type_demande 
    LEFT JOIN ".DB_PREFIXE."type_licence 
        ON demande_licence.type_licence=type_licence.type_licence ";
// SELECT 
$champAffiche = array(
    'demande_licence.demande_licence as "'.__("demande_licence").'"',
    'type_licence.libelle as "'.__("type_licence").'"',
    'demande_licence.numero_licence as "'.__("numero_licence").'"',
    'terme.libelle as "'.__("terme").'"',
    'to_char(demande_licence.date_debut_validite ,\'DD/MM/YYYY\') as "'.__("date_debut_validite").'"',
    'to_char(demande_licence.date_fin_validite ,\'DD/MM/YYYY\') as "'.__("date_fin_validite").'"',
    'etablissement.raison_sociale as "'.__("etablissement").'"',
    );
//
$champNonAffiche = array(
    'demande_licence.date_demande_licence as "'.__("date_demande_licence").'"',
    'demande_licence.date_ancienne_demande as "'.__("date_ancienne_demande").'"',
    'demande_licence.type_demande as "'.__("type_demande").'"',
    'demande_licence.compte_rendu as "'.__("compte_rendu").'"',
    'demande_licence.statut_demande as "'.__("statut_demande").'"',
    'demande_licence.occasion as "'.__("occasion").'"',
    'demande_licence.particularite as "'.__("particularite").'"',
    'demande_licence.heure_debut as "'.__("heure_debut").'"',
    'demande_licence.heure_fin as "'.__("heure_fin").'"',
    );
//
$champRecherche = array(
    'demande_licence.demande_licence as "'.__("demande_licence").'"',
    'type_licence.libelle as "'.__("type_licence").'"',
    'demande_licence.numero_licence as "'.__("numero_licence").'"',
    'terme.libelle as "'.__("terme").'"',
    'etablissement.raison_sociale as "'.__("etablissement").'"',
    );
$tri="ORDER BY demande_licence.date_demande_licence ASC NULLS LAST";
$edition="demande_licence";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement" => array("etablissement", "etablissement_permanent", "etablissement_temporaire", ),
    "statut_demande" => array("statut_demande", ),
    "terme" => array("terme", ),
    "type_demande" => array("type_demande", ),
    "type_licence" => array("type_licence", ),
);
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (demande_licence.etablissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - statut_demande
if (in_array($retourformulaire, $foreign_keys_extended["statut_demande"])) {
    $selection = " WHERE (demande_licence.statut_demande = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - terme
if (in_array($retourformulaire, $foreign_keys_extended["terme"])) {
    $selection = " WHERE (demande_licence.terme = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - type_demande
if (in_array($retourformulaire, $foreign_keys_extended["type_demande"])) {
    $selection = " WHERE (demande_licence.type_demande = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - type_licence
if (in_array($retourformulaire, $foreign_keys_extended["type_licence"])) {
    $selection = " WHERE (demande_licence.type_licence = ".intval($idxformulaire).") ";
}

