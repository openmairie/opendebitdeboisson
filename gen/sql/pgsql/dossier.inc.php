<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("dossiers");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."dossier
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON dossier.etablissement=etablissement.etablissement ";
// SELECT 
$champAffiche = array(
    'dossier.dossier as "'.__("dossier").'"',
    'to_char(dossier.date_piece ,\'DD/MM/YYYY\') as "'.__("date_piece").'"',
    'dossier.observation as "'.__("observation").'"',
    'etablissement.raison_sociale as "'.__("etablissement").'"',
    );
//
$champNonAffiche = array(
    'dossier.fichier as "'.__("fichier").'"',
    );
//
$champRecherche = array(
    'dossier.dossier as "'.__("dossier").'"',
    'dossier.observation as "'.__("observation").'"',
    'etablissement.raison_sociale as "'.__("etablissement").'"',
    );
$tri="ORDER BY dossier.date_piece ASC NULLS LAST";
$edition="dossier";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement" => array("etablissement", "etablissement_permanent", "etablissement_temporaire", ),
);
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (dossier.etablissement = ".intval($idxformulaire).") ";
}

