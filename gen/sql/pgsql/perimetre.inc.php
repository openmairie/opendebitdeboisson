<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("périmètres d'exclusion");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."perimetre";
// SELECT 
$champAffiche = array(
    'perimetre.perimetre as "'.__("perimetre").'"',
    'perimetre.libelle as "'.__("libelle").'"',
    'perimetre.longueur_exclusion_metre as "'.__("longueur_exclusion_metre").'"',
    );
//
$champNonAffiche = array(
    'perimetre.numero_voie as "'.__("numero_voie").'"',
    'perimetre.complement as "'.__("complement").'"',
    'perimetre.voie as "'.__("voie").'"',
    'perimetre.libelle_voie as "'.__("libelle_voie").'"',
    'perimetre.complement_voie as "'.__("complement_voie").'"',
    'perimetre.geom as "'.__("geom").'"',
    );
//
$champRecherche = array(
    'perimetre.perimetre as "'.__("perimetre").'"',
    'perimetre.libelle as "'.__("libelle").'"',
    'perimetre.longueur_exclusion_metre as "'.__("longueur_exclusion_metre").'"',
    );
$tri="ORDER BY perimetre.libelle ASC NULLS LAST";
$edition="perimetre";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

