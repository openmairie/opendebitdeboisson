<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("tables de référence")." -> ".__("types de demandes");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."type_demande";
// SELECT 
$champAffiche = array(
    'type_demande.type_demande as "'.__("type_demande").'"',
    'type_demande.code as "'.__("code").'"',
    'type_demande.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'type_demande.type_demande as "'.__("type_demande").'"',
    'type_demande.code as "'.__("code").'"',
    'type_demande.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY type_demande.libelle ASC NULLS LAST";
$edition="type_demande";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'demande_licence',
);

