<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("tables de référence")." -> ".__("types d'établissements");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."type_etablissement";
// SELECT 
$champAffiche = array(
    'type_etablissement.type_etablissement as "'.__("type_etablissement").'"',
    'type_etablissement.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'type_etablissement.type_etablissement as "'.__("type_etablissement").'"',
    'type_etablissement.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY type_etablissement.libelle ASC NULLS LAST";
$edition="type_etablissement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement',
);

