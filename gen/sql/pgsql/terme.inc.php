<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("tables de référence")." -> ".__("termes");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."terme";
// SELECT 
$champAffiche = array(
    'terme.terme as "'.__("terme").'"',
    'terme.code as "'.__("code").'"',
    'terme.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'terme.terme as "'.__("terme").'"',
    'terme.code as "'.__("code").'"',
    'terme.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY terme.libelle ASC NULLS LAST";
$edition="terme";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'demande_licence',
);

