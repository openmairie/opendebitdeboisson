<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("tables de référence")." -> ".__("statuts de la demande");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."statut_demande";
// SELECT 
$champAffiche = array(
    'statut_demande.statut_demande as "'.__("statut_demande").'"',
    'statut_demande.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'statut_demande.statut_demande as "'.__("statut_demande").'"',
    'statut_demande.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY statut_demande.libelle ASC NULLS LAST";
$edition="statut_demande";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'demande_licence',
);

