<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("courriers");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."courrier
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON courrier.etablissement=etablissement.etablissement ";
// SELECT 
$champAffiche = array(
    'courrier.courrier as "'.__("courrier").'"',
    'to_char(courrier.date_courrier ,\'DD/MM/YYYY\') as "'.__("date_courrier").'"',
    'courrier.modele_lettre_type as "'.__("modele_lettre_type").'"',
    'courrier.objet as "'.__("objet").'"',
    "case courrier.fichier_finalise when 't' then 'Oui' else 'Non' end as \"".__("fichier_finalise")."\"",
    'etablissement.raison_sociale as "'.__("etablissement").'"',
    );
//
$champNonAffiche = array(
    'courrier.corps_om_html as "'.__("corps_om_html").'"',
    'courrier.fichier as "'.__("fichier").'"',
    );
//
$champRecherche = array(
    'courrier.courrier as "'.__("courrier").'"',
    'courrier.modele_lettre_type as "'.__("modele_lettre_type").'"',
    'courrier.objet as "'.__("objet").'"',
    'etablissement.raison_sociale as "'.__("etablissement").'"',
    );
$tri="ORDER BY courrier.date_courrier ASC NULLS LAST";
$edition="courrier";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement" => array("etablissement", "etablissement_permanent", "etablissement_temporaire", ),
);
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (courrier.etablissement = ".intval($idxformulaire).") ";
}

