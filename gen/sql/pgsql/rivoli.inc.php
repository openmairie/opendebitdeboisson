<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("géolocalisation")." -> ".__("rivoli");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."rivoli";
// SELECT 
$champAffiche = array(
    'rivoli.rivoli as "'.__("rivoli").'"',
    'rivoli.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'rivoli.rivoli as "'.__("rivoli").'"',
    'rivoli.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY rivoli.libelle ASC NULLS LAST";
$edition="rivoli";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

