<?php
//$Id$ 
//gen openMairie le 19/10/2018 13:02

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("établissements");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."etablissement
    LEFT JOIN ".DB_PREFIXE."titre_de_civilite as titre_de_civilite0 
        ON etablissement.civilite_co_exploitant=titre_de_civilite0.titre_de_civilite 
    LEFT JOIN ".DB_PREFIXE."titre_de_civilite as titre_de_civilite1 
        ON etablissement.civilite_exploitant=titre_de_civilite1.titre_de_civilite 
    LEFT JOIN ".DB_PREFIXE."titre_de_civilite as titre_de_civilite2 
        ON etablissement.civilite_proprietaire=titre_de_civilite2.titre_de_civilite 
    LEFT JOIN ".DB_PREFIXE."qualite_exploitant as qualite_exploitant3 
        ON etablissement.qualite_ancien_exploitant=qualite_exploitant3.qualite_exploitant 
    LEFT JOIN ".DB_PREFIXE."qualite_exploitant as qualite_exploitant4 
        ON etablissement.qualite_co_exploitant=qualite_exploitant4.qualite_exploitant 
    LEFT JOIN ".DB_PREFIXE."qualite_exploitant as qualite_exploitant5 
        ON etablissement.qualite_exploitant=qualite_exploitant5.qualite_exploitant 
    LEFT JOIN ".DB_PREFIXE."type_etablissement 
        ON etablissement.type_etablissement=type_etablissement.type_etablissement ";
// SELECT 
$champAffiche = array(
    'etablissement.etablissement as "'.__("etablissement").'"',
    'etablissement.raison_sociale as "'.__("raison_sociale").'"',
    'etablissement.enseigne as "'.__("enseigne").'"',
    'etablissement.libelle_voie as "'.__("libelle_voie").'"',
    'etablissement.nom_exploitant as "'.__("nom_exploitant").'"',
    'etablissement.nom_proprietaire as "'.__("nom_proprietaire").'"',
    );
//
$champNonAffiche = array(
    'etablissement.no_siret as "'.__("no_siret").'"',
    'etablissement.forme_juridique as "'.__("forme_juridique").'"',
    'etablissement.nature as "'.__("nature").'"',
    'etablissement.type_etablissement as "'.__("type_etablissement").'"',
    'etablissement.permis_exploitation as "'.__("permis_exploitation").'"',
    'etablissement.date_permis_exploitation as "'.__("date_permis_exploitation").'"',
    'etablissement.permis_exploitation_nuit as "'.__("permis_exploitation_nuit").'"',
    'etablissement.date_permis_exploitation_nuit as "'.__("date_permis_exploitation_nuit").'"',
    'etablissement.numero_voie as "'.__("numero_voie").'"',
    'etablissement.complement as "'.__("complement").'"',
    'etablissement.voie as "'.__("voie").'"',
    'etablissement.complement_voie as "'.__("complement_voie").'"',
    'etablissement.cp_etablissement as "'.__("cp_etablissement").'"',
    'etablissement.ville_etablissement as "'.__("ville_etablissement").'"',
    'etablissement.geom as "'.__("geom").'"',
    'etablissement.civilite_exploitant as "'.__("civilite_exploitant").'"',
    'etablissement.nom_marital_exploitant as "'.__("nom_marital_exploitant").'"',
    'etablissement.prenom_exploitant as "'.__("prenom_exploitant").'"',
    'etablissement.date_naissance_exploitant as "'.__("date_naissance_exploitant").'"',
    'etablissement.ville_naissance_exploitant as "'.__("ville_naissance_exploitant").'"',
    'etablissement.cp_naissance_exploitant as "'.__("cp_naissance_exploitant").'"',
    'etablissement.adresse1_exploitant as "'.__("adresse1_exploitant").'"',
    'etablissement.adresse2_exploitant as "'.__("adresse2_exploitant").'"',
    'etablissement.cp_exploitant as "'.__("cp_exploitant").'"',
    'etablissement.ville_exploitant as "'.__("ville_exploitant").'"',
    'etablissement.qualite_exploitant as "'.__("qualite_exploitant").'"',
    'etablissement.nationalite_exploitant as "'.__("nationalite_exploitant").'"',
    'etablissement.particularite_exploitant as "'.__("particularite_exploitant").'"',
    'etablissement.civilite_co_exploitant as "'.__("civilite_co_exploitant").'"',
    'etablissement.nom_co_exploitant as "'.__("nom_co_exploitant").'"',
    'etablissement.nom_marital_co_exploitant as "'.__("nom_marital_co_exploitant").'"',
    'etablissement.prenom_co_exploitant as "'.__("prenom_co_exploitant").'"',
    'etablissement.date_naissance_co_exploitant as "'.__("date_naissance_co_exploitant").'"',
    'etablissement.ville_naissance_co_exploitant as "'.__("ville_naissance_co_exploitant").'"',
    'etablissement.cp_naissance_co_exploitant as "'.__("cp_naissance_co_exploitant").'"',
    'etablissement.adresse1_co_exploitant as "'.__("adresse1_co_exploitant").'"',
    'etablissement.adresse2_co_exploitant as "'.__("adresse2_co_exploitant").'"',
    'etablissement.cp_co_exploitant as "'.__("cp_co_exploitant").'"',
    'etablissement.ville_co_exploitant as "'.__("ville_co_exploitant").'"',
    'etablissement.nationalite_co_exploitant as "'.__("nationalite_co_exploitant").'"',
    'etablissement.qualite_co_exploitant as "'.__("qualite_co_exploitant").'"',
    'etablissement.particularite_co_exploitant as "'.__("particularite_co_exploitant").'"',
    'etablissement.nom_ancien_exploitant as "'.__("nom_ancien_exploitant").'"',
    'etablissement.prenom_ancien_exploitant as "'.__("prenom_ancien_exploitant").'"',
    'etablissement.qualite_ancien_exploitant as "'.__("qualite_ancien_exploitant").'"',
    'etablissement.civilite_proprietaire as "'.__("civilite_proprietaire").'"',
    'etablissement.prenom_proprietaire as "'.__("prenom_proprietaire").'"',
    'etablissement.adresse1_proprietaire as "'.__("adresse1_proprietaire").'"',
    'etablissement.adresse2_proprietaire as "'.__("adresse2_proprietaire").'"',
    'etablissement.cp_proprietaire as "'.__("cp_proprietaire").'"',
    'etablissement.ville_proprietaire as "'.__("ville_proprietaire").'"',
    'etablissement.profession_proprietaire as "'.__("profession_proprietaire").'"',
    'etablissement.date_fermeture as "'.__("date_fermeture").'"',
    'etablissement.date_liquidation as "'.__("date_liquidation").'"',
    'etablissement.observation as "'.__("observation").'"',
    'etablissement.ancien_proprietaire as "'.__("ancien_proprietaire").'"',
    );
//
$champRecherche = array(
    'etablissement.etablissement as "'.__("etablissement").'"',
    'etablissement.raison_sociale as "'.__("raison_sociale").'"',
    'etablissement.enseigne as "'.__("enseigne").'"',
    'etablissement.libelle_voie as "'.__("libelle_voie").'"',
    'etablissement.nom_exploitant as "'.__("nom_exploitant").'"',
    'etablissement.nom_proprietaire as "'.__("nom_proprietaire").'"',
    );
$tri="ORDER BY etablissement.raison_sociale ASC NULLS LAST";
$edition="etablissement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "titre_de_civilite" => array("titre_de_civilite", ),
    "qualite_exploitant" => array("qualite_exploitant", ),
    "type_etablissement" => array("type_etablissement", ),
);
// Filtre listing sous formulaire - titre_de_civilite
if (in_array($retourformulaire, $foreign_keys_extended["titre_de_civilite"])) {
    $selection = " WHERE (etablissement.civilite_co_exploitant = ".intval($idxformulaire)." OR etablissement.civilite_exploitant = ".intval($idxformulaire)." OR etablissement.civilite_proprietaire = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - qualite_exploitant
if (in_array($retourformulaire, $foreign_keys_extended["qualite_exploitant"])) {
    $selection = " WHERE (etablissement.qualite_ancien_exploitant = ".intval($idxformulaire)." OR etablissement.qualite_co_exploitant = ".intval($idxformulaire)." OR etablissement.qualite_exploitant = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - type_etablissement
if (in_array($retourformulaire, $foreign_keys_extended["type_etablissement"])) {
    $selection = " WHERE (etablissement.type_etablissement = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'courrier',
    'demande_licence',
    'dossier',
);

