<?php
//$Id$ 
//gen openMairie le 10/08/2018 17:44

require_once "../obj/om_dbform.class.php";

class demande_licence_gen extends om_dbform {

    protected $_absolute_class_name = "demande_licence";

    var $table = "demande_licence";
    var $clePrimaire = "demande_licence";
    var $typeCle = "N";
    var $required_field = array(
        "date_debut_validite",
        "date_demande_licence",
        "date_fin_validite",
        "demande_licence",
        "etablissement",
        "heure_debut",
        "heure_fin",
        "terme",
        "type_demande",
        "type_licence"
    );
    
    var $foreign_keys_extended = array(
        "etablissement" => array("etablissement", "etablissement_permanent", "etablissement_temporaire", ),
        "statut_demande" => array("statut_demande", ),
        "terme" => array("terme", ),
        "type_demande" => array("type_demande", ),
        "type_licence" => array("type_licence", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("date_demande_licence");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "demande_licence",
            "date_demande_licence",
            "date_ancienne_demande",
            "type_demande",
            "type_licence",
            "compte_rendu",
            "statut_demande",
            "numero_licence",
            "occasion",
            "particularite",
            "terme",
            "date_debut_validite",
            "heure_debut",
            "date_fin_validite",
            "heure_fin",
            "etablissement",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement() {
        return "SELECT etablissement.etablissement, etablissement.raison_sociale FROM ".DB_PREFIXE."etablissement ORDER BY etablissement.raison_sociale ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_by_id() {
        return "SELECT etablissement.etablissement, etablissement.raison_sociale FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_statut_demande() {
        return "SELECT statut_demande.statut_demande, statut_demande.libelle FROM ".DB_PREFIXE."statut_demande ORDER BY statut_demande.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_statut_demande_by_id() {
        return "SELECT statut_demande.statut_demande, statut_demande.libelle FROM ".DB_PREFIXE."statut_demande WHERE statut_demande = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_terme() {
        return "SELECT terme.terme, terme.libelle FROM ".DB_PREFIXE."terme ORDER BY terme.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_terme_by_id() {
        return "SELECT terme.terme, terme.libelle FROM ".DB_PREFIXE."terme WHERE terme = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_demande() {
        return "SELECT type_demande.type_demande, type_demande.libelle FROM ".DB_PREFIXE."type_demande ORDER BY type_demande.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_demande_by_id() {
        return "SELECT type_demande.type_demande, type_demande.libelle FROM ".DB_PREFIXE."type_demande WHERE type_demande = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_licence() {
        return "SELECT type_licence.type_licence, type_licence.libelle FROM ".DB_PREFIXE."type_licence ORDER BY type_licence.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_licence_by_id() {
        return "SELECT type_licence.type_licence, type_licence.libelle FROM ".DB_PREFIXE."type_licence WHERE type_licence = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['demande_licence'])) {
            $this->valF['demande_licence'] = ""; // -> requis
        } else {
            $this->valF['demande_licence'] = $val['demande_licence'];
        }
        if ($val['date_demande_licence'] != "") {
            $this->valF['date_demande_licence'] = $this->dateDB($val['date_demande_licence']);
        }
        if ($val['date_ancienne_demande'] != "") {
            $this->valF['date_ancienne_demande'] = $this->dateDB($val['date_ancienne_demande']);
        } else {
            $this->valF['date_ancienne_demande'] = NULL;
        }
        if (!is_numeric($val['type_demande'])) {
            $this->valF['type_demande'] = ""; // -> requis
        } else {
            $this->valF['type_demande'] = $val['type_demande'];
        }
        if (!is_numeric($val['type_licence'])) {
            $this->valF['type_licence'] = ""; // -> requis
        } else {
            $this->valF['type_licence'] = $val['type_licence'];
        }
            $this->valF['compte_rendu'] = $val['compte_rendu'];
        if (!is_numeric($val['statut_demande'])) {
            $this->valF['statut_demande'] = NULL;
        } else {
            $this->valF['statut_demande'] = $val['statut_demande'];
        }
        if ($val['numero_licence'] == "") {
            $this->valF['numero_licence'] = NULL;
        } else {
            $this->valF['numero_licence'] = $val['numero_licence'];
        }
        if ($val['occasion'] == "") {
            $this->valF['occasion'] = NULL;
        } else {
            $this->valF['occasion'] = $val['occasion'];
        }
            $this->valF['particularite'] = $val['particularite'];
        if (!is_numeric($val['terme'])) {
            $this->valF['terme'] = ""; // -> requis
        } else {
            $this->valF['terme'] = $val['terme'];
        }
        if ($val['date_debut_validite'] != "") {
            $this->valF['date_debut_validite'] = $this->dateDB($val['date_debut_validite']);
        }
            $this->valF['heure_debut'] = $val['heure_debut'];
        if ($val['date_fin_validite'] != "") {
            $this->valF['date_fin_validite'] = $this->dateDB($val['date_fin_validite']);
        }
            $this->valF['heure_fin'] = $val['heure_fin'];
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = ""; // -> requis
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("demande_licence", "hidden");
            $form->setType("date_demande_licence", "date");
            $form->setType("date_ancienne_demande", "date");
            if ($this->is_in_context_of_foreign_key("type_demande", $this->retourformulaire)) {
                $form->setType("type_demande", "selecthiddenstatic");
            } else {
                $form->setType("type_demande", "select");
            }
            if ($this->is_in_context_of_foreign_key("type_licence", $this->retourformulaire)) {
                $form->setType("type_licence", "selecthiddenstatic");
            } else {
                $form->setType("type_licence", "select");
            }
            $form->setType("compte_rendu", "textarea");
            if ($this->is_in_context_of_foreign_key("statut_demande", $this->retourformulaire)) {
                $form->setType("statut_demande", "selecthiddenstatic");
            } else {
                $form->setType("statut_demande", "select");
            }
            $form->setType("numero_licence", "text");
            $form->setType("occasion", "text");
            $form->setType("particularite", "textarea");
            if ($this->is_in_context_of_foreign_key("terme", $this->retourformulaire)) {
                $form->setType("terme", "selecthiddenstatic");
            } else {
                $form->setType("terme", "select");
            }
            $form->setType("date_debut_validite", "date");
            $form->setType("heure_debut", "text");
            $form->setType("date_fin_validite", "date");
            $form->setType("heure_fin", "text");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("demande_licence", "hiddenstatic");
            $form->setType("date_demande_licence", "date");
            $form->setType("date_ancienne_demande", "date");
            if ($this->is_in_context_of_foreign_key("type_demande", $this->retourformulaire)) {
                $form->setType("type_demande", "selecthiddenstatic");
            } else {
                $form->setType("type_demande", "select");
            }
            if ($this->is_in_context_of_foreign_key("type_licence", $this->retourformulaire)) {
                $form->setType("type_licence", "selecthiddenstatic");
            } else {
                $form->setType("type_licence", "select");
            }
            $form->setType("compte_rendu", "textarea");
            if ($this->is_in_context_of_foreign_key("statut_demande", $this->retourformulaire)) {
                $form->setType("statut_demande", "selecthiddenstatic");
            } else {
                $form->setType("statut_demande", "select");
            }
            $form->setType("numero_licence", "text");
            $form->setType("occasion", "text");
            $form->setType("particularite", "textarea");
            if ($this->is_in_context_of_foreign_key("terme", $this->retourformulaire)) {
                $form->setType("terme", "selecthiddenstatic");
            } else {
                $form->setType("terme", "select");
            }
            $form->setType("date_debut_validite", "date");
            $form->setType("heure_debut", "text");
            $form->setType("date_fin_validite", "date");
            $form->setType("heure_fin", "text");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("demande_licence", "hiddenstatic");
            $form->setType("date_demande_licence", "hiddenstatic");
            $form->setType("date_ancienne_demande", "hiddenstatic");
            $form->setType("type_demande", "selectstatic");
            $form->setType("type_licence", "selectstatic");
            $form->setType("compte_rendu", "hiddenstatic");
            $form->setType("statut_demande", "selectstatic");
            $form->setType("numero_licence", "hiddenstatic");
            $form->setType("occasion", "hiddenstatic");
            $form->setType("particularite", "hiddenstatic");
            $form->setType("terme", "selectstatic");
            $form->setType("date_debut_validite", "hiddenstatic");
            $form->setType("heure_debut", "hiddenstatic");
            $form->setType("date_fin_validite", "hiddenstatic");
            $form->setType("heure_fin", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("demande_licence", "static");
            $form->setType("date_demande_licence", "datestatic");
            $form->setType("date_ancienne_demande", "datestatic");
            $form->setType("type_demande", "selectstatic");
            $form->setType("type_licence", "selectstatic");
            $form->setType("compte_rendu", "textareastatic");
            $form->setType("statut_demande", "selectstatic");
            $form->setType("numero_licence", "static");
            $form->setType("occasion", "static");
            $form->setType("particularite", "textareastatic");
            $form->setType("terme", "selectstatic");
            $form->setType("date_debut_validite", "datestatic");
            $form->setType("heure_debut", "static");
            $form->setType("date_fin_validite", "datestatic");
            $form->setType("heure_fin", "static");
            $form->setType("etablissement", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('demande_licence','VerifNum(this)');
        $form->setOnchange('date_demande_licence','fdate(this)');
        $form->setOnchange('date_ancienne_demande','fdate(this)');
        $form->setOnchange('type_demande','VerifNum(this)');
        $form->setOnchange('type_licence','VerifNum(this)');
        $form->setOnchange('statut_demande','VerifNum(this)');
        $form->setOnchange('terme','VerifNum(this)');
        $form->setOnchange('date_debut_validite','fdate(this)');
        $form->setOnchange('date_fin_validite','fdate(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("demande_licence", 11);
        $form->setTaille("date_demande_licence", 12);
        $form->setTaille("date_ancienne_demande", 12);
        $form->setTaille("type_demande", 11);
        $form->setTaille("type_licence", 11);
        $form->setTaille("compte_rendu", 80);
        $form->setTaille("statut_demande", 11);
        $form->setTaille("numero_licence", 30);
        $form->setTaille("occasion", 30);
        $form->setTaille("particularite", 80);
        $form->setTaille("terme", 11);
        $form->setTaille("date_debut_validite", 12);
        $form->setTaille("heure_debut", 8);
        $form->setTaille("date_fin_validite", 12);
        $form->setTaille("heure_fin", 8);
        $form->setTaille("etablissement", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("demande_licence", 11);
        $form->setMax("date_demande_licence", 12);
        $form->setMax("date_ancienne_demande", 12);
        $form->setMax("type_demande", 11);
        $form->setMax("type_licence", 11);
        $form->setMax("compte_rendu", 6);
        $form->setMax("statut_demande", 11);
        $form->setMax("numero_licence", 50);
        $form->setMax("occasion", 50);
        $form->setMax("particularite", 6);
        $form->setMax("terme", 11);
        $form->setMax("date_debut_validite", 12);
        $form->setMax("heure_debut", 8);
        $form->setMax("date_fin_validite", 12);
        $form->setMax("heure_fin", 8);
        $form->setMax("etablissement", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('demande_licence', __('demande_licence'));
        $form->setLib('date_demande_licence', __('date_demande_licence'));
        $form->setLib('date_ancienne_demande', __('date_ancienne_demande'));
        $form->setLib('type_demande', __('type_demande'));
        $form->setLib('type_licence', __('type_licence'));
        $form->setLib('compte_rendu', __('compte_rendu'));
        $form->setLib('statut_demande', __('statut_demande'));
        $form->setLib('numero_licence', __('numero_licence'));
        $form->setLib('occasion', __('occasion'));
        $form->setLib('particularite', __('particularite'));
        $form->setLib('terme', __('terme'));
        $form->setLib('date_debut_validite', __('date_debut_validite'));
        $form->setLib('heure_debut', __('heure_debut'));
        $form->setLib('date_fin_validite', __('date_fin_validite'));
        $form->setLib('heure_fin', __('heure_fin'));
        $form->setLib('etablissement', __('etablissement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // etablissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement",
            $this->get_var_sql_forminc__sql("etablissement"),
            $this->get_var_sql_forminc__sql("etablissement_by_id"),
            false
        );
        // statut_demande
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "statut_demande",
            $this->get_var_sql_forminc__sql("statut_demande"),
            $this->get_var_sql_forminc__sql("statut_demande_by_id"),
            false
        );
        // terme
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "terme",
            $this->get_var_sql_forminc__sql("terme"),
            $this->get_var_sql_forminc__sql("terme_by_id"),
            false
        );
        // type_demande
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "type_demande",
            $this->get_var_sql_forminc__sql("type_demande"),
            $this->get_var_sql_forminc__sql("type_demande_by_id"),
            false
        );
        // type_licence
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "type_licence",
            $this->get_var_sql_forminc__sql("type_licence"),
            $this->get_var_sql_forminc__sql("type_licence_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('statut_demande', $this->retourformulaire))
                $form->setVal('statut_demande', $idxformulaire);
            if($this->is_in_context_of_foreign_key('terme', $this->retourformulaire))
                $form->setVal('terme', $idxformulaire);
            if($this->is_in_context_of_foreign_key('type_demande', $this->retourformulaire))
                $form->setVal('type_demande', $idxformulaire);
            if($this->is_in_context_of_foreign_key('type_licence', $this->retourformulaire))
                $form->setVal('type_licence', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
