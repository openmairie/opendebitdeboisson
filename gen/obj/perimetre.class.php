<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

require_once "../obj/om_dbform.class.php";

class perimetre_gen extends om_dbform {

    protected $_absolute_class_name = "perimetre";

    var $table = "perimetre";
    var $clePrimaire = "perimetre";
    var $typeCle = "N";
    var $required_field = array(
        "libelle",
        "longueur_exclusion_metre",
        "perimetre"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "perimetre",
            "libelle",
            "longueur_exclusion_metre",
            "numero_voie",
            "complement",
            "voie",
            "libelle_voie",
            "complement_voie",
            "geom",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['perimetre'])) {
            $this->valF['perimetre'] = ""; // -> requis
        } else {
            $this->valF['perimetre'] = $val['perimetre'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['longueur_exclusion_metre'])) {
            $this->valF['longueur_exclusion_metre'] = ""; // -> requis
        } else {
            $this->valF['longueur_exclusion_metre'] = $val['longueur_exclusion_metre'];
        }
        if (!is_numeric($val['numero_voie'])) {
            $this->valF['numero_voie'] = NULL;
        } else {
            $this->valF['numero_voie'] = $val['numero_voie'];
        }
        if ($val['complement'] == "") {
            $this->valF['complement'] = NULL;
        } else {
            $this->valF['complement'] = $val['complement'];
        }
        if ($val['voie'] == "") {
            $this->valF['voie'] = NULL;
        } else {
            $this->valF['voie'] = $val['voie'];
        }
        if ($val['libelle_voie'] == "") {
            $this->valF['libelle_voie'] = NULL;
        } else {
            $this->valF['libelle_voie'] = $val['libelle_voie'];
        }
        if ($val['complement_voie'] == "") {
            $this->valF['complement_voie'] = NULL;
        } else {
            $this->valF['complement_voie'] = $val['complement_voie'];
        }
        if ($val['geom'] == "") {
            unset($this->valF['geom']);
        } else {
            $this->valF['geom'] = $val['geom'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("perimetre", "hidden");
            $form->setType("libelle", "text");
            $form->setType("longueur_exclusion_metre", "text");
            $form->setType("numero_voie", "text");
            $form->setType("complement", "text");
            $form->setType("voie", "text");
            $form->setType("libelle_voie", "text");
            $form->setType("complement_voie", "text");
            $form->setType("geom", "geom");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("perimetre", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("longueur_exclusion_metre", "text");
            $form->setType("numero_voie", "text");
            $form->setType("complement", "text");
            $form->setType("voie", "text");
            $form->setType("libelle_voie", "text");
            $form->setType("complement_voie", "text");
            $form->setType("geom", "geom");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("perimetre", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("longueur_exclusion_metre", "hiddenstatic");
            $form->setType("numero_voie", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("voie", "hiddenstatic");
            $form->setType("libelle_voie", "hiddenstatic");
            $form->setType("complement_voie", "hiddenstatic");
            $form->setType("geom", "geom");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("perimetre", "static");
            $form->setType("libelle", "static");
            $form->setType("longueur_exclusion_metre", "static");
            $form->setType("numero_voie", "static");
            $form->setType("complement", "static");
            $form->setType("voie", "static");
            $form->setType("libelle_voie", "static");
            $form->setType("complement_voie", "static");
            $form->setType("geom", "geom");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('perimetre','VerifNum(this)');
        $form->setOnchange('longueur_exclusion_metre','VerifNum(this)');
        $form->setOnchange('numero_voie','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("perimetre", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("longueur_exclusion_metre", 11);
        $form->setTaille("numero_voie", 11);
        $form->setTaille("complement", 10);
        $form->setTaille("voie", 10);
        $form->setTaille("libelle_voie", 30);
        $form->setTaille("complement_voie", 30);
        $form->setTaille("geom", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("perimetre", 11);
        $form->setMax("libelle", 70);
        $form->setMax("longueur_exclusion_metre", 11);
        $form->setMax("numero_voie", 11);
        $form->setMax("complement", 10);
        $form->setMax("voie", 5);
        $form->setMax("libelle_voie", 40);
        $form->setMax("complement_voie", 40);
        $form->setMax("geom", 551424);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('perimetre', __('perimetre'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('longueur_exclusion_metre', __('longueur_exclusion_metre'));
        $form->setLib('numero_voie', __('numero_voie'));
        $form->setLib('complement', __('complement'));
        $form->setLib('voie', __('voie'));
        $form->setLib('libelle_voie', __('libelle_voie'));
        $form->setLib('complement_voie', __('complement_voie'));
        $form->setLib('geom', __('geom'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // geom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("perimetre", $this->getParameter("idx"), "0");
            $form->setSelect("geom", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
