<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

require_once "../obj/om_dbform.class.php";

class rivoli_gen extends om_dbform {

    protected $_absolute_class_name = "rivoli";

    var $table = "rivoli";
    var $clePrimaire = "rivoli";
    var $typeCle = "A";
    var $required_field = array(
        "libelle",
        "rivoli"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "rivoli",
            "libelle",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['rivoli'] = $val['rivoli'];
        $this->valF['libelle'] = $val['libelle'];
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("rivoli", "text");
            $form->setType("libelle", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("rivoli", "hiddenstatic");
            $form->setType("libelle", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("rivoli", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("rivoli", "static");
            $form->setType("libelle", "static");
        }

    }

    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("rivoli", 10);
        $form->setTaille("libelle", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("rivoli", 4);
        $form->setMax("libelle", 80);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('rivoli', __('rivoli'));
        $form->setLib('libelle', __('libelle'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
