<?php
//$Id$ 
//gen openMairie le 10/08/2018 17:44

require_once "../obj/om_dbform.class.php";

class type_licence_gen extends om_dbform {

    protected $_absolute_class_name = "type_licence";

    var $table = "type_licence";
    var $clePrimaire = "type_licence";
    var $typeCle = "N";
    var $required_field = array(
        "libelle",
        "type_licence"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "type_licence",
            "libelle",
            "contrainte_proximite",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['type_licence'])) {
            $this->valF['type_licence'] = ""; // -> requis
        } else {
            $this->valF['type_licence'] = $val['type_licence'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if ($val['contrainte_proximite'] == 1 || $val['contrainte_proximite'] == "t" || $val['contrainte_proximite'] == "Oui") {
            $this->valF['contrainte_proximite'] = true;
        } else {
            $this->valF['contrainte_proximite'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("type_licence", "hidden");
            $form->setType("libelle", "text");
            $form->setType("contrainte_proximite", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("type_licence", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("contrainte_proximite", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("type_licence", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("contrainte_proximite", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("type_licence", "static");
            $form->setType("libelle", "static");
            $form->setType("contrainte_proximite", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('type_licence','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("type_licence", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("contrainte_proximite", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("type_licence", 11);
        $form->setMax("libelle", 50);
        $form->setMax("contrainte_proximite", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('type_licence', __('type_licence'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('contrainte_proximite', __('contrainte_proximite'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : demande_licence
        $this->rechercheTable($this->f->db, "demande_licence", "type_licence", $id);
    }


}
