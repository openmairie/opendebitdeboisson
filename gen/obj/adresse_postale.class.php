<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

require_once "../obj/om_dbform.class.php";

class adresse_postale_gen extends om_dbform {

    protected $_absolute_class_name = "adresse_postale";

    var $table = "adresse_postale";
    var $clePrimaire = "adresse_postale";
    var $typeCle = "N";
    var $required_field = array(
        "adresse_postale"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "adresse_postale",
            "rivoli",
            "numero",
            "complement",
            "libelle",
            "geom",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['adresse_postale'])) {
            $this->valF['adresse_postale'] = ""; // -> requis
        } else {
            $this->valF['adresse_postale'] = $val['adresse_postale'];
        }
        if ($val['rivoli'] == "") {
            $this->valF['rivoli'] = NULL;
        } else {
            $this->valF['rivoli'] = $val['rivoli'];
        }
        if (!is_numeric($val['numero'])) {
            $this->valF['numero'] = NULL;
        } else {
            $this->valF['numero'] = $val['numero'];
        }
        if ($val['complement'] == "") {
            $this->valF['complement'] = NULL;
        } else {
            $this->valF['complement'] = $val['complement'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
        if ($val['geom'] == "") {
            unset($this->valF['geom']);
        } else {
            $this->valF['geom'] = $val['geom'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("adresse_postale", "hidden");
            $form->setType("rivoli", "text");
            $form->setType("numero", "text");
            $form->setType("complement", "text");
            $form->setType("libelle", "text");
            $form->setType("geom", "geom");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("adresse_postale", "hiddenstatic");
            $form->setType("rivoli", "text");
            $form->setType("numero", "text");
            $form->setType("complement", "text");
            $form->setType("libelle", "text");
            $form->setType("geom", "geom");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("adresse_postale", "hiddenstatic");
            $form->setType("rivoli", "hiddenstatic");
            $form->setType("numero", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("geom", "geom");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("adresse_postale", "static");
            $form->setType("rivoli", "static");
            $form->setType("numero", "static");
            $form->setType("complement", "static");
            $form->setType("libelle", "static");
            $form->setType("geom", "geom");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('adresse_postale','VerifNum(this)');
        $form->setOnchange('numero','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("adresse_postale", 11);
        $form->setTaille("rivoli", 10);
        $form->setTaille("numero", 11);
        $form->setTaille("complement", 10);
        $form->setTaille("libelle", 30);
        $form->setTaille("geom", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("adresse_postale", 11);
        $form->setMax("rivoli", 10);
        $form->setMax("numero", 11);
        $form->setMax("complement", 10);
        $form->setMax("libelle", 100);
        $form->setMax("geom", 551424);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('adresse_postale', __('adresse_postale'));
        $form->setLib('rivoli', __('rivoli'));
        $form->setLib('numero', __('numero'));
        $form->setLib('complement', __('complement'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('geom', __('geom'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // geom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("adresse_postale", $this->getParameter("idx"), "0");
            $form->setSelect("geom", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
