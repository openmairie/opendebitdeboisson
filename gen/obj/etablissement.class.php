<?php
//$Id$ 
//gen openMairie le 29/10/2021 11:45

require_once "../obj/om_dbform.class.php";

class etablissement_gen extends om_dbform {

    protected $_absolute_class_name = "etablissement";

    var $table = "etablissement";
    var $clePrimaire = "etablissement";
    var $typeCle = "N";
    var $required_field = array(
        "enseigne",
        "etablissement",
        "nature",
        "raison_sociale"
    );
    
    var $foreign_keys_extended = array(
        "titre_de_civilite" => array("titre_de_civilite", ),
        "qualite_exploitant" => array("qualite_exploitant", ),
        "type_etablissement" => array("type_etablissement", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("raison_sociale");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "etablissement",
            "raison_sociale",
            "enseigne",
            "no_siret",
            "forme_juridique",
            "nature",
            "type_etablissement",
            "permis_exploitation",
            "date_permis_exploitation",
            "permis_exploitation_nuit",
            "date_permis_exploitation_nuit",
            "numero_voie",
            "complement",
            "voie",
            "libelle_voie",
            "complement_voie",
            "cp_etablissement",
            "ville_etablissement",
            "geom",
            "civilite_exploitant",
            "nom_exploitant",
            "nom_marital_exploitant",
            "prenom_exploitant",
            "date_naissance_exploitant",
            "ville_naissance_exploitant",
            "cp_naissance_exploitant",
            "adresse1_exploitant",
            "adresse2_exploitant",
            "cp_exploitant",
            "ville_exploitant",
            "qualite_exploitant",
            "nationalite_exploitant",
            "particularite_exploitant",
            "civilite_co_exploitant",
            "nom_co_exploitant",
            "nom_marital_co_exploitant",
            "prenom_co_exploitant",
            "date_naissance_co_exploitant",
            "ville_naissance_co_exploitant",
            "cp_naissance_co_exploitant",
            "adresse1_co_exploitant",
            "adresse2_co_exploitant",
            "cp_co_exploitant",
            "ville_co_exploitant",
            "nationalite_co_exploitant",
            "qualite_co_exploitant",
            "particularite_co_exploitant",
            "nom_ancien_exploitant",
            "prenom_ancien_exploitant",
            "qualite_ancien_exploitant",
            "civilite_proprietaire",
            "nom_proprietaire",
            "prenom_proprietaire",
            "adresse1_proprietaire",
            "adresse2_proprietaire",
            "cp_proprietaire",
            "ville_proprietaire",
            "profession_proprietaire",
            "date_fermeture",
            "date_liquidation",
            "observation",
            "ancien_proprietaire",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite_co_exploitant() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite ORDER BY titre_de_civilite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite_co_exploitant_by_id() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite WHERE titre_de_civilite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite_exploitant() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite ORDER BY titre_de_civilite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite_exploitant_by_id() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite WHERE titre_de_civilite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite_proprietaire() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite ORDER BY titre_de_civilite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite_proprietaire_by_id() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite WHERE titre_de_civilite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_qualite_ancien_exploitant() {
        return "SELECT qualite_exploitant.qualite_exploitant, qualite_exploitant.libelle FROM ".DB_PREFIXE."qualite_exploitant ORDER BY qualite_exploitant.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_qualite_ancien_exploitant_by_id() {
        return "SELECT qualite_exploitant.qualite_exploitant, qualite_exploitant.libelle FROM ".DB_PREFIXE."qualite_exploitant WHERE qualite_exploitant = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_qualite_co_exploitant() {
        return "SELECT qualite_exploitant.qualite_exploitant, qualite_exploitant.libelle FROM ".DB_PREFIXE."qualite_exploitant ORDER BY qualite_exploitant.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_qualite_co_exploitant_by_id() {
        return "SELECT qualite_exploitant.qualite_exploitant, qualite_exploitant.libelle FROM ".DB_PREFIXE."qualite_exploitant WHERE qualite_exploitant = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_qualite_exploitant() {
        return "SELECT qualite_exploitant.qualite_exploitant, qualite_exploitant.libelle FROM ".DB_PREFIXE."qualite_exploitant ORDER BY qualite_exploitant.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_qualite_exploitant_by_id() {
        return "SELECT qualite_exploitant.qualite_exploitant, qualite_exploitant.libelle FROM ".DB_PREFIXE."qualite_exploitant WHERE qualite_exploitant = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_etablissement() {
        return "SELECT type_etablissement.type_etablissement, type_etablissement.libelle FROM ".DB_PREFIXE."type_etablissement ORDER BY type_etablissement.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_etablissement_by_id() {
        return "SELECT type_etablissement.type_etablissement, type_etablissement.libelle FROM ".DB_PREFIXE."type_etablissement WHERE type_etablissement = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = ""; // -> requis
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        $this->valF['raison_sociale'] = $val['raison_sociale'];
        $this->valF['enseigne'] = $val['enseigne'];
        if ($val['no_siret'] == "") {
            $this->valF['no_siret'] = NULL;
        } else {
            $this->valF['no_siret'] = $val['no_siret'];
        }
        if ($val['forme_juridique'] == "") {
            $this->valF['forme_juridique'] = NULL;
        } else {
            $this->valF['forme_juridique'] = $val['forme_juridique'];
        }
        $this->valF['nature'] = $val['nature'];
        if (!is_numeric($val['type_etablissement'])) {
            $this->valF['type_etablissement'] = NULL;
        } else {
            $this->valF['type_etablissement'] = $val['type_etablissement'];
        }
        if ($val['permis_exploitation'] == 1 || $val['permis_exploitation'] == "t" || $val['permis_exploitation'] == "Oui") {
            $this->valF['permis_exploitation'] = true;
        } else {
            $this->valF['permis_exploitation'] = false;
        }
        if ($val['date_permis_exploitation'] != "") {
            $this->valF['date_permis_exploitation'] = $this->dateDB($val['date_permis_exploitation']);
        } else {
            $this->valF['date_permis_exploitation'] = NULL;
        }
        if ($val['permis_exploitation_nuit'] == 1 || $val['permis_exploitation_nuit'] == "t" || $val['permis_exploitation_nuit'] == "Oui") {
            $this->valF['permis_exploitation_nuit'] = true;
        } else {
            $this->valF['permis_exploitation_nuit'] = false;
        }
        if ($val['date_permis_exploitation_nuit'] != "") {
            $this->valF['date_permis_exploitation_nuit'] = $this->dateDB($val['date_permis_exploitation_nuit']);
        } else {
            $this->valF['date_permis_exploitation_nuit'] = NULL;
        }
        if (!is_numeric($val['numero_voie'])) {
            $this->valF['numero_voie'] = NULL;
        } else {
            $this->valF['numero_voie'] = $val['numero_voie'];
        }
        if ($val['complement'] == "") {
            $this->valF['complement'] = NULL;
        } else {
            $this->valF['complement'] = $val['complement'];
        }
        if ($val['voie'] == "") {
            $this->valF['voie'] = NULL;
        } else {
            $this->valF['voie'] = $val['voie'];
        }
        if ($val['libelle_voie'] == "") {
            $this->valF['libelle_voie'] = NULL;
        } else {
            $this->valF['libelle_voie'] = $val['libelle_voie'];
        }
        if ($val['complement_voie'] == "") {
            $this->valF['complement_voie'] = NULL;
        } else {
            $this->valF['complement_voie'] = $val['complement_voie'];
        }
        if ($val['cp_etablissement'] == "") {
            $this->valF['cp_etablissement'] = NULL;
        } else {
            $this->valF['cp_etablissement'] = $val['cp_etablissement'];
        }
        if ($val['ville_etablissement'] == "") {
            $this->valF['ville_etablissement'] = NULL;
        } else {
            $this->valF['ville_etablissement'] = $val['ville_etablissement'];
        }
        if ($val['geom'] == "") {
            unset($this->valF['geom']);
        } else {
            $this->valF['geom'] = $val['geom'];
        }
        if (!is_numeric($val['civilite_exploitant'])) {
            $this->valF['civilite_exploitant'] = NULL;
        } else {
            $this->valF['civilite_exploitant'] = $val['civilite_exploitant'];
        }
        if ($val['nom_exploitant'] == "") {
            $this->valF['nom_exploitant'] = NULL;
        } else {
            $this->valF['nom_exploitant'] = $val['nom_exploitant'];
        }
        if ($val['nom_marital_exploitant'] == "") {
            $this->valF['nom_marital_exploitant'] = NULL;
        } else {
            $this->valF['nom_marital_exploitant'] = $val['nom_marital_exploitant'];
        }
        if ($val['prenom_exploitant'] == "") {
            $this->valF['prenom_exploitant'] = NULL;
        } else {
            $this->valF['prenom_exploitant'] = $val['prenom_exploitant'];
        }
        if ($val['date_naissance_exploitant'] != "") {
            $this->valF['date_naissance_exploitant'] = $this->dateDB($val['date_naissance_exploitant']);
        } else {
            $this->valF['date_naissance_exploitant'] = NULL;
        }
        if ($val['ville_naissance_exploitant'] == "") {
            $this->valF['ville_naissance_exploitant'] = NULL;
        } else {
            $this->valF['ville_naissance_exploitant'] = $val['ville_naissance_exploitant'];
        }
        if ($val['cp_naissance_exploitant'] == "") {
            $this->valF['cp_naissance_exploitant'] = NULL;
        } else {
            $this->valF['cp_naissance_exploitant'] = $val['cp_naissance_exploitant'];
        }
        if ($val['adresse1_exploitant'] == "") {
            $this->valF['adresse1_exploitant'] = NULL;
        } else {
            $this->valF['adresse1_exploitant'] = $val['adresse1_exploitant'];
        }
        if ($val['adresse2_exploitant'] == "") {
            $this->valF['adresse2_exploitant'] = NULL;
        } else {
            $this->valF['adresse2_exploitant'] = $val['adresse2_exploitant'];
        }
        if ($val['cp_exploitant'] == "") {
            $this->valF['cp_exploitant'] = NULL;
        } else {
            $this->valF['cp_exploitant'] = $val['cp_exploitant'];
        }
        if ($val['ville_exploitant'] == "") {
            $this->valF['ville_exploitant'] = NULL;
        } else {
            $this->valF['ville_exploitant'] = $val['ville_exploitant'];
        }
        if (!is_numeric($val['qualite_exploitant'])) {
            $this->valF['qualite_exploitant'] = NULL;
        } else {
            $this->valF['qualite_exploitant'] = $val['qualite_exploitant'];
        }
        if ($val['nationalite_exploitant'] == "") {
            $this->valF['nationalite_exploitant'] = NULL;
        } else {
            $this->valF['nationalite_exploitant'] = $val['nationalite_exploitant'];
        }
            $this->valF['particularite_exploitant'] = $val['particularite_exploitant'];
        if (!is_numeric($val['civilite_co_exploitant'])) {
            $this->valF['civilite_co_exploitant'] = NULL;
        } else {
            $this->valF['civilite_co_exploitant'] = $val['civilite_co_exploitant'];
        }
        if ($val['nom_co_exploitant'] == "") {
            $this->valF['nom_co_exploitant'] = NULL;
        } else {
            $this->valF['nom_co_exploitant'] = $val['nom_co_exploitant'];
        }
        if ($val['nom_marital_co_exploitant'] == "") {
            $this->valF['nom_marital_co_exploitant'] = NULL;
        } else {
            $this->valF['nom_marital_co_exploitant'] = $val['nom_marital_co_exploitant'];
        }
        if ($val['prenom_co_exploitant'] == "") {
            $this->valF['prenom_co_exploitant'] = NULL;
        } else {
            $this->valF['prenom_co_exploitant'] = $val['prenom_co_exploitant'];
        }
        if ($val['date_naissance_co_exploitant'] != "") {
            $this->valF['date_naissance_co_exploitant'] = $this->dateDB($val['date_naissance_co_exploitant']);
        } else {
            $this->valF['date_naissance_co_exploitant'] = NULL;
        }
        if ($val['ville_naissance_co_exploitant'] == "") {
            $this->valF['ville_naissance_co_exploitant'] = NULL;
        } else {
            $this->valF['ville_naissance_co_exploitant'] = $val['ville_naissance_co_exploitant'];
        }
        if ($val['cp_naissance_co_exploitant'] == "") {
            $this->valF['cp_naissance_co_exploitant'] = NULL;
        } else {
            $this->valF['cp_naissance_co_exploitant'] = $val['cp_naissance_co_exploitant'];
        }
        if ($val['adresse1_co_exploitant'] == "") {
            $this->valF['adresse1_co_exploitant'] = NULL;
        } else {
            $this->valF['adresse1_co_exploitant'] = $val['adresse1_co_exploitant'];
        }
        if ($val['adresse2_co_exploitant'] == "") {
            $this->valF['adresse2_co_exploitant'] = NULL;
        } else {
            $this->valF['adresse2_co_exploitant'] = $val['adresse2_co_exploitant'];
        }
        if ($val['cp_co_exploitant'] == "") {
            $this->valF['cp_co_exploitant'] = NULL;
        } else {
            $this->valF['cp_co_exploitant'] = $val['cp_co_exploitant'];
        }
        if ($val['ville_co_exploitant'] == "") {
            $this->valF['ville_co_exploitant'] = NULL;
        } else {
            $this->valF['ville_co_exploitant'] = $val['ville_co_exploitant'];
        }
        if ($val['nationalite_co_exploitant'] == "") {
            $this->valF['nationalite_co_exploitant'] = NULL;
        } else {
            $this->valF['nationalite_co_exploitant'] = $val['nationalite_co_exploitant'];
        }
        if (!is_numeric($val['qualite_co_exploitant'])) {
            $this->valF['qualite_co_exploitant'] = NULL;
        } else {
            $this->valF['qualite_co_exploitant'] = $val['qualite_co_exploitant'];
        }
            $this->valF['particularite_co_exploitant'] = $val['particularite_co_exploitant'];
        if ($val['nom_ancien_exploitant'] == "") {
            $this->valF['nom_ancien_exploitant'] = NULL;
        } else {
            $this->valF['nom_ancien_exploitant'] = $val['nom_ancien_exploitant'];
        }
        if ($val['prenom_ancien_exploitant'] == "") {
            $this->valF['prenom_ancien_exploitant'] = NULL;
        } else {
            $this->valF['prenom_ancien_exploitant'] = $val['prenom_ancien_exploitant'];
        }
        if (!is_numeric($val['qualite_ancien_exploitant'])) {
            $this->valF['qualite_ancien_exploitant'] = NULL;
        } else {
            $this->valF['qualite_ancien_exploitant'] = $val['qualite_ancien_exploitant'];
        }
        if (!is_numeric($val['civilite_proprietaire'])) {
            $this->valF['civilite_proprietaire'] = NULL;
        } else {
            $this->valF['civilite_proprietaire'] = $val['civilite_proprietaire'];
        }
        if ($val['nom_proprietaire'] == "") {
            $this->valF['nom_proprietaire'] = NULL;
        } else {
            $this->valF['nom_proprietaire'] = $val['nom_proprietaire'];
        }
        if ($val['prenom_proprietaire'] == "") {
            $this->valF['prenom_proprietaire'] = NULL;
        } else {
            $this->valF['prenom_proprietaire'] = $val['prenom_proprietaire'];
        }
        if ($val['adresse1_proprietaire'] == "") {
            $this->valF['adresse1_proprietaire'] = NULL;
        } else {
            $this->valF['adresse1_proprietaire'] = $val['adresse1_proprietaire'];
        }
        if ($val['adresse2_proprietaire'] == "") {
            $this->valF['adresse2_proprietaire'] = NULL;
        } else {
            $this->valF['adresse2_proprietaire'] = $val['adresse2_proprietaire'];
        }
        if ($val['cp_proprietaire'] == "") {
            $this->valF['cp_proprietaire'] = NULL;
        } else {
            $this->valF['cp_proprietaire'] = $val['cp_proprietaire'];
        }
        if ($val['ville_proprietaire'] == "") {
            $this->valF['ville_proprietaire'] = NULL;
        } else {
            $this->valF['ville_proprietaire'] = $val['ville_proprietaire'];
        }
        if ($val['profession_proprietaire'] == "") {
            $this->valF['profession_proprietaire'] = NULL;
        } else {
            $this->valF['profession_proprietaire'] = $val['profession_proprietaire'];
        }
        if ($val['date_fermeture'] != "") {
            $this->valF['date_fermeture'] = $this->dateDB($val['date_fermeture']);
        } else {
            $this->valF['date_fermeture'] = NULL;
        }
        if ($val['date_liquidation'] != "") {
            $this->valF['date_liquidation'] = $this->dateDB($val['date_liquidation']);
        } else {
            $this->valF['date_liquidation'] = NULL;
        }
            $this->valF['observation'] = $val['observation'];
        if ($val['ancien_proprietaire'] == "") {
            $this->valF['ancien_proprietaire'] = NULL;
        } else {
            $this->valF['ancien_proprietaire'] = $val['ancien_proprietaire'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("etablissement", "hidden");
            $form->setType("raison_sociale", "text");
            $form->setType("enseigne", "text");
            $form->setType("no_siret", "text");
            $form->setType("forme_juridique", "text");
            $form->setType("nature", "text");
            if ($this->is_in_context_of_foreign_key("type_etablissement", $this->retourformulaire)) {
                $form->setType("type_etablissement", "selecthiddenstatic");
            } else {
                $form->setType("type_etablissement", "select");
            }
            $form->setType("permis_exploitation", "checkbox");
            $form->setType("date_permis_exploitation", "date");
            $form->setType("permis_exploitation_nuit", "checkbox");
            $form->setType("date_permis_exploitation_nuit", "date");
            $form->setType("numero_voie", "text");
            $form->setType("complement", "text");
            $form->setType("voie", "text");
            $form->setType("libelle_voie", "text");
            $form->setType("complement_voie", "text");
            $form->setType("cp_etablissement", "text");
            $form->setType("ville_etablissement", "text");
            $form->setType("geom", "geom");
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("civilite_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("civilite_exploitant", "select");
            }
            $form->setType("nom_exploitant", "text");
            $form->setType("nom_marital_exploitant", "text");
            $form->setType("prenom_exploitant", "text");
            $form->setType("date_naissance_exploitant", "date");
            $form->setType("ville_naissance_exploitant", "text");
            $form->setType("cp_naissance_exploitant", "text");
            $form->setType("adresse1_exploitant", "text");
            $form->setType("adresse2_exploitant", "text");
            $form->setType("cp_exploitant", "text");
            $form->setType("ville_exploitant", "text");
            if ($this->is_in_context_of_foreign_key("qualite_exploitant", $this->retourformulaire)) {
                $form->setType("qualite_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("qualite_exploitant", "select");
            }
            $form->setType("nationalite_exploitant", "text");
            $form->setType("particularite_exploitant", "textarea");
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("civilite_co_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("civilite_co_exploitant", "select");
            }
            $form->setType("nom_co_exploitant", "text");
            $form->setType("nom_marital_co_exploitant", "text");
            $form->setType("prenom_co_exploitant", "text");
            $form->setType("date_naissance_co_exploitant", "date");
            $form->setType("ville_naissance_co_exploitant", "text");
            $form->setType("cp_naissance_co_exploitant", "text");
            $form->setType("adresse1_co_exploitant", "text");
            $form->setType("adresse2_co_exploitant", "text");
            $form->setType("cp_co_exploitant", "text");
            $form->setType("ville_co_exploitant", "text");
            $form->setType("nationalite_co_exploitant", "text");
            if ($this->is_in_context_of_foreign_key("qualite_exploitant", $this->retourformulaire)) {
                $form->setType("qualite_co_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("qualite_co_exploitant", "select");
            }
            $form->setType("particularite_co_exploitant", "textarea");
            $form->setType("nom_ancien_exploitant", "text");
            $form->setType("prenom_ancien_exploitant", "text");
            if ($this->is_in_context_of_foreign_key("qualite_exploitant", $this->retourformulaire)) {
                $form->setType("qualite_ancien_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("qualite_ancien_exploitant", "select");
            }
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("civilite_proprietaire", "selecthiddenstatic");
            } else {
                $form->setType("civilite_proprietaire", "select");
            }
            $form->setType("nom_proprietaire", "text");
            $form->setType("prenom_proprietaire", "text");
            $form->setType("adresse1_proprietaire", "text");
            $form->setType("adresse2_proprietaire", "text");
            $form->setType("cp_proprietaire", "text");
            $form->setType("ville_proprietaire", "text");
            $form->setType("profession_proprietaire", "text");
            $form->setType("date_fermeture", "date");
            $form->setType("date_liquidation", "date");
            $form->setType("observation", "textarea");
            $form->setType("ancien_proprietaire", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("etablissement", "hiddenstatic");
            $form->setType("raison_sociale", "text");
            $form->setType("enseigne", "text");
            $form->setType("no_siret", "text");
            $form->setType("forme_juridique", "text");
            $form->setType("nature", "text");
            if ($this->is_in_context_of_foreign_key("type_etablissement", $this->retourformulaire)) {
                $form->setType("type_etablissement", "selecthiddenstatic");
            } else {
                $form->setType("type_etablissement", "select");
            }
            $form->setType("permis_exploitation", "checkbox");
            $form->setType("date_permis_exploitation", "date");
            $form->setType("permis_exploitation_nuit", "checkbox");
            $form->setType("date_permis_exploitation_nuit", "date");
            $form->setType("numero_voie", "text");
            $form->setType("complement", "text");
            $form->setType("voie", "text");
            $form->setType("libelle_voie", "text");
            $form->setType("complement_voie", "text");
            $form->setType("cp_etablissement", "text");
            $form->setType("ville_etablissement", "text");
            $form->setType("geom", "geom");
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("civilite_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("civilite_exploitant", "select");
            }
            $form->setType("nom_exploitant", "text");
            $form->setType("nom_marital_exploitant", "text");
            $form->setType("prenom_exploitant", "text");
            $form->setType("date_naissance_exploitant", "date");
            $form->setType("ville_naissance_exploitant", "text");
            $form->setType("cp_naissance_exploitant", "text");
            $form->setType("adresse1_exploitant", "text");
            $form->setType("adresse2_exploitant", "text");
            $form->setType("cp_exploitant", "text");
            $form->setType("ville_exploitant", "text");
            if ($this->is_in_context_of_foreign_key("qualite_exploitant", $this->retourformulaire)) {
                $form->setType("qualite_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("qualite_exploitant", "select");
            }
            $form->setType("nationalite_exploitant", "text");
            $form->setType("particularite_exploitant", "textarea");
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("civilite_co_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("civilite_co_exploitant", "select");
            }
            $form->setType("nom_co_exploitant", "text");
            $form->setType("nom_marital_co_exploitant", "text");
            $form->setType("prenom_co_exploitant", "text");
            $form->setType("date_naissance_co_exploitant", "date");
            $form->setType("ville_naissance_co_exploitant", "text");
            $form->setType("cp_naissance_co_exploitant", "text");
            $form->setType("adresse1_co_exploitant", "text");
            $form->setType("adresse2_co_exploitant", "text");
            $form->setType("cp_co_exploitant", "text");
            $form->setType("ville_co_exploitant", "text");
            $form->setType("nationalite_co_exploitant", "text");
            if ($this->is_in_context_of_foreign_key("qualite_exploitant", $this->retourformulaire)) {
                $form->setType("qualite_co_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("qualite_co_exploitant", "select");
            }
            $form->setType("particularite_co_exploitant", "textarea");
            $form->setType("nom_ancien_exploitant", "text");
            $form->setType("prenom_ancien_exploitant", "text");
            if ($this->is_in_context_of_foreign_key("qualite_exploitant", $this->retourformulaire)) {
                $form->setType("qualite_ancien_exploitant", "selecthiddenstatic");
            } else {
                $form->setType("qualite_ancien_exploitant", "select");
            }
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("civilite_proprietaire", "selecthiddenstatic");
            } else {
                $form->setType("civilite_proprietaire", "select");
            }
            $form->setType("nom_proprietaire", "text");
            $form->setType("prenom_proprietaire", "text");
            $form->setType("adresse1_proprietaire", "text");
            $form->setType("adresse2_proprietaire", "text");
            $form->setType("cp_proprietaire", "text");
            $form->setType("ville_proprietaire", "text");
            $form->setType("profession_proprietaire", "text");
            $form->setType("date_fermeture", "date");
            $form->setType("date_liquidation", "date");
            $form->setType("observation", "textarea");
            $form->setType("ancien_proprietaire", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("etablissement", "hiddenstatic");
            $form->setType("raison_sociale", "hiddenstatic");
            $form->setType("enseigne", "hiddenstatic");
            $form->setType("no_siret", "hiddenstatic");
            $form->setType("forme_juridique", "hiddenstatic");
            $form->setType("nature", "hiddenstatic");
            $form->setType("type_etablissement", "selectstatic");
            $form->setType("permis_exploitation", "hiddenstatic");
            $form->setType("date_permis_exploitation", "hiddenstatic");
            $form->setType("permis_exploitation_nuit", "hiddenstatic");
            $form->setType("date_permis_exploitation_nuit", "hiddenstatic");
            $form->setType("numero_voie", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("voie", "hiddenstatic");
            $form->setType("libelle_voie", "hiddenstatic");
            $form->setType("complement_voie", "hiddenstatic");
            $form->setType("cp_etablissement", "hiddenstatic");
            $form->setType("ville_etablissement", "hiddenstatic");
            $form->setType("geom", "geom");
            $form->setType("civilite_exploitant", "selectstatic");
            $form->setType("nom_exploitant", "hiddenstatic");
            $form->setType("nom_marital_exploitant", "hiddenstatic");
            $form->setType("prenom_exploitant", "hiddenstatic");
            $form->setType("date_naissance_exploitant", "hiddenstatic");
            $form->setType("ville_naissance_exploitant", "hiddenstatic");
            $form->setType("cp_naissance_exploitant", "hiddenstatic");
            $form->setType("adresse1_exploitant", "hiddenstatic");
            $form->setType("adresse2_exploitant", "hiddenstatic");
            $form->setType("cp_exploitant", "hiddenstatic");
            $form->setType("ville_exploitant", "hiddenstatic");
            $form->setType("qualite_exploitant", "selectstatic");
            $form->setType("nationalite_exploitant", "hiddenstatic");
            $form->setType("particularite_exploitant", "hiddenstatic");
            $form->setType("civilite_co_exploitant", "selectstatic");
            $form->setType("nom_co_exploitant", "hiddenstatic");
            $form->setType("nom_marital_co_exploitant", "hiddenstatic");
            $form->setType("prenom_co_exploitant", "hiddenstatic");
            $form->setType("date_naissance_co_exploitant", "hiddenstatic");
            $form->setType("ville_naissance_co_exploitant", "hiddenstatic");
            $form->setType("cp_naissance_co_exploitant", "hiddenstatic");
            $form->setType("adresse1_co_exploitant", "hiddenstatic");
            $form->setType("adresse2_co_exploitant", "hiddenstatic");
            $form->setType("cp_co_exploitant", "hiddenstatic");
            $form->setType("ville_co_exploitant", "hiddenstatic");
            $form->setType("nationalite_co_exploitant", "hiddenstatic");
            $form->setType("qualite_co_exploitant", "selectstatic");
            $form->setType("particularite_co_exploitant", "hiddenstatic");
            $form->setType("nom_ancien_exploitant", "hiddenstatic");
            $form->setType("prenom_ancien_exploitant", "hiddenstatic");
            $form->setType("qualite_ancien_exploitant", "selectstatic");
            $form->setType("civilite_proprietaire", "selectstatic");
            $form->setType("nom_proprietaire", "hiddenstatic");
            $form->setType("prenom_proprietaire", "hiddenstatic");
            $form->setType("adresse1_proprietaire", "hiddenstatic");
            $form->setType("adresse2_proprietaire", "hiddenstatic");
            $form->setType("cp_proprietaire", "hiddenstatic");
            $form->setType("ville_proprietaire", "hiddenstatic");
            $form->setType("profession_proprietaire", "hiddenstatic");
            $form->setType("date_fermeture", "hiddenstatic");
            $form->setType("date_liquidation", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("ancien_proprietaire", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("etablissement", "static");
            $form->setType("raison_sociale", "static");
            $form->setType("enseigne", "static");
            $form->setType("no_siret", "static");
            $form->setType("forme_juridique", "static");
            $form->setType("nature", "static");
            $form->setType("type_etablissement", "selectstatic");
            $form->setType("permis_exploitation", "checkboxstatic");
            $form->setType("date_permis_exploitation", "datestatic");
            $form->setType("permis_exploitation_nuit", "checkboxstatic");
            $form->setType("date_permis_exploitation_nuit", "datestatic");
            $form->setType("numero_voie", "static");
            $form->setType("complement", "static");
            $form->setType("voie", "static");
            $form->setType("libelle_voie", "static");
            $form->setType("complement_voie", "static");
            $form->setType("cp_etablissement", "static");
            $form->setType("ville_etablissement", "static");
            $form->setType("geom", "geom");
            $form->setType("civilite_exploitant", "selectstatic");
            $form->setType("nom_exploitant", "static");
            $form->setType("nom_marital_exploitant", "static");
            $form->setType("prenom_exploitant", "static");
            $form->setType("date_naissance_exploitant", "datestatic");
            $form->setType("ville_naissance_exploitant", "static");
            $form->setType("cp_naissance_exploitant", "static");
            $form->setType("adresse1_exploitant", "static");
            $form->setType("adresse2_exploitant", "static");
            $form->setType("cp_exploitant", "static");
            $form->setType("ville_exploitant", "static");
            $form->setType("qualite_exploitant", "selectstatic");
            $form->setType("nationalite_exploitant", "static");
            $form->setType("particularite_exploitant", "textareastatic");
            $form->setType("civilite_co_exploitant", "selectstatic");
            $form->setType("nom_co_exploitant", "static");
            $form->setType("nom_marital_co_exploitant", "static");
            $form->setType("prenom_co_exploitant", "static");
            $form->setType("date_naissance_co_exploitant", "datestatic");
            $form->setType("ville_naissance_co_exploitant", "static");
            $form->setType("cp_naissance_co_exploitant", "static");
            $form->setType("adresse1_co_exploitant", "static");
            $form->setType("adresse2_co_exploitant", "static");
            $form->setType("cp_co_exploitant", "static");
            $form->setType("ville_co_exploitant", "static");
            $form->setType("nationalite_co_exploitant", "static");
            $form->setType("qualite_co_exploitant", "selectstatic");
            $form->setType("particularite_co_exploitant", "textareastatic");
            $form->setType("nom_ancien_exploitant", "static");
            $form->setType("prenom_ancien_exploitant", "static");
            $form->setType("qualite_ancien_exploitant", "selectstatic");
            $form->setType("civilite_proprietaire", "selectstatic");
            $form->setType("nom_proprietaire", "static");
            $form->setType("prenom_proprietaire", "static");
            $form->setType("adresse1_proprietaire", "static");
            $form->setType("adresse2_proprietaire", "static");
            $form->setType("cp_proprietaire", "static");
            $form->setType("ville_proprietaire", "static");
            $form->setType("profession_proprietaire", "static");
            $form->setType("date_fermeture", "datestatic");
            $form->setType("date_liquidation", "datestatic");
            $form->setType("observation", "textareastatic");
            $form->setType("ancien_proprietaire", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('type_etablissement','VerifNum(this)');
        $form->setOnchange('date_permis_exploitation','fdate(this)');
        $form->setOnchange('date_permis_exploitation_nuit','fdate(this)');
        $form->setOnchange('numero_voie','VerifNum(this)');
        $form->setOnchange('civilite_exploitant','VerifNum(this)');
        $form->setOnchange('date_naissance_exploitant','fdate(this)');
        $form->setOnchange('qualite_exploitant','VerifNum(this)');
        $form->setOnchange('civilite_co_exploitant','VerifNum(this)');
        $form->setOnchange('date_naissance_co_exploitant','fdate(this)');
        $form->setOnchange('qualite_co_exploitant','VerifNum(this)');
        $form->setOnchange('qualite_ancien_exploitant','VerifNum(this)');
        $form->setOnchange('civilite_proprietaire','VerifNum(this)');
        $form->setOnchange('date_fermeture','fdate(this)');
        $form->setOnchange('date_liquidation','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("etablissement", 11);
        $form->setTaille("raison_sociale", 30);
        $form->setTaille("enseigne", 30);
        $form->setTaille("no_siret", 14);
        $form->setTaille("forme_juridique", 20);
        $form->setTaille("nature", 15);
        $form->setTaille("type_etablissement", 11);
        $form->setTaille("permis_exploitation", 1);
        $form->setTaille("date_permis_exploitation", 12);
        $form->setTaille("permis_exploitation_nuit", 1);
        $form->setTaille("date_permis_exploitation_nuit", 12);
        $form->setTaille("numero_voie", 11);
        $form->setTaille("complement", 10);
        $form->setTaille("voie", 10);
        $form->setTaille("libelle_voie", 30);
        $form->setTaille("complement_voie", 30);
        $form->setTaille("cp_etablissement", 10);
        $form->setTaille("ville_etablissement", 30);
        $form->setTaille("geom", 30);
        $form->setTaille("civilite_exploitant", 11);
        $form->setTaille("nom_exploitant", 30);
        $form->setTaille("nom_marital_exploitant", 30);
        $form->setTaille("prenom_exploitant", 30);
        $form->setTaille("date_naissance_exploitant", 12);
        $form->setTaille("ville_naissance_exploitant", 30);
        $form->setTaille("cp_naissance_exploitant", 10);
        $form->setTaille("adresse1_exploitant", 30);
        $form->setTaille("adresse2_exploitant", 30);
        $form->setTaille("cp_exploitant", 10);
        $form->setTaille("ville_exploitant", 30);
        $form->setTaille("qualite_exploitant", 11);
        $form->setTaille("nationalite_exploitant", 30);
        $form->setTaille("particularite_exploitant", 80);
        $form->setTaille("civilite_co_exploitant", 11);
        $form->setTaille("nom_co_exploitant", 30);
        $form->setTaille("nom_marital_co_exploitant", 30);
        $form->setTaille("prenom_co_exploitant", 30);
        $form->setTaille("date_naissance_co_exploitant", 12);
        $form->setTaille("ville_naissance_co_exploitant", 30);
        $form->setTaille("cp_naissance_co_exploitant", 10);
        $form->setTaille("adresse1_co_exploitant", 30);
        $form->setTaille("adresse2_co_exploitant", 30);
        $form->setTaille("cp_co_exploitant", 10);
        $form->setTaille("ville_co_exploitant", 30);
        $form->setTaille("nationalite_co_exploitant", 30);
        $form->setTaille("qualite_co_exploitant", 11);
        $form->setTaille("particularite_co_exploitant", 80);
        $form->setTaille("nom_ancien_exploitant", 30);
        $form->setTaille("prenom_ancien_exploitant", 30);
        $form->setTaille("qualite_ancien_exploitant", 11);
        $form->setTaille("civilite_proprietaire", 11);
        $form->setTaille("nom_proprietaire", 30);
        $form->setTaille("prenom_proprietaire", 30);
        $form->setTaille("adresse1_proprietaire", 30);
        $form->setTaille("adresse2_proprietaire", 30);
        $form->setTaille("cp_proprietaire", 10);
        $form->setTaille("ville_proprietaire", 30);
        $form->setTaille("profession_proprietaire", 30);
        $form->setTaille("date_fermeture", 12);
        $form->setTaille("date_liquidation", 12);
        $form->setTaille("observation", 80);
        $form->setTaille("ancien_proprietaire", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("etablissement", 11);
        $form->setMax("raison_sociale", 50);
        $form->setMax("enseigne", 50);
        $form->setMax("no_siret", 14);
        $form->setMax("forme_juridique", 20);
        $form->setMax("nature", 15);
        $form->setMax("type_etablissement", 11);
        $form->setMax("permis_exploitation", 1);
        $form->setMax("date_permis_exploitation", 12);
        $form->setMax("permis_exploitation_nuit", 1);
        $form->setMax("date_permis_exploitation_nuit", 12);
        $form->setMax("numero_voie", 11);
        $form->setMax("complement", 10);
        $form->setMax("voie", 5);
        $form->setMax("libelle_voie", 40);
        $form->setMax("complement_voie", 40);
        $form->setMax("cp_etablissement", 5);
        $form->setMax("ville_etablissement", 40);
        $form->setMax("geom", 551424);
        $form->setMax("civilite_exploitant", 11);
        $form->setMax("nom_exploitant", 40);
        $form->setMax("nom_marital_exploitant", 40);
        $form->setMax("prenom_exploitant", 40);
        $form->setMax("date_naissance_exploitant", 12);
        $form->setMax("ville_naissance_exploitant", 40);
        $form->setMax("cp_naissance_exploitant", 5);
        $form->setMax("adresse1_exploitant", 40);
        $form->setMax("adresse2_exploitant", 40);
        $form->setMax("cp_exploitant", 5);
        $form->setMax("ville_exploitant", 40);
        $form->setMax("qualite_exploitant", 11);
        $form->setMax("nationalite_exploitant", 40);
        $form->setMax("particularite_exploitant", 6);
        $form->setMax("civilite_co_exploitant", 11);
        $form->setMax("nom_co_exploitant", 40);
        $form->setMax("nom_marital_co_exploitant", 40);
        $form->setMax("prenom_co_exploitant", 40);
        $form->setMax("date_naissance_co_exploitant", 12);
        $form->setMax("ville_naissance_co_exploitant", 40);
        $form->setMax("cp_naissance_co_exploitant", 5);
        $form->setMax("adresse1_co_exploitant", 40);
        $form->setMax("adresse2_co_exploitant", 40);
        $form->setMax("cp_co_exploitant", 5);
        $form->setMax("ville_co_exploitant", 40);
        $form->setMax("nationalite_co_exploitant", 40);
        $form->setMax("qualite_co_exploitant", 11);
        $form->setMax("particularite_co_exploitant", 6);
        $form->setMax("nom_ancien_exploitant", 40);
        $form->setMax("prenom_ancien_exploitant", 40);
        $form->setMax("qualite_ancien_exploitant", 11);
        $form->setMax("civilite_proprietaire", 11);
        $form->setMax("nom_proprietaire", 40);
        $form->setMax("prenom_proprietaire", 40);
        $form->setMax("adresse1_proprietaire", 40);
        $form->setMax("adresse2_proprietaire", 40);
        $form->setMax("cp_proprietaire", 5);
        $form->setMax("ville_proprietaire", 40);
        $form->setMax("profession_proprietaire", 40);
        $form->setMax("date_fermeture", 12);
        $form->setMax("date_liquidation", 12);
        $form->setMax("observation", 6);
        $form->setMax("ancien_proprietaire", 250);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('etablissement', __('etablissement'));
        $form->setLib('raison_sociale', __('raison_sociale'));
        $form->setLib('enseigne', __('enseigne'));
        $form->setLib('no_siret', __('no_siret'));
        $form->setLib('forme_juridique', __('forme_juridique'));
        $form->setLib('nature', __('nature'));
        $form->setLib('type_etablissement', __('type_etablissement'));
        $form->setLib('permis_exploitation', __('permis_exploitation'));
        $form->setLib('date_permis_exploitation', __('date_permis_exploitation'));
        $form->setLib('permis_exploitation_nuit', __('permis_exploitation_nuit'));
        $form->setLib('date_permis_exploitation_nuit', __('date_permis_exploitation_nuit'));
        $form->setLib('numero_voie', __('numero_voie'));
        $form->setLib('complement', __('complement'));
        $form->setLib('voie', __('voie'));
        $form->setLib('libelle_voie', __('libelle_voie'));
        $form->setLib('complement_voie', __('complement_voie'));
        $form->setLib('cp_etablissement', __('cp_etablissement'));
        $form->setLib('ville_etablissement', __('ville_etablissement'));
        $form->setLib('geom', __('geom'));
        $form->setLib('civilite_exploitant', __('civilite_exploitant'));
        $form->setLib('nom_exploitant', __('nom_exploitant'));
        $form->setLib('nom_marital_exploitant', __('nom_marital_exploitant'));
        $form->setLib('prenom_exploitant', __('prenom_exploitant'));
        $form->setLib('date_naissance_exploitant', __('date_naissance_exploitant'));
        $form->setLib('ville_naissance_exploitant', __('ville_naissance_exploitant'));
        $form->setLib('cp_naissance_exploitant', __('cp_naissance_exploitant'));
        $form->setLib('adresse1_exploitant', __('adresse1_exploitant'));
        $form->setLib('adresse2_exploitant', __('adresse2_exploitant'));
        $form->setLib('cp_exploitant', __('cp_exploitant'));
        $form->setLib('ville_exploitant', __('ville_exploitant'));
        $form->setLib('qualite_exploitant', __('qualite_exploitant'));
        $form->setLib('nationalite_exploitant', __('nationalite_exploitant'));
        $form->setLib('particularite_exploitant', __('particularite_exploitant'));
        $form->setLib('civilite_co_exploitant', __('civilite_co_exploitant'));
        $form->setLib('nom_co_exploitant', __('nom_co_exploitant'));
        $form->setLib('nom_marital_co_exploitant', __('nom_marital_co_exploitant'));
        $form->setLib('prenom_co_exploitant', __('prenom_co_exploitant'));
        $form->setLib('date_naissance_co_exploitant', __('date_naissance_co_exploitant'));
        $form->setLib('ville_naissance_co_exploitant', __('ville_naissance_co_exploitant'));
        $form->setLib('cp_naissance_co_exploitant', __('cp_naissance_co_exploitant'));
        $form->setLib('adresse1_co_exploitant', __('adresse1_co_exploitant'));
        $form->setLib('adresse2_co_exploitant', __('adresse2_co_exploitant'));
        $form->setLib('cp_co_exploitant', __('cp_co_exploitant'));
        $form->setLib('ville_co_exploitant', __('ville_co_exploitant'));
        $form->setLib('nationalite_co_exploitant', __('nationalite_co_exploitant'));
        $form->setLib('qualite_co_exploitant', __('qualite_co_exploitant'));
        $form->setLib('particularite_co_exploitant', __('particularite_co_exploitant'));
        $form->setLib('nom_ancien_exploitant', __('nom_ancien_exploitant'));
        $form->setLib('prenom_ancien_exploitant', __('prenom_ancien_exploitant'));
        $form->setLib('qualite_ancien_exploitant', __('qualite_ancien_exploitant'));
        $form->setLib('civilite_proprietaire', __('civilite_proprietaire'));
        $form->setLib('nom_proprietaire', __('nom_proprietaire'));
        $form->setLib('prenom_proprietaire', __('prenom_proprietaire'));
        $form->setLib('adresse1_proprietaire', __('adresse1_proprietaire'));
        $form->setLib('adresse2_proprietaire', __('adresse2_proprietaire'));
        $form->setLib('cp_proprietaire', __('cp_proprietaire'));
        $form->setLib('ville_proprietaire', __('ville_proprietaire'));
        $form->setLib('profession_proprietaire', __('profession_proprietaire'));
        $form->setLib('date_fermeture', __('date_fermeture'));
        $form->setLib('date_liquidation', __('date_liquidation'));
        $form->setLib('observation', __('observation'));
        $form->setLib('ancien_proprietaire', __('ancien_proprietaire'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // civilite_co_exploitant
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "civilite_co_exploitant",
            $this->get_var_sql_forminc__sql("civilite_co_exploitant"),
            $this->get_var_sql_forminc__sql("civilite_co_exploitant_by_id"),
            false
        );
        // civilite_exploitant
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "civilite_exploitant",
            $this->get_var_sql_forminc__sql("civilite_exploitant"),
            $this->get_var_sql_forminc__sql("civilite_exploitant_by_id"),
            false
        );
        // civilite_proprietaire
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "civilite_proprietaire",
            $this->get_var_sql_forminc__sql("civilite_proprietaire"),
            $this->get_var_sql_forminc__sql("civilite_proprietaire_by_id"),
            false
        );
        // qualite_ancien_exploitant
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "qualite_ancien_exploitant",
            $this->get_var_sql_forminc__sql("qualite_ancien_exploitant"),
            $this->get_var_sql_forminc__sql("qualite_ancien_exploitant_by_id"),
            false
        );
        // qualite_co_exploitant
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "qualite_co_exploitant",
            $this->get_var_sql_forminc__sql("qualite_co_exploitant"),
            $this->get_var_sql_forminc__sql("qualite_co_exploitant_by_id"),
            false
        );
        // qualite_exploitant
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "qualite_exploitant",
            $this->get_var_sql_forminc__sql("qualite_exploitant"),
            $this->get_var_sql_forminc__sql("qualite_exploitant_by_id"),
            false
        );
        // type_etablissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "type_etablissement",
            $this->get_var_sql_forminc__sql("type_etablissement"),
            $this->get_var_sql_forminc__sql("type_etablissement_by_id"),
            false
        );
        // geom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("etablissement", $this->getParameter("idx"), "0");
            $form->setSelect("geom", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('type_etablissement', $this->retourformulaire))
                $form->setVal('type_etablissement', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('titre_de_civilite', $this->retourformulaire))
                $form->setVal('civilite_co_exploitant', $idxformulaire);
            if($this->is_in_context_of_foreign_key('titre_de_civilite', $this->retourformulaire))
                $form->setVal('civilite_exploitant', $idxformulaire);
            if($this->is_in_context_of_foreign_key('titre_de_civilite', $this->retourformulaire))
                $form->setVal('civilite_proprietaire', $idxformulaire);
            if($this->is_in_context_of_foreign_key('qualite_exploitant', $this->retourformulaire))
                $form->setVal('qualite_ancien_exploitant', $idxformulaire);
            if($this->is_in_context_of_foreign_key('qualite_exploitant', $this->retourformulaire))
                $form->setVal('qualite_co_exploitant', $idxformulaire);
            if($this->is_in_context_of_foreign_key('qualite_exploitant', $this->retourformulaire))
                $form->setVal('qualite_exploitant', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "etablissement", $id);
        // Verification de la cle secondaire : demande_licence
        $this->rechercheTable($this->f->db, "demande_licence", "etablissement", $id);
        // Verification de la cle secondaire : dossier
        $this->rechercheTable($this->f->db, "dossier", "etablissement", $id);
    }


}
