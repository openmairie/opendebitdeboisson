<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

require_once "../obj/om_dbform.class.php";

class dossier_gen extends om_dbform {

    protected $_absolute_class_name = "dossier";

    var $table = "dossier";
    var $clePrimaire = "dossier";
    var $typeCle = "N";
    var $required_field = array(
        "date_piece",
        "dossier",
        "etablissement",
        "fichier"
    );
    
    var $foreign_keys_extended = array(
        "etablissement" => array("etablissement", "etablissement_permanent", "etablissement_temporaire", ),
    );
    var $abstract_type = array(
        "fichier" => "file",
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("date_piece");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "dossier",
            "date_piece",
            "fichier",
            "observation",
            "etablissement",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement() {
        return "SELECT etablissement.etablissement, etablissement.raison_sociale FROM ".DB_PREFIXE."etablissement ORDER BY etablissement.raison_sociale ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_by_id() {
        return "SELECT etablissement.etablissement, etablissement.raison_sociale FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier'])) {
            $this->valF['dossier'] = ""; // -> requis
        } else {
            $this->valF['dossier'] = $val['dossier'];
        }
        if ($val['date_piece'] != "") {
            $this->valF['date_piece'] = $this->dateDB($val['date_piece']);
        }
        $this->valF['fichier'] = $val['fichier'];
        if ($val['observation'] == "") {
            $this->valF['observation'] = NULL;
        } else {
            $this->valF['observation'] = $val['observation'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = ""; // -> requis
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier", "hidden");
            $form->setType("date_piece", "date");
            if ($this->retourformulaire == "") {
                $form->setType("fichier", "upload");
            } else {
                $form->setType("fichier", "upload2");
            }
            $form->setType("observation", "text");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier", "hiddenstatic");
            $form->setType("date_piece", "date");
            if ($this->retourformulaire == "") {
                $form->setType("fichier", "upload");
            } else {
                $form->setType("fichier", "upload2");
            }
            $form->setType("observation", "text");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier", "hiddenstatic");
            $form->setType("date_piece", "hiddenstatic");
            $form->setType("fichier", "filestatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier", "static");
            $form->setType("date_piece", "datestatic");
            $form->setType("fichier", "file");
            $form->setType("observation", "static");
            $form->setType("etablissement", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier','VerifNum(this)');
        $form->setOnchange('date_piece','fdate(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier", 11);
        $form->setTaille("date_piece", 12);
        $form->setTaille("fichier", 30);
        $form->setTaille("observation", 30);
        $form->setTaille("etablissement", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier", 11);
        $form->setMax("date_piece", 12);
        $form->setMax("fichier", 50);
        $form->setMax("observation", 200);
        $form->setMax("etablissement", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier', __('dossier'));
        $form->setLib('date_piece', __('date_piece'));
        $form->setLib('fichier', __('fichier'));
        $form->setLib('observation', __('observation'));
        $form->setLib('etablissement', __('etablissement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // etablissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement",
            $this->get_var_sql_forminc__sql("etablissement"),
            $this->get_var_sql_forminc__sql("etablissement_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
