<?php
//$Id$ 
//gen openMairie le 07/08/2018 10:12

require_once "../obj/om_dbform.class.php";

class courrier_gen extends om_dbform {

    protected $_absolute_class_name = "courrier";

    var $table = "courrier";
    var $clePrimaire = "courrier";
    var $typeCle = "N";
    var $required_field = array(
        "courrier",
        "etablissement",
        "fichier_finalise",
        "modele_lettre_type"
    );
    
    var $foreign_keys_extended = array(
        "etablissement" => array("etablissement", "etablissement_permanent", "etablissement_temporaire", ),
    );
    var $abstract_type = array(
        "fichier" => "file",
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("date_courrier");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "courrier",
            "date_courrier",
            "modele_lettre_type",
            "objet",
            "corps_om_html",
            "fichier",
            "fichier_finalise",
            "etablissement",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement() {
        return "SELECT etablissement.etablissement, etablissement.raison_sociale FROM ".DB_PREFIXE."etablissement ORDER BY etablissement.raison_sociale ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_by_id() {
        return "SELECT etablissement.etablissement, etablissement.raison_sociale FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['courrier'])) {
            $this->valF['courrier'] = ""; // -> requis
        } else {
            $this->valF['courrier'] = $val['courrier'];
        }
        if ($val['date_courrier'] != "") {
            $this->valF['date_courrier'] = $this->dateDB($val['date_courrier']);
        } else {
            $this->valF['date_courrier'] = NULL;
        }
        $this->valF['modele_lettre_type'] = $val['modele_lettre_type'];
        if ($val['objet'] == "") {
            $this->valF['objet'] = NULL;
        } else {
            $this->valF['objet'] = $val['objet'];
        }
            $this->valF['corps_om_html'] = $val['corps_om_html'];
        if ($val['fichier'] == "") {
            $this->valF['fichier'] = NULL;
        } else {
            $this->valF['fichier'] = $val['fichier'];
        }
        if ($val['fichier_finalise'] == 1 || $val['fichier_finalise'] == "t" || $val['fichier_finalise'] == "Oui") {
            $this->valF['fichier_finalise'] = true;
        } else {
            $this->valF['fichier_finalise'] = false;
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = ""; // -> requis
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("courrier", "hidden");
            $form->setType("date_courrier", "date");
            $form->setType("modele_lettre_type", "text");
            $form->setType("objet", "text");
            $form->setType("corps_om_html", "html");
            if ($this->retourformulaire == "") {
                $form->setType("fichier", "upload");
            } else {
                $form->setType("fichier", "upload2");
            }
            $form->setType("fichier_finalise", "checkbox");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("courrier", "hiddenstatic");
            $form->setType("date_courrier", "date");
            $form->setType("modele_lettre_type", "text");
            $form->setType("objet", "text");
            $form->setType("corps_om_html", "html");
            if ($this->retourformulaire == "") {
                $form->setType("fichier", "upload");
            } else {
                $form->setType("fichier", "upload2");
            }
            $form->setType("fichier_finalise", "checkbox");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("courrier", "hiddenstatic");
            $form->setType("date_courrier", "hiddenstatic");
            $form->setType("modele_lettre_type", "hiddenstatic");
            $form->setType("objet", "hiddenstatic");
            $form->setType("corps_om_html", "hiddenstatic");
            $form->setType("fichier", "filestatic");
            $form->setType("fichier_finalise", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("courrier", "static");
            $form->setType("date_courrier", "datestatic");
            $form->setType("modele_lettre_type", "static");
            $form->setType("objet", "static");
            $form->setType("corps_om_html", "htmlstatic");
            $form->setType("fichier", "file");
            $form->setType("fichier_finalise", "checkboxstatic");
            $form->setType("etablissement", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('courrier','VerifNum(this)');
        $form->setOnchange('date_courrier','fdate(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("courrier", 11);
        $form->setTaille("date_courrier", 12);
        $form->setTaille("modele_lettre_type", 30);
        $form->setTaille("objet", 30);
        $form->setTaille("corps_om_html", 80);
        $form->setTaille("fichier", 30);
        $form->setTaille("fichier_finalise", 1);
        $form->setTaille("etablissement", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("courrier", 11);
        $form->setMax("date_courrier", 12);
        $form->setMax("modele_lettre_type", 50);
        $form->setMax("objet", 80);
        $form->setMax("corps_om_html", 6);
        $form->setMax("fichier", 50);
        $form->setMax("fichier_finalise", 1);
        $form->setMax("etablissement", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('courrier', __('courrier'));
        $form->setLib('date_courrier', __('date_courrier'));
        $form->setLib('modele_lettre_type', __('modele_lettre_type'));
        $form->setLib('objet', __('objet'));
        $form->setLib('corps_om_html', __('corps_om_html'));
        $form->setLib('fichier', __('fichier'));
        $form->setLib('fichier_finalise', __('fichier_finalise'));
        $form->setLib('etablissement', __('etablissement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // etablissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement",
            $this->get_var_sql_forminc__sql("etablissement"),
            $this->get_var_sql_forminc__sql("etablissement_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
