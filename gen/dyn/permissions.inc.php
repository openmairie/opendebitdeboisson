<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

$files_to_avoid = array(
	"carte_globale.map.class.php",
    "opendebitdeboisson.class.php",
    "om_application_override.class.php",
    "om_dbform.class.php",
    "om_formulaire.class.php",
);

$permissions = array(
);
