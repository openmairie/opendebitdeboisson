<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

/**
 * Surcharge applicative de la classe 'om_dbform'.
 */
$om_dbform_path_override = "../obj/om_dbform.class.php";
$om_dbform_class_override = "om_dbform";

/**
 *
 */
$administration_parametrage = "administration & paramétrage";
$application = "application";

/**
 *
 */
$tables_to_overload = array(
    // A&M
    "adresse_postale" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "adresses postales",
        "breadcrumb_in_page_title" => array($administration_parametrage, "géolocalisation", ),
        "displayed_fields_in_tableinc" => array(
            "rivoli", "numero", "libelle", "complement",
        ),
    ),
    // METIER
    "courrier" => array(
        "tablename_in_page_title" => "courriers",
        "breadcrumb_in_page_title" => array($application, ),
        "displayed_fields_in_tableinc" => array(
            "date_courrier", "modele_lettre_type", "objet", "fichier_finalise", "etablissement",
        ),
        "specific_config_for_fields" => array(
            "fichier" => array(
                "abstract_type" => "file",
            ),
        ),
    ),
    // METIER
    "demande_licence" => array(
        "tablename_in_page_title" => "demandes de licence",
        "breadcrumb_in_page_title" => array($application, ),
        "displayed_fields_in_tableinc" => array(
            "type_licence", "numero_licence", "terme", "date_debut_validite", "date_fin_validite", "etablissement",
        ),
    ),
    // METIER
    "dossier" => array(
        "tablename_in_page_title" => "dossiers",
        "breadcrumb_in_page_title" => array($application, ),
        "displayed_fields_in_tableinc" => array(
            "date_piece", "observation", "etablissement",
        ),
        "specific_config_for_fields" => array(
            "fichier" => array(
                "abstract_type" => "file",
            ),
        ),
    ),
    // METIER
    "etablissement" => array(
        "tablename_in_page_title" => "établissements",
        "breadcrumb_in_page_title" => array($application, ),
        "displayed_fields_in_tableinc" => array(
            "raison_sociale", "enseigne", "libelle_voie", "nom_exploitant", "nom_proprietaire",
        ),
        "extended_class" => array("etablissement_permanent", "etablissement_temporaire", ),
    ),
    // A&M
    "qualite_exploitant" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "qualités de l'exploitant",
        "breadcrumb_in_page_title" => array($administration_parametrage, "tables de référence", ),
    ),
    // FMWK
    "om_collectivite" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "général", ),
    ),
    // FMWK
    "om_dashboard" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "tableaux de bord", ),
    ),
    // FMWK
    "om_droit" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "gestion des utilisateurs", ),
    ),
    // FMWK
    "om_etat" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_lettretype" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_logo" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_parametre" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "général", ),
    ),
    // FMWK
    "om_permission" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "gestion des utilisateurs", ),
    ),
    // FMWK
    "om_profil" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "gestion des utilisateurs", ),
    ),
    // FMWK
    "om_requete" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_sig_extent" => array(
        "tablename_in_page_title" => "étendue",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sig_flux" => array(
        "tablename_in_page_title" => "flux",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sig_map" => array(
        "tablename_in_page_title" => "carte",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sig_map_comp" => array(
        "tablename_in_page_title" => "géométrie",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sig_map_flux" => array(
        "tablename_in_page_title" => "flux appliqué à une carte",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sousetat" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_utilisateur" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "gestion des utilisateurs", ),
        "displayed_fields_in_tableinc" => array(
            "nom", "email", "login", "om_type", "om_profil",
        ),
    ),
    // FMWK
    "om_widget" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "tableaux de bord", ),
        "displayed_fields_in_tableinc" => array(
            "libelle", "om_profil", "type",
        ),
    ),
    // METIER
    "perimetre" => array(
        "tablename_in_page_title" => "périmètres d'exclusion",
        "breadcrumb_in_page_title" => array($application, ),
        "displayed_fields_in_tableinc" => array(
            "libelle", "longueur_exclusion_metre",
        ),
    ),
    // A&M
    "rivoli" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "rivoli",
        "breadcrumb_in_page_title" => array($administration_parametrage, "géolocalisation", ),
    ),
    // A&M
    "statut_demande" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "statuts de la demande",
        "breadcrumb_in_page_title" => array($administration_parametrage, "tables de référence", ),
    ),
    // A&M
    "terme" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "termes",
        "breadcrumb_in_page_title" => array($administration_parametrage, "tables de référence", ),
    ),
    // A&M
    "titre_de_civilite" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "titres de civilités",
        "breadcrumb_in_page_title" => array($administration_parametrage, "tables de référence", ),
    ),
    // A&M
    "type_demande" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "types de demandes",
        "breadcrumb_in_page_title" => array($administration_parametrage, "tables de référence", ),
    ),
    // A&M
    "type_etablissement" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "types d'établissements",
        "breadcrumb_in_page_title" => array($administration_parametrage, "tables de référence", ),
    ),
    // A&M
    "type_licence" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "types de licences",
        "breadcrumb_in_page_title" => array($administration_parametrage, "tables de référence", ),
    ),
);
