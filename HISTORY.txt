openDébitDeBoisson Historique des versions
==========================================

2.2.1 (unreleased)
------------------

* Aucun changement.


2.2.0 (30/12/2022)
------------------

* Évolution : Amélioration du layout. Ticket #10055.

* Évolution : Ajout de 'Valeur alternative possible' sur certaines colonnes dans les imports établissement et demande_licence. Ticket #9637.

* Évolution : Augmentation des colonnes enseigne et raison sociale de la table établissement de 40 à 50. Ticket #9636.

* Correction : Mise à jour du framework openMairie > v4.10.0 & Compatibilité PHP8.0. Ticket #9635.

* Correction : Problème d'affichage du menu d'administration & paramétrage. Ticket #9638.


2.1.0 (22/07/2019)
------------------

* Évolution : Ajout d'une requête mémorisée listant les demandes de licence avec toutes les informations de l'établissement lié. Ticket #9255.

* Évolution : Ajout d'un widget de statistique affichant le nombre de licences par type filtré par date de fin de validité et sans les demandes de transfert. Ticket #9256.

* Évolution : Ajout du champ de fusion [courrier.corps_texte] sur les courriers permettant de récupérer la valeur du champ "corps" sans style d'affichage. Ticket #9257.


2.0.0 (07/01/2019)
------------------

* Évolution : Uniformisation de la taille des champs de la table des établissements à 40 caractères.

* Évolution : Ajout du champ interfacé 'ancien_proprietaire' sur l'établissement.

* Évolution : Ajout du champ interfacé 'observation' sur l'établissement.

* Évolution : Modification de la saisie d'une demande de licence.
  - Le champ 'date_fin_validite' est désormais obligatoire.
  - Dans la version précédente si aucune valeur n'était saisie dans ce champs alors c'est la valeur date du jour + 20 ans qui était enregistrée en base de données, désormais dès que le champ 'date_debut_validite' est saisie alors la valeur est récupérée pour l'additionner à 100 ans et la saisir dans le champ 'date_fin_validite' en javascript.

* Évolution : Désactivation du paramètre permettant la hiérarchisation des permissions.

* Évolution : Modification de la récupération des champs de fusion par objet pour les établissements.

* Évolution : Renommage de la table exploitant en qualite_exploitant ainsi que des trois colonnes rattachées dans la table établissement (exploitant > qualite_exploitant, co_exploitant > qualite_co_exploitant et ancien_exploitant > qualite_ancien_exploitant).

* Évolution : Ajout d'une case à cocher sur le formulaire des établissements permanents pour copier les informations de l'exploitant dans les champs du propriétaire.

* Évolution : Modification du bouton de récupération de l'adresse postale par un champ autocomplete sur les formulaires de l'établissement permanent et du périmètre d'exclusion.

* Évolution : Mise à niveau vers la version 4.9.0 du framework openMairie.

* Évolution : Ajout du champ 'type d'établissement' sur l'établissement avec sa table de référence liée. Non obligatoire, l'objectif est de qualifier le type d'établissement à titre informatif.

* Évolution : Optimisation du modèle de données avec l'ajout du caractère obligatoire de certaines colonnes, l'ajout de clés primaires, l'ajout de clés étrangères, ...

* Évolution : Remplacement de l'ancien système de stockage des fichiers par le filesystem_storage. Ceci entraine la suppression de la colonne ancien_nom sur le dossier et le renommage de la colonne 'nom' par 'fichier'.

* Évolution : Suppression du champ 'ancien_proprietaire' sur l'établissement. Ce champ n'est pas interfacé et ne comporte aucun traitement.

* Évolution : Dans la table courrier, désormais on doit sélectionner une lettretype qui sera le modèle de l'édition (la colonne 'modele_lettre_type' est ajoutée), les champs 'objet' (anciennement 'type_courrier') (texte simple) et 'corps_om_html' (anciennement 'zone_texte') (texte riche) seront utilisés comme champs de fusion. L'édition sera désormais stockée sur le système de fichier lorsque l'utilisateur la finalisera (les colonnes 'fichier' et 'fichier_finalise' sont ajoutées). La mention document de travail apparaît en filigrane quand l'édition n'est pas finalisée et l'action modifier n'est pas disponible si le document est finalisé.

* Évolution : Changement des types de champ de la table etablissement pour permis_exploitation et permis_exploitation_nuit de character varying(3) vers boolean.

* Évolution : Modification de la saisie d'une demande de licence.
  - 'date_demande_licence' : dans la version précédente si aucune valeur n'était saisie dans ce champ alors c'est la date du jour qui était enregistrée en base de données. Désormais le formulaire d'ajout se charge avec la date du jour pré-saisie, l'utilisateur peut la changer et le champ est désormais obligatoire.
  - 'heure_debut' : dans la version précédente si aucune valeur n'était saisie dans ce champs alors c'est la valeur '00:00:00' qui était enregistrée en base de données. Désormais le formulaire d'ajout se charge avec cette même valeur pré-saisie, l'utilisateur peut la changer et le champ est désormais obligatoire.
  - 'heure_fin' : dans la version précédente si aucune valeur n'était saisie dans ce champs alors c'est la valeur '23:59:59' qui était enregistrée en base de données. Désormais le formulaire d'ajout se charge avec cette même valeur pré-saisie, l'utilisateur peut la changer et le champ est désormais obligatoire.
  - 'statut_demande': ajout du champ 'statut de la demande' sur la demande de licence avec sa table de référence liée. Non obligatoire, l'objectif est de qualifier l'état de la demande à titre informatif.
  - 'numero_licence' : ajout du champ 'numéro de licence' sur la demande de licence. Non obligatoire, il permet d'identifier facilement une licence.
  - 'ancien_exploitant' : suppression du champ du modèle de données. Le champ n'est jamais utilisé.
  - 'date_debut_validite' : Le champ est désormais obligatoire. Dans la version précédente si aucune valeur n'était saisie dans ce champs alors c'est la valeur date de demande + 15 jours qui était enregistrée en base de données. Désormais l'utilisateur devra saisir la date explicitement.
  - 'date_fin_validite' : Le champ est désormais obligatoire. Dans la version précédente si aucune valeur n'était saisie dans ce champs alors c'est la valeur date du jour + 20 ans qui était enregistrée en base de données. Désormais l'utilisateur devra saisir la date explicitement.

* Évolution : Correction des traitements de demande de licence.
 - lien vers les établissements à proximité ;
 - doublons dans la liste de établissements à proximité.

Ticket de suivi de la version #9116.
