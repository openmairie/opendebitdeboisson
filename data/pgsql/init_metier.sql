--
CREATE TABLE adresse_postale (
    adresse_postale integer NOT NULL,
    rivoli character varying(10),
    numero integer,
    complement character varying(10),
    libelle character varying(100),
    geom public.geometry(Point,2154),
    PRIMARY KEY(adresse_postale)
);
CREATE SEQUENCE adresse_postale_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE adresse_postale_seq OWNED BY adresse_postale.adresse_postale;

--
CREATE TABLE courrier(
    courrier integer NOT NULL,
    date_courrier DATE,
    modele_lettre_type character varying(50) NOT NULL,
    objet character varying(80),
    corps_om_html TEXT,
    fichier character varying(50),
    fichier_finalise boolean NOT NULL,
    etablissement integer NOT NULL,
    PRIMARY KEY(courrier)
);
CREATE SEQUENCE courrier_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE courrier_seq OWNED BY courrier.courrier;

--
CREATE TABLE demande_licence(
    demande_licence integer NOT NULL,
    date_demande_licence DATE NOT NULL,
    date_ancienne_demande DATE,
    type_demande integer NOT NULL,
    type_licence integer NOT NULL,
    compte_rendu TEXT,

    statut_demande integer,
    numero_licence character varying(50),

    occasion character varying(50),
    particularite TEXT,

    terme integer NOT NULL,
    date_debut_validite DATE NOT NULL,
    heure_debut TIME NOT NULL,
    date_fin_validite DATE NOT NULL,
    heure_fin TIME NOT NULL,

    etablissement integer NOT NULL,
    PRIMARY KEY (demande_licence)
);
CREATE SEQUENCE demande_licence_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE demande_licence_seq OWNED BY demande_licence.demande_licence;

--
CREATE TABLE dossier(
    dossier integer NOT NULL,
    date_piece DATE NOT NULL,
    fichier character varying(50) NOT NULL,
    observation character varying(200),
    etablissement integer NOT NULL,
    PRIMARY KEY (dossier)
);
CREATE SEQUENCE dossier_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE dossier_seq OWNED BY dossier.dossier;

--
CREATE TABLE etablissement(
    etablissement integer NOT NULL,
    raison_sociale character varying(50) NOT NULL,
    enseigne character varying(50) NOT NULL,
    no_siret character varying(14),
    forme_juridique character varying(20),
    nature character varying(15) NOT NULL,
    type_etablissement integer,
    permis_exploitation boolean NOT NULL DEFAULT false,
    date_permis_exploitation DATE,
    permis_exploitation_nuit boolean NOT NULL DEFAULT false,
    date_permis_exploitation_nuit DATE,

    numero_voie integer,
    complement character varying(10),
    voie character varying(5),
    libelle_voie character varying(40),
    complement_voie character varying(40),
    cp_etablissement character varying(5),
    ville_etablissement character varying(40),
    geom public.geometry(Point,2154),

    civilite_exploitant integer,
    nom_exploitant character varying(40),
    nom_marital_exploitant character varying(40),
    prenom_exploitant character varying(40),
    date_naissance_exploitant DATE,
    ville_naissance_exploitant character varying(40),
    cp_naissance_exploitant CHARACTER(5),
    adresse1_exploitant character varying(40),
    adresse2_exploitant character varying(40),
    cp_exploitant CHARACTER(5),
    ville_exploitant character varying(40),
    qualite_exploitant integer,
    nationalite_exploitant character varying(40),
    particularite_exploitant TEXT,

    civilite_co_exploitant integer,
    nom_co_exploitant character varying(40),
    nom_marital_co_exploitant character varying(40),
    prenom_co_exploitant character varying(40),
    date_naissance_co_exploitant DATE,
    ville_naissance_co_exploitant character varying(40),
    cp_naissance_co_exploitant CHARACTER(5),
    adresse1_co_exploitant character varying(40),
    adresse2_co_exploitant character varying(40),
    cp_co_exploitant CHARACTER(5),
    ville_co_exploitant character varying(40),
    nationalite_co_exploitant character varying(40),
    qualite_co_exploitant integer,
    particularite_co_exploitant TEXT,

    nom_ancien_exploitant character varying(40),
    prenom_ancien_exploitant character varying(40),
    qualite_ancien_exploitant integer,

    civilite_proprietaire integer,
    nom_proprietaire character varying(40),
    prenom_proprietaire character varying(40),
    adresse1_proprietaire character varying(40),
    adresse2_proprietaire character varying(40),
    cp_proprietaire CHARACTER(5),
    ville_proprietaire character varying(40),
    profession_proprietaire character varying(40),

    date_fermeture DATE,
    date_liquidation DATE,

    observation text,
    ancien_proprietaire character varying(250),

    PRIMARY KEY (etablissement)
);
CREATE SEQUENCE etablissement_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE etablissement_seq OWNED BY etablissement.etablissement;
--
COMMENT ON COLUMN etablissement.observation IS 'Observations concernant l''établissement';
COMMENT ON COLUMN etablissement.ancien_proprietaire IS 'Ancien propriétaire de l''établissement';

--
CREATE TABLE qualite_exploitant(
    qualite_exploitant integer NOT NULL,
    libelle character varying(30) NOT NULL,
    PRIMARY KEY (qualite_exploitant)
);
CREATE SEQUENCE qualite_exploitant_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE qualite_exploitant_seq OWNED BY qualite_exploitant.qualite_exploitant;

--
CREATE TABLE perimetre(
    perimetre integer NOT NULL,
    libelle character varying(70) NOT NULL,
    longueur_exclusion_metre integer NOT NULL,

    numero_voie integer,
    complement character varying(10),
    voie character varying(5),
    libelle_voie character varying(40),
    complement_voie character varying(40),
    geom public.geometry(Point,2154),

    PRIMARY KEY (perimetre)
);
CREATE SEQUENCE perimetre_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE perimetre_seq OWNED BY perimetre.perimetre;

--
CREATE TABLE rivoli (
    rivoli character varying(4) NOT NULL,
    libelle character varying(80) NOT NULL,
    PRIMARY KEY(rivoli)
);

--
CREATE TABLE statut_demande(
    statut_demande integer NOT NULL,
    libelle character varying(100) NOT NULL,
    PRIMARY KEY (statut_demande)
);
CREATE SEQUENCE statut_demande_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE statut_demande_seq OWNED BY statut_demande.statut_demande;

--
CREATE TABLE terme(
    terme integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(30) NOT NULL,
    PRIMARY KEY (terme)
);
CREATE SEQUENCE terme_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE terme_seq OWNED BY terme.terme;

--
CREATE TABLE titre_de_civilite(
    titre_de_civilite integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(20) NOT NULL,
    PRIMARY KEY (titre_de_civilite)
);
CREATE SEQUENCE titre_de_civilite_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE titre_de_civilite_seq OWNED BY titre_de_civilite.titre_de_civilite;

--
CREATE TABLE type_demande(
    type_demande integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(20) NOT NULL,
    PRIMARY KEY (type_demande)
);
CREATE SEQUENCE type_demande_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE type_demande_seq OWNED BY type_demande.type_demande;

--
CREATE TABLE type_etablissement(
    type_etablissement integer NOT NULL,
    libelle character varying(100) NOT NULL,
    PRIMARY KEY (type_etablissement)
);
CREATE SEQUENCE type_etablissement_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE type_etablissement_seq OWNED BY type_etablissement.type_etablissement;

--
CREATE TABLE type_licence(
    type_licence integer NOT NULL,
    libelle character varying(50) NOT NULL,
    contrainte_proximite boolean NOT NULL DEFAULT false,
    PRIMARY KEY  (type_licence)
);
CREATE SEQUENCE type_licence_seq
    INCREMENT 1
    NO MINVALUE
    NO MAXVALUE
    START 1
    CACHE 1;
ALTER SEQUENCE type_licence_seq OWNED BY type_licence.type_licence;

-- references
ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_etablissement
    FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);
ALTER TABLE ONLY dossier
    ADD CONSTRAINT dossier_etablissement
    FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);
ALTER TABLE ONLY demande_licence
    ADD CONSTRAINT demande_licence_etablissement
    FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_qualite_exploitant
    FOREIGN KEY (qualite_exploitant) REFERENCES qualite_exploitant(qualite_exploitant);
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_qualite_co_exploitant
    FOREIGN KEY (qualite_co_exploitant) REFERENCES qualite_exploitant(qualite_exploitant);
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_qualite_ancien_exploitant
    FOREIGN KEY (qualite_ancien_exploitant) REFERENCES qualite_exploitant(qualite_exploitant);
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_civilite_exploitant
    FOREIGN KEY (civilite_exploitant) REFERENCES titre_de_civilite(titre_de_civilite);
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_civilite_co_exploitant
    FOREIGN KEY (civilite_co_exploitant) REFERENCES titre_de_civilite(titre_de_civilite);
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_civilite_proprietaire
    FOREIGN KEY (civilite_proprietaire) REFERENCES titre_de_civilite(titre_de_civilite);
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_type_etablissement
    FOREIGN KEY (type_etablissement) REFERENCES type_etablissement(type_etablissement);
ALTER TABLE ONLY demande_licence
    ADD CONSTRAINT demande_licence_statut_demande
    FOREIGN KEY (statut_demande) REFERENCES statut_demande(statut_demande);
ALTER TABLE ONLY demande_licence
    ADD CONSTRAINT demande_licence_terme
    FOREIGN KEY (terme) REFERENCES terme(terme);
ALTER TABLE ONLY demande_licence
    ADD CONSTRAINT demande_licence_type_licence
    FOREIGN KEY (type_licence) REFERENCES type_licence(type_licence);
ALTER TABLE ONLY demande_licence
    ADD CONSTRAINT demande_licence_type_demande
    FOREIGN KEY (type_demande) REFERENCES type_demande(type_demande);

