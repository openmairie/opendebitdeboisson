-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v2.1.1 depuis la version v2.1.0
--
-- @package opendebitdeboisson
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
-- Ticket #9636 - BEGIN
--
ALTER TABLE etablissement ALTER COLUMN raison_sociale TYPE character varying(50);
ALTER TABLE etablissement ALTER COLUMN enseigne TYPE character varying(50);
--
-- Ticket #9636 - END
--

