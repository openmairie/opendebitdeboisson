<?php
/**
 * Ce script permet d'interfacer le widget de tableau de bord 'Dernières demandes'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../obj/opendebitdeboisson.class.php";
if (isset($f) !== true) {
    $f = new opendebitdeboisson("nohtml");
}

$inst_util__demande_licence = $f->get_inst__om_dbform(array(
    "obj" => "demande_licence",
));
$inst_util__demande_licence->view_widget_dernieres_demandes();
