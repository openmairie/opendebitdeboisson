<?php
/**
 * Ce script permet d'interfacer l'application.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

require_once "../obj/opendebitdeboisson.class.php";
$flag = filter_input(INPUT_GET, 'module');
if (in_array($flag, array("login", "logout", )) === false) {
    $flag = "nohtml";
}
$f = new opendebitdeboisson($flag);
$f->view_main();
