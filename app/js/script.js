/**
 * JS APP
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

/**
 * Gère le comportement de la page settings (administration et paramétrage) :
 *  - autofocus sur le champ de recherche
 *  - comportement du livesearch
 */
 function handle_settings_page() {
    // Auto focus sur le champ de recherche au chargement de la page
    $('#settings-live-search #filter').focus();
    // Live search dans le contenu de la page settings
    $("#settings-live-search #filter").keyup(function(){
        var filter = $(this).val(), count = 0;
        $("div.list-group a").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }
        });
    });
}

/**
 * Au chargement de la page
 */
$(function() {
    // Gère le comportement de la page settings (administration et paramétrage).
    handle_settings_page();
});

/**
 * Permet d'éventuellement lancer des scripts spécifiques à l'application.
 *
 * @param boolean tinymce_load permet de définir si les éditeurs tinyMCE doivent être chargés.
 */
function app_initialize_content(tinymce_load) {
    // Gestion de l'adresse postale
    handle_adresse_postale();
    treatment_exploitant_proprietaire_fields();
}

/**
 * @return void
 */
function show_hide_form_fields(fields, hide) {
    // Si le paramètre n'est pas renseigné
    if (typeof(hide) == 'undefined' ){
        hide = false;
    }

    // Affiche ou cache les champs
    fields.forEach(function(field) {
        if (hide === false) {
            $('#' + field).parent().parent().show();
        } else {
            $('#' + field).parent().parent().hide();
        }
    });
}

/**
 * @return void
 */
function activate_deactivate_form_fields(fields, readonly) {
    // Si le paramètre n'est pas renseigné
    if (typeof(readonly) == 'undefined' ){
        readonly = false;
    }
    // Active ou désactive les champs
    fields.forEach(function(field) {
        $('#' + field).prop('readonly', readonly);
    });
}

/**
 * Gère le traitement des champs d'adresse utilisant l'adresse postale au
 * chargement des pages.
 *
 * @return void
 */
function handle_adresse_postale() {
    if ($('#saisie_manuelle').is(':visible') === true) {
        treatment_adresse_postale_fields($('#autocomplete-adresse_postale-id')[0], $('#saisie_manuelle')[0])
    }
}

/**
 * @return void
 */
function copy_adresse_postale_fields(adresse_postale) {
    // Identifiant de l'adresse postale
    var adresse_postale_id = adresse_postale.value;
    // Lien vers l'action permettant de récupérer les valeurs de l'adresse
    // postale
    var url = '../app/index.php?module=form&obj=adresse_postale&action=11&idx=' + adresse_postale_id;
    // Remplissage des champs de l'adresse
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        async: false,
        success: function(adresse_postale_values) {
            $('#numero_voie').val(adresse_postale_values.numero);
            $('#complement').val(adresse_postale_values.complement);
            $('#voie').val(adresse_postale_values.rivoli);
            $('#libelle_voie').val(adresse_postale_values.libelle);
        }
    });
}

/**
 * @return void
 */
function treatment_adresse_postale_fields(adresse_postale, saisie_manuelle) {
    // Liste des champs composant l'adresse
    var fields = ['numero_voie', 'complement', 'voie', 'libelle_voie'];
    //
    if (saisie_manuelle.checked === true) {
        activate_deactivate_form_fields(fields);
        activate_deactivate_form_fields(['autocomplete-adresse_postale-search'], true);
    } else {
        activate_deactivate_form_fields(fields, true);
        activate_deactivate_form_fields(['autocomplete-adresse_postale-search']);
        copy_adresse_postale_fields(adresse_postale);
    }
}

/**
 * @return void
 */
function form_confirmation_adresse_postale_on_saisie_manuelle(adresse_postale, saisie_manuelle, title_form_confirmation, msg_form_confirmation) {
    // Si la case à cocher pour la saisie manuelle est décochée
    if (saisie_manuelle.checked !== true) {
        // Affiche une fenêtre modale pour confirmer l'action
        var dialogbloc = $("<div id=\"dialog-action-confirmation\" title=\"" + title_form_confirmation + "\">" + msg_form_confirmation + "</div>").insertAfter('#footer');
        //
        $(dialogbloc).dialog( "destroy" );
        $(dialogbloc).dialog({
            resizable: false,
            height:160,
            width:350,
            modal: true,
            buttons: [
                {
                    text: msg_form_action_confirmation_button_confirm,
                    id: 'ui-button-valid',
                        click: function() {
                            clear_autocomplete('autocomplete-adresse_postale');
                            treatment_adresse_postale_fields(adresse_postale, saisie_manuelle)
                            $(this).dialog("close");
                        }
                }, {
                    text: msg_form_action_confirmation_button_cancel,
                    id: 'ui-button-cancel',
                        click: function() {
                            saisie_manuelle.checked = true;
                            $(this).dialog("close");
                        }
                }
            ]
        });
    } else {
        // Si la case à cocher pour la saisie manuelle est cochée alors il n'y a
        // pas besoin de confirmation
        clear_autocomplete('autocomplete-adresse_postale');
        treatment_adresse_postale_fields(adresse_postale, saisie_manuelle)
    }
}

/**
 * Si la case est cochée alors copie les valeurs de l'exploitant dans les
 * champs du propriétaire et on verrouille les champs du propriétaire.
 *
 * @return void
 */
function treatment_exploitant_proprietaire_fields() {
    var fields = ["civilite", "nom", "prenom", "adresse1", "adresse2", "cp", "ville"];
    if ($('#idem_exploitant_proprietaire').prop('checked') === true) {
        for (var i = 0, len = fields.length; i < len; i++) {
          $('#'+fields[i]+'_proprietaire').val($('#'+fields[i]+'_exploitant')[0].value);
          $("#"+fields[i]+"_proprietaire").prop('readonly', true);
        }
    } else {
        for (var i = 0, len = fields.length; i < len; i++) {
          $("#"+fields[i]+"_proprietaire").prop('readonly', false);
        }
    }
}

/**
 * Si la case est décochée, on vide les champs du propriétaire.
 *
 * @retrun void
 */
function clean_exploitant_proprietaire_fields() {
    var fields = ["civilite", "nom", "prenom", "adresse1", "adresse2", "cp", "ville"];
    if ($('#idem_exploitant_proprietaire').prop('checked') !== true) {
        for (var i = 0, len = fields.length; i < len; i++) {
          $('#'+fields[i]+'_proprietaire').val('');
        }
    }
}

/**
 * Permet d'ajouter des années à une date récupérée d'un champ de formulaire et
 * de mettre à jour un champ de formulaire avec la nouvelle date.
 *
 * @return void
 * 
 */
function add_years_to_date(date_field_src, date_field_dest, years) {
    // Change le format de la date dd/mm/yyyy en yyyy-mm-dd
    var date = new Date(date_field_src.value.split('/').reverse().join('-'));
    // Ajoute un nombre d'année à la date
    var date_result = new Date(date.setFullYear(date.getFullYear() + years));
    // Met la valeur dans le champ de destiantion et change le format de la date
    // vers dd/mm/yyyy
    function pad(s) { return (s < 10) ? '0' + s : s; }
    $(date_field_dest).val([pad(date_result.getDate()), pad(date_result.getMonth()+1), date_result.getFullYear()].join('/'));
    fdate(date_field_dest);
}
