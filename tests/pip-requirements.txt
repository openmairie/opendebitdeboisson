openmairie.devtools==0.4.0
openmairie.robotframework==4.9.10008
maildump==0.5.6
selenium==2.53.6
robotframework==3.0.4
robotframework-seleniumlibrary==3.1.1
robotframework-selenium2screenshots==0.8.1
requests==2.22.0
robotframework-requests==0.5.0
Pillow==6.1.0
robotframework-archivelibrary==0.4.0
psycopg2==2.8.3
python-daemon==2.2.4
Unidecode==1.2.0

