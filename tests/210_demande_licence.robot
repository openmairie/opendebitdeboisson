*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    ${testid} =  Set Variable  TEST210
    Set Suite Variable  ${testid}

    ${select_list_terme_permanente} =  Create List  Permanente
    Set Suite Variable  ${select_list_terme_permanente}

    #
    &{etablissement_permanent_01} =  Create Dictionary
    ...  raison_sociale=${testid}ETABLISSEMENTPERMANENT01RS
    ...  enseigne=${testid}ETABLISSEMENTPERMANENT01ENSEIGNE
    ${etablissement_permanent_01_id} =  Ajouter l'établissement  ${etablissement_permanent_01}
    Set Suite Variable  ${etablissement_permanent_01}
    Set Suite Variable  ${etablissement_permanent_01_id}
    #
    &{demande_licence_01} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_01_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_01_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_01}
    Set Suite Variable  ${demande_licence_01}
    Set Suite Variable  ${demande_licence_01_id}
    #
    &{etablissement_permanent_02} =  Create Dictionary
    ...  raison_sociale=${testid}ETABLISSEMENTPERMANENT02RS
    ...  enseigne=${testid}ETABLISSEMENTPERMANENT02ENSEIGNE
    ...  particularite_exploitant=${testid}ETABLISSEMENTPERMANENT02PARTICULARITE
    ${etablissement_permanent_02_id} =  Ajouter l'établissement  ${etablissement_permanent_02}
    Set Suite Variable  ${etablissement_permanent_02}
    Set Suite Variable  ${etablissement_permanent_02_id}
    #
    &{etablissement_temporaire_01} =  Create Dictionary
    ...  raison_sociale=${testid}ETABLISSEMENTTEMPORAIRE01RS
    ...  enseigne=${testid}ETABLISSEMENTTEMPORAIRE01ENSEIGNE
    ${etablissement_temporaire_01_id} =  Ajouter l'établissement temporaire  ${etablissement_temporaire_01}
    Set Suite Variable  ${etablissement_temporaire_01}
    Set Suite Variable  ${etablissement_temporaire_01_id}
    #
    &{demande_licence_02} =  Create Dictionary
    ...  etablissement_id=${etablissement_temporaire_01_id}
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_02_id} =  Ajouter la demande de licence sur l'établissement temporaire  ${demande_licence_02}
    Set Suite Variable  ${demande_licence_02}
    Set Suite Variable  ${demande_licence_02_id}
    #
    &{etablissement_temporaire_02} =  Create Dictionary
    ...  raison_sociale=${testid}ETABLISSEMENTTEMPORAIRE02RS
    ...  enseigne=${testid}ETABLISSEMENTTEMPORAIRE02ENSEIGNE
    ...  particularite_exploitant=${testid}ETABLISSEMENTTEMPORAIRE02PARTICULARITE
    ${etablissement_temporaire_02_id} =  Ajouter l'établissement temporaire  ${etablissement_temporaire_02}
    Set Suite Variable  ${etablissement_temporaire_02}
    Set Suite Variable  ${etablissement_temporaire_02_id}


Formulaire d'une demande de licence (établissement permanent)
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin

    # Cas n°1
    Depuis le formulaire d'ajout d'une demande de licence sur l'établissement  ${etablissement_permanent_01_id}
    # Particularité de l'exploitant sur l'établissement pré-remplie à l'ajout
    # Pas de valeur sur l'établissement permanent 01 donc le champ doit être vide
    Form Value Should Be  css=#particularite  ${EMPTY}
    # Date de l'ancienne demande pré-remplie à l'ajout si une demande existe
    # Une seule demande sur l'établissement permanent 01 donc la date doit être pré-remplie avec sa date de début de validité
    Form Value Should Be  css=#date_ancienne_demande  ${demande_licence_01.date_debut_validite}
    #
    Select List Should Contain List  css=#terme  ${select_list_terme_permanente}

    # Cas n°2
    Depuis le formulaire d'ajout d'une demande de licence sur l'établissement  ${etablissement_permanent_02_id}
    # Particularité de l'exploitant sur l'établissement pré-remplie à l'ajout
    # Une valeur sur l'établissement permanent 02 donc le champ doit être pré-remplie avec cette valeur
    Form Value Should Be  css=#particularite  ${etablissement_permanent_02.particularite_exploitant}
    # Date de l'ancienne demande pré-remplie à l'ajout si une demande existe
    # Aucune autre demande sur l'établissement permanent 02 donc la date doit être vide
    Form Value Should Be  css=#date_ancienne_demande  ${EMPTY}
    #
    Select List Should Contain List  css=#terme  ${select_list_terme_permanente}

    # Vérification des actions
    Depuis le contexte de la demande de licence sur l'établissement  ${etablissement_permanent_01_id}  ${demande_licence_01_id}
    Portlet Action Should Be In SubForm  demande_licence  cerfa_declaration
    Portlet Action Should Be In SubForm  demande_licence  recepisse
    Portlet Action Should Be In SubForm  demande_licence  recepisse_co_exploitant
    Portlet Action Should Be In SubForm  demande_licence  demande_temp_reponse


Formulaire d'une demande de licence (établissement temporaire)
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin

    # Cas n°1
    Depuis le formulaire d'ajout d'une demande de licence sur l'établissement temporaire  ${etablissement_temporaire_01_id}
    # Particularité de l'exploitant sur l'établissement pré-remplie à l'ajout
    # Pas de valeur sur l'établissement temporaire 01 donc le champ doit être vide
    Form Value Should Be  css=#particularite  ${EMPTY}
    # La valeur du type de demande est fixé à D'OUVERTURE sur un établissement temporaire
    Element Should Contain  css=#sousform-demande_licence div.field-type-selecthiddenstatic  D'OUVERTURE
    #
    Select List Should Not Contain List  css=#terme  ${select_list_terme_permanente}

    # Cas n°2
    Depuis le formulaire d'ajout d'une demande de licence sur l'établissement temporaire  ${etablissement_temporaire_02_id}
    # Particularité de l'exploitant sur l'établissement pré-remplie à l'ajout
    # Une valeur sur l'établissement temporaire 02 donc le champ doit être pré-remplie avec cette valeur
    Form Value Should Be  css=#particularite  ${etablissement_temporaire_02.particularite_exploitant}
    # La valeur du type de demande est fixé à D'OUVERTURE sur un établissement temporaire
    Element Should Contain  css=#sousform-demande_licence div.field-type-selecthiddenstatic  D'OUVERTURE
    #
    Select List Should Not Contain List  css=#terme  ${select_list_terme_permanente}

    # Vérification des actions
    Depuis le contexte de la demande de licence sur l'établissement temporaire  ${etablissement_temporaire_01_id}  ${demande_licence_02_id}
    Portlet Action Should Be In SubForm  demande_licence  cerfa_declaration
    Portlet Action Should Be In SubForm  demande_licence  recepisse
    Portlet Action Should Not Be In SubForm  demande_licence  recepisse_co_exploitant
    Portlet Action Should Be In SubForm  demande_licence  demande_temp_reponse


Traitement de la demande de licence
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin

    &{etablissement_permanent_03} =  Create Dictionary
    ...  raison_sociale=${testid}ETPERM03RS
    ...  enseigne=${testid}ETPERM03ENSEIGNE
    ...  adresse_postale=68 - BIS - RUE ALEXIS LEPÈRE
    ${etablissement_permanent_03_id} =  Ajouter l'établissement  ${etablissement_permanent_03}

    &{demande_licence_03} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_03_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_03_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_03}

    #Verification du traitement du permis d'exploitation
    Modifier la demande de licence sur un établissement  ${demande_licence_03_id}  ${demande_licence_03}

    Error Message Should Contain In SubForm  L'établissement ne possède pas de permis d'exploitation.

    &{etablissement_permanent_03_modif} =  Create Dictionary
    ...  permis_exploitation=true
    Modifier l'établissement  ${etablissement_permanent_03_id}  ${etablissement_permanent_03_modif}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    Modifier la demande de licence sur un établissement  ${demande_licence_03_id}  ${demande_licence_03}
    Valid Message Should Contain In SubForm  Vos modifications ont bien été enregistrées.

    #Vérification du traitement des contraintes de proximité avec un périmètre d'exclusion
    &{perimetre_values} =  Create Dictionary
    ...  libelle=${testid}PERIMETRE02LIBDEMANDEL
    ...  adresse_postale=71 - BIS - RUE ALEXIS LEPÈRE
    ${perimetre_id} =  Ajouter le périmètre  ${perimetre_values}

    &{type_licence_values} =  Create Dictionary
    ...  contrainte_proximite=true
    Modifier le type de licence  ${demande_licence_03.type_licence}  ${type_licence_values}

    Modifier la demande de licence sur un établissement  ${demande_licence_03_id}  ${demande_licence_03}
    Error Message Should Contain In SubForm  La distance est non respectée avec le périmètre d'exclusion ${perimetre_values.libelle} avec 9,39 mètres.

    #Vérification du traitement des contraintes de proximité avec un autre établissement
    &{etablissement_permanent_04} =  Create Dictionary
    ...  raison_sociale=${testid}ETPERM04RS
    ...  enseigne=${testid}ETPERM04ENSEIGNE
    ...  adresse_postale=71 - BIS - RUE ALEXIS LEPÈRE
    ...  permis_exploitation=true
    ${etablissement_permanent_04_id} =  Ajouter l'établissement  ${etablissement_permanent_04}

    &{demande_licence_04} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_04_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_04_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_04}

    &{perimetre_values} =  Create Dictionary
    ...  libelle=${testid}PERIMETRE04LIBDEMANDEL
    ...  adresse_postale=71 - BIS - RUE ALEXIS LEPÈRE
    ${perimetre_id} =  Ajouter le périmètre  ${perimetre_values}

    Modifier la demande de licence sur un établissement  ${demande_licence_03_id}  ${demande_licence_03}
    Error Message Should Contain In SubForm  La distance est non respectée avec l'établissement possédant une contrainte de proximité ${etablissement_permanent_04.enseigne} avec 9,39 mètres.

    #Vérification du traitement du nombre de licence temporaire et liée à un terrain de sport dans une année.
    &{etablissement_permanent_05} =  Create Dictionary
    ...  raison_sociale=${testid}ETPERM05RS
    ...  enseigne=${testid}ETPERM05ENSEIGNE
    ...  permis_exploitation=true
    ${etablissement_permanent_05_id} =  Ajouter l'établissement  ${etablissement_permanent_05}

    &{demande_licence_05} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_05_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=01/01/2017
    ...  date_fin_validite=05/01/2017
    ${demande_licence_05_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_05}

    &{demande_licence_06} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_05_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=05/01/2017
    ...  date_fin_validite=10/01/2017
    ${demande_licence_06_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_06}

    &{demande_licence_07} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_05_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=15/01/2017
    ${demande_licence_07_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_07}

    &{demande_licence_08} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_05_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=15/01/2017
    ...  date_fin_validite=20/01/2017
    ${demande_licence_08_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_08}

    &{demande_licence_09} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_05_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=25/01/2017
    ...  date_fin_validite=30/01/2017
    ${demande_licence_09_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_09}

    &{demande_licence_10} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_05_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=01/02/2017
    ...  date_fin_validite=05/02/2017
    ${demande_licence_10_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_10}

    Modifier la demande de licence sur un établissement  ${demande_licence_10_id}  ${demande_licence_10}
    Error Message Should Contain  Le nombre maximum de demande de licence temporaire pour cet établissement est dépassé avec 6 demandes.

    &{demande_licence_11} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_05_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Liée à un terrain de sport
    ...  date_debut_validite=01/02/2017
    ...  date_fin_validite=05/02/2017
    ${demande_licence_11_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_11}

    Modifier la demande de licence sur un établissement  ${demande_licence_10_id}  ${demande_licence_11}
    Modifier la demande de licence sur un établissement  ${demande_licence_09_id}  ${demande_licence_11}
    Modifier la demande de licence sur un établissement  ${demande_licence_08_id}  ${demande_licence_11}
    Modifier la demande de licence sur un établissement  ${demande_licence_07_id}  ${demande_licence_11}
    Modifier la demande de licence sur un établissement  ${demande_licence_06_id}  ${demande_licence_11}
    Modifier la demande de licence sur un établissement  ${demande_licence_05_id}  ${demande_licence_11}
    ${demande_licence_12_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_11}
    ${demande_licence_13_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_11}
    ${demande_licence_14_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_11}
    ${demande_licence_15_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_11}
    Modifier la demande de licence sur un établissement  ${demande_licence_13_id}  ${demande_licence_11}
    Error Message Should Contain  Nombre maximum de demande liée à un terrain de sport pour cet établissement est dépassé avec 11 demandes

