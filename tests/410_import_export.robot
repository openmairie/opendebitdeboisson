*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...

*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]  ...
    ${testid} =  Set Variable  TEST410
    Set Suite Variable  ${testid}


Reqmo nombre de licence par type
    [Documentation]  ....
    Depuis la page d'accueil  admin  admin

    #
    &{type_licence_values} =  Create Dictionary
    ...  libelle=${testid}TYPELICENCELIB001
    ...  contrainte_proximite=true
    ${type_licence_id} =  Ajouter le type de licence  ${type_licence_values}
    #
    &{etablissement_permanent} =  Create Dictionary
    ...  raison_sociale=${testid}ETABPERM01RS
    ...  enseigne=${testid}ETABPERM01ENS
    ${etablissement_permanent_id} =  Ajouter l'établissement  ${etablissement_permanent}
    #
    &{demande_licence_01} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=${testid}TYPELICENCELIB001
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_01_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_01}
    #
    &{demande_licence_02} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=${testid}TYPELICENCELIB001
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_02_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_02}
    #
    &{demande_licence_03} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=${testid}TYPELICENCELIB001
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_03_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_03}
    #
    &{demande_licence_04} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=${testid}TYPELICENCELIB001
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_04_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_04}
    #
    &{demande_licence_05} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=${testid}TYPELICENCELIB001
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_05_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_05}

    Depuis l'écran principal du module 'Reqmo'
    Click On Link  statistiques_licences_par_type
    Click On Submit Button In Reqmo
    Element Should Contain  css=table.tab-tab  ${demande_licence_01.type_licence}
    Element Should Contain  css=table.tab-tab  5


Reqmo type de licence par établissement
    [Documentation]  ....
    Depuis la page d'accueil  admin  admin

    #
    &{type_licence_values} =  Create Dictionary
    ...  libelle=${testid}TYPELICENCELIB002
    ...  contrainte_proximite=true
    ${type_licence_id} =  Ajouter le type de licence  ${type_licence_values}
    #
    &{etablissement_permanent} =  Create Dictionary
    ...  raison_sociale=${testid}ETABPERM02RS
    ...  enseigne=${testid}ETABPERM02ENS
    ${etablissement_permanent_id} =  Ajouter l'établissement  ${etablissement_permanent}
    #
    &{demande_licence} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=${testid}TYPELICENCELIB002
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence}

    Depuis l'écran principal du module 'Reqmo'
    Click On Link  statistiques_licence_type_par_etablissement
    Click On Submit Button In Reqmo
    Element Should Contain  css=table.tab-tab  ${etablissement_permanent_id} ${etablissement_permanent.enseigne} ${etablissement_permanent.raison_sociale}
    Element Should Contain  css=table.tab-tab  ${demande_licence.type_licence}


Reqmo toutes les informations sur les demandes de licence et établissements liés
    [Documentation]  ....
    Depuis la page d'accueil  admin  admin

    #
    &{type_licence_values} =  Create Dictionary
    ...  libelle=${testid}TYPELICENCELIB003
    ...  contrainte_proximite=true
    ${type_licence_id} =  Ajouter le type de licence  ${type_licence_values}
    #
    &{etablissement_permanent} =  Create Dictionary
    ...  raison_sociale=${testid}ETABPERM03RS
    ...  enseigne=${testid}ETABPERM03ENS
    ...  no_siret=${testid}ETPE03S
    ...  observation=${testid}ETABPERM03OBS
    ...  ancien_proprietaire=${testid}ETABPERM03ANCPROP
    ${etablissement_permanent_id} =  Ajouter l'établissement  ${etablissement_permanent}
    #
    &{demande_licence} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=${testid}TYPELICENCELIB003
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence}

    Depuis l'écran principal du module 'Reqmo'
    Click On Link  statistiques_demande_licence_etablissement
    Click On Submit Button In Reqmo
    # Vérifie quelques informations concernant les demandes de licence et les
    # établissements
    Element Should Contain  css=table.tab-tab  ${demande_licence.type_licence}
    Element Should Contain  css=table.tab-tab  ${demande_licence.type_demande}
    Element Should Contain  css=table.tab-tab  ${etablissement_permanent_id}
    Element Should Contain  css=table.tab-tab  ${etablissement_permanent.raison_sociale}
    Element Should Contain  css=table.tab-tab  ${etablissement_permanent.enseigne}
    Element Should Contain  css=table.tab-tab  ${etablissement_permanent.no_siret}
    Element Should Contain  css=table.tab-tab  ${etablissement_permanent.observation}
    Element Should Contain  css=table.tab-tab  ${etablissement_permanent.ancien_proprietaire}


Reqmo nombre de licence par type filtré par la date de fin de validité et sans les demandes de transfert
    [Documentation]  ....
    Depuis la page d'accueil  admin  admin

    #
    &{type_licence_values} =  Create Dictionary
    ...  libelle=${testid}TYPELICENCELIB004
    ...  contrainte_proximite=true
    ${type_licence_id} =  Ajouter le type de licence  ${type_licence_values}
    #
    &{etablissement_permanent} =  Create Dictionary
    ...  raison_sociale=${testid}ETABPERM04RS
    ...  enseigne=${testid}ETABPERM04ENS
    ${etablissement_permanent_id} =  Ajouter l'établissement  ${etablissement_permanent}
    #
    &{demande_licence_01} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=${testid}TYPELICENCELIB001
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_01_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_01}
    #
    &{demande_licence_02} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=DE TRANSFERT
    ...  type_licence=${testid}TYPELICENCELIB001
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2030
    ${demande_licence_02_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_02}
    #
    &{demande_licence_03} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=${testid}TYPELICENCELIB001
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2030
    ${demande_licence_03_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_03}

    Depuis l'écran principal du module 'Reqmo'
    Click On Link  statistiques_licences_par_type
    Click On Submit Button In Reqmo
    Element Should Contain  css=table.tab-tab  ${demande_licence_03.type_licence}
    Element Should Contain  css=table.tab-tab  1


Import des établissements
    [Documentation]  ....
    Depuis la page d'accueil    admin    admin
    Depuis l'import    etablissement
    Add File    fic1    test410-import-etablissement.csv
    Click On Submit Button In Import CSV
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    3 ligne(s) importée(s)

    Depuis le listing des établissements
    Input Text  css=div#adv-search-classic-fields input.champFormulaire  ${testid}IMPORT
    Click On Search Button
    Element Should Contain  css=table.tab-tab  ${testid}IMPORTETAB001RS
    Element Should Contain  css=table.tab-tab  ${testid}IMPORTETAB003RS

    Depuis le listing des établissements temporaires
    Input Text  css=div#adv-search-classic-fields input.champFormulaire  ${testid}IMPORT
    Element Should Contain  css=table.tab-tab  ${testid}IMPORTETAB002RS


Import des demandes de licence
    [Documentation]  ...
    Depuis la page d'accueil    admin    admin
    Depuis l'import    demande_licence
    Add File    fic1    test410-import-demande_licence.csv
    Click On Submit Button In Import CSV
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    3 ligne(s) importée(s)

    Depuis l'onglet 'Demandes de licence' de l'établissement  1
    Element Should Contain  css=table.tab-tab  20/04/1991
    Element Should Contain  css=table.tab-tab  07/07/1990
    Element Should Contain  css=table.tab-tab  15/08/2016
