<?php
/**
 * Ce script contient la définition de la classe 'GeneralTest'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id: testGeneral.php 1058 2020-03-25 15:31:10Z fmichon $
 */

require_once "testGeneral_common.php";
final class GeneralTest extends GeneralCommon {
    public function setUp(): void {
        $this->common_setUp();
    }
    public function tearDown(): void {
        $this->common_tearDown();
    }
    public function onNotSuccessfulTest(Throwable $e): void {
        $this->common_onNotSuccessfulTest($e);
    }
}
