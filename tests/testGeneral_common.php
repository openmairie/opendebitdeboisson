<?php
/**
 * Ce script contient la définition de la classe 'GeneralCommon'.
 *
 * @package opendebitdeboisson
 * @version SVN : $Id$
 */

/**
 * Cette classe permet de tester unitairement les fonctions de l'application.
 */
abstract class GeneralCommon extends PHPUnit\Framework\TestCase {

    /**
     * Méthode lancée en début de traitement
     */
    function common_setUp() {
        date_default_timezone_set('Europe/Paris');
        echo ' = '.get_called_class().'.'.str_replace('test_', '', $this->getName())."\r\n";
    }

    /**
     * Méthode lancée en fin de traitement
     */
    function common_tearDown() {
    }

    /**
     * Méthode étant appelée lors du fail d'un test.
     *
     * @param $e Exception remontée lors du test
     * @return void
     */
    public function common_onNotSuccessfulTest(Throwable $e) {
        echo 'Line '.$e->getLine().' : '.$e->getMessage()."\r\n";
        parent::onNotSuccessfulTest($e);
    }

    /**
     * Test template
     */
    public function test_template() {
        // Instanciation de la classe *om_application*
        require_once "../obj/opendebitdeboisson.class.php";
        $_POST["login"] = "admin";
        $_POST["password"] = "admin";
        $_POST["login_action_connect"] = true;
        $f = new opendebitdeboisson("login_and_nohtml");
        $f->disableLog();
        // Test
        $this->assertEquals($f->authenticated, true);
        // Destruction de la classe *om_application*
        $f->logout();
        $f->__destruct();
    }
}
