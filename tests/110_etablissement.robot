*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    ${testid} =  Set Variable  TEST110
    Set Suite Variable  ${testid}


L'établissement (permanent)
    [Documentation]  L'établissement (permanent) est un établissement de nature
    ...  'permanent'.
    Depuis la page d'accueil  admin  admin

    # L'établissement permanent n'apparait pas dans le listing des établissements temporaires
    &{etablissement_permanent_01} =  Create Dictionary
    ...  raison_sociale=${testid}ETABLISSEMENTPERMANENT01RS
    ...  enseigne=${testid}ETABLISSEMENTPERMANENT01ENSEIGNE
    ${etablissement_permanent_01_id} =  Ajouter l'établissement  ${etablissement_permanent_01}
    Depuis le listing des établissements temporaires
    Input Text  css=#adv-search-classic-fields input[name="recherche"]  ${etablissement_permanent_01.raison_sociale}
    Click Element  css=#adv-search-submit
    ${pagination_result} =  Get Pagination Text
    Should Be Equal  ${pagination_result}  1 - 0 enregistrement(s) sur 0 = [${etablissement_permanent_01.raison_sociale.upper()}]

    # L'établissement contient tous les blocs (contrairement à l'établissement temporaire)
    Depuis le formulaire d'ajout d'un établissement
    Element Should Be Visible  css=.etablissement-bloc-permis-d-exploitation
    Element Should Be Visible  css=.etablissement-bloc-adresse
    Element Should Be Visible  css=.etablissement-bloc-exploitant
    Element Should Be Visible  css=.etablissement-bloc-co-exploitant
    Element Should Be Visible  css=.etablissement-bloc-ancien-exploitant
    Element Should Be Visible  css=.etablissement-bloc-ancien-proprietaire
    Element Should Be Visible  css=.etablissement-bloc-observation
    Element Should Be Visible  css=.etablissement-bloc-proprietaire
    Element Should Be Visible  css=.etablissement-bloc-inactivite

    # Vérification de l'utilisation de la copie des informations de l'exploitant
    Depuis le formulaire d'ajout d'un établissement
    Element Should Be Visible  id=idem_exploitant_proprietaire
    &{etablissement_permanent_02} =  Create Dictionary
    ...  raison_sociale=${testid}ETSTPERMANENT02RS
    ...  enseigne=${testid}ETSPERMANENT02ENSEIGNE
    ...  civilite_exploitant=Madame
    ...  nom_exploitant=Dodier
    ...  nom_marital_exploitant=Dodier
    ...  prenom_exploitant=Phillipa
    ...  date_naissance_exploitant=02/11/1987
    ...  ville_naissance_exploitant=ROANNE
    ...  cp_naissance_exploitant=42300
    ...  adresse1_exploitant=97 rue Gustave Eiffel
    ...  adresse2_exploitant=
    ...  cp_exploitant=42300
    ...  ville_exploitant=ROANNE
    Saisir les informations dans le formulaire de l'établissement  ${etablissement_permanent_02}
    Set Checkbox  id=idem_exploitant_proprietaire  true
    Selected List Label Should Be  id=civilite_proprietaire  ${etablissement_permanent_02.civilite_exploitant}
    Form Value Should Be  id=nom_proprietaire  ${etablissement_permanent_02.nom_exploitant}
    Form Value Should Be  id=prenom_proprietaire  ${etablissement_permanent_02.prenom_exploitant}
    Form Value Should Be  id=adresse1_proprietaire  ${etablissement_permanent_02.adresse1_exploitant}
    Form Value Should Be  id=adresse2_proprietaire  ${etablissement_permanent_02.adresse2_exploitant}
    Form Value Should Be  id=cp_proprietaire  ${etablissement_permanent_02.cp_exploitant}
    Form Value Should Be  id=ville_proprietaire  ${etablissement_permanent_02.ville_exploitant}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    # Vérification des informations dans le listing
    Depuis le listing des établissements
    Element Should Contain  css=table.tab-tab  Madame Phillipa Dodier


Gestion de l'adresse postale sur un établissement permanent
    [Documentation]  Vérification de l'utilisation de l'adresse postale sur un
    ...  établissement permanent.
    Depuis la page d'accueil  admin  admin

    &{adresse_postale_values} =  Create Dictionary
    ...  numero=18
    ...  complement=BIS
    ...  libelle=RUE DE LA RÉPUBLIQUE
    ${adresse_postale_id} =  Ajouter l'adresse postale  ${adresse_postale_values}

    &{etablissement_permanent_values} =  Create Dictionary
    ...  raison_sociale=${testid}ETABLISSEMENTPERMANENT02RS
    ...  enseigne=${testid}ETABLISSEMENTPERMANENT02ENSEIGNE
    ...  adresse_postale=18 - BIS - RUE DE LA RÉPUBLIQUE
    Depuis le formulaire d'ajout d'un établissement
    Saisir les informations dans le formulaire de l'établissement  ${etablissement_permanent_values}

    # Vérifie que les champs de l'adresse sont renseignés grâce à l'adresse
    # postale et qu'ils ne sont pas modifiable
    Form Value Should Be  numero_voie  18
    Form Value Should Be  complement  BIS
    Form Value Should Be  libelle_voie  RUE DE LA RÉPUBLIQUE
    Element Should Be Disabled  numero_voie
    Element Should Be Disabled  complement
    Element Should Be Disabled  libelle_voie

    # Vérifie les champs en saisie manuelle
    Set Checkbox  saisie_manuelle  true
    Form Value Should Be  autocomplete-adresse_postale-id  ${EMPTY}
    Element Should Be Disabled  autocomplete-adresse_postale-search
    Element Should Be Enabled  numero_voie
    Element Should Be Enabled  complement
    Element Should Be Enabled  libelle_voie

    # Vérifie le passage de la saisie manuelle vers la saisie automatique
    Set Checkbox  saisie_manuelle  false
    Wait Until Element Is Visible  css=div.ui-dialog
    Element Should Contain  css=div.ui-dialog  Cette action videra les champs de l'adresse de l'établissement.
    Click Element  id=ui-button-valid
    Wait Until Element Is Not Visible  css=div.ui-dialog
    Form Value Should Be  autocomplete-adresse_postale-id  ${EMPTY}
    Form Value Should Be  numero_voie  ${EMPTY}
    Form Value Should Be  complement  ${EMPTY}
    Form Value Should Be  libelle_voie  ${EMPTY}
    Element Should Be Enabled  autocomplete-adresse_postale-search
    Element Should Be Disabled  numero_voie
    Element Should Be Disabled  complement
    Element Should Be Disabled  libelle_voie

    # Valide le formulaire pour vérifier les champs en consultation
    ${etablissement_permanent_id} =  Ajouter l'établissement  ${etablissement_permanent_values}
    Form Static Value Should Be  numero_voie  18
    Form Static Value Should Be  complement  BIS
    Form Static Value Should Be  libelle_voie  RUE DE LA RÉPUBLIQUE

    #Verification de geom
    &{etablissement_permanent_values_geom} =  Create Dictionary
    ...  raison_sociale=${testid}ETABLISSEMENT03RS
    ...  enseigne=ETABLISSEMENTPERMANENT03ENSEIGNE
    ...  adresse_postale=45 - B - RUE DE LA RÉVOLUTION
    ${etablissement_permanent_id_geom} =  Ajouter l'établissement  ${etablissement_permanent_values_geom}
    ${geom} =  Get Value  css=#geom
    Should Not Be Equal  ${geom}  ${EMPTY}
    &{etablissement_permanent_values_geom2} =  Create Dictionary
    ...  raison_sociale=${testid}ETABLISSEMENT03RS
    ...  enseigne=ETABLISSEMENTPERMANENT03ENSEIGNE
    ...  adresse_postale=45 - B - RUE ALEXIS LEPÈRE
    Modifier l'établissement  ${etablissement_permanent_id_geom}  ${etablissement_permanent_values_geom2}
    ${geom2} =  Get Value  css=#geom
    Should Not Be Equal  ${geom2}  ${geom}


Les spécificités d’un établissement temporaire
    [Documentation]  Il existe un sous-type d’établissement (appelé
    ...  établissement temporaire) qui est une variante plus simplifiée d’un
    ...  établissement, non-géolocalisable, ne pouvant demander que des
    ...  demandes de licences temporaire ou liée à un terrain de sport.
    Depuis la page d'accueil  admin  admin

    # L'établissement temporaire n'apparait pas dans le listing des établissements permanents
    &{etablissement_temporaire_01} =  Create Dictionary
    ...  raison_sociale=${testid}ETABLISSEMENTTEMPORAIRE01RS
    ...  enseigne=${testid}ETABLISSEMENTTEMPORAIRE01ENSEIGNE
    ...  civilite_exploitant=Monsieur
    ...  nom_exploitant=Simon
    ...  prenom_exploitant=Fabien
    ${etablissement_temporaire_01_id} =  Ajouter l'établissement temporaire  ${etablissement_temporaire_01}
    Depuis le listing des établissements
    Input Text  css=#adv-search-classic-fields input[name="recherche"]  ${etablissement_temporaire_01.raison_sociale}
    Click Element  css=#adv-search-submit
    ${pagination_result} =  Get Pagination Text
    Should Be Equal  ${pagination_result}  1 - 0 enregistrement(s) sur 0 = [${etablissement_temporaire_01.raison_sociale.upper()}]

    # L'établissement temporaire ne contient pas les blocs de champs cachés
    Depuis le formulaire d'ajout d'un établissement temporaire
    Element Should Not Be Visible  css=.etablissement-bloc-permis-d-exploitation
    Element Should Be Visible  css=.etablissement-bloc-adresse
    Element Should Be Visible  css=.etablissement-bloc-exploitant
    Element Should Be Visible  css=.etablissement-bloc-observation
    Element Should Not Be Visible  css=.etablissement-bloc-co-exploitant
    Element Should Not Be Visible  css=.etablissement-bloc-ancien-exploitant
    Element Should Not Be Visible  css=.etablissement-bloc-ancien-proprietaire
    Element Should Not Be Visible  css=.etablissement-bloc-proprietaire
    Element Should Not Be Visible  css=.etablissement-bloc-inactivite

    # Vérification des informations dans le listing
    Depuis le listing des établissements temporaires
    Element Should Contain  css=table.tab-tab  Monsieur Fabien Simon


Vue synthétique de l'établissement
    [Documentation]

    #Vérification des lien d'action dans la vue synthétique
    Depuis la page d'accueil  admin  admin
    &{etablissement_permanent_03} =  Create Dictionary
    ...  raison_sociale=${testid}ETABVUESYNTHRS
    ...  enseigne=${testid}ETABVUESYNTHENS
    ${etablissement_permanent_03_id} =  Ajouter l'établissement  ${etablissement_permanent_03}

    &{demande_licence_01} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_03_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Permanente
    ...  date_debut_validite=28/09/2017
    ...  date_fin_validite=30/09/2018
    ${demande_licence_01_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_01}

    &{dossier_01} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_03_id}
    ...  date_piece=21/09/2018
    ...  fichier_upload=image_2.jpg
    ...  observation=Image test
    ${dossier_01_id} =  Ajouter le dossier depuis l'établissement  ${dossier_01}

    Ajouter la requête  courrier_test  courrier_test  null  objet  null  null  courrier;etablissement  null

    &{lettre_type_01} =  Create Dictionary
    ...  id=110
    ...  libelle=LETTRETYPEVUESYNTHETIQUE
    ...  requete=courrier_test
    Ajouter la lettre-type depuis le menu  ${lettre_type_01.id}  ${lettre_type_01.libelle}  <p>[etablissement.enseigne]</p>  ...  ${lettre_type_01.requete}  true

    &{courrier_01} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_03_id}
    ...  date_courrier=21/09/2018
    ...  modele_lettre_type=${lettre_type_01.id} ${lettre_type_01.libelle}
    ...  objet=objet test
    ${courrier_01_id} =  Ajouter le courrier  ${courrier_01}

    # Vérification de chaque lien vers un document PDF sur la vue synthétique
    # Vérification également de la présence du bouton permettant de visualiser
    # les documents uploadés dans dossier
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=etablissement_permanent&action=12&idx=${etablissement_permanent_03_id}
    Click On Link  edition_recepisse_${demande_licence_01_id}
    Open PDF  ${OM_PDF_TITLE}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Récépissé
    Close PDF
    Click On Link  edition_temp_${demande_licence_01_id}
    Open PDF  ${OM_PDF_TITLE}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Demande de licence temporaire avec réponse du Maire
    Close PDF
    Click On Link  edition_courrier_${courrier_01_id}
    Open PDF  ${OM_PDF_TITLE}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${etablissement_permanent_03.enseigne}
    Close PDF
    Element Should Contain  voir_piece_${dossier_01_id}  voir la pièce
