*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...

*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    ${testid} =  Set Variable  TEST310
    Set Suite Variable  ${testid}


Gestion de l'adresse postale sur un périmètre d'exclusion
    [Documentation]  Vérification de l'utilisation de l'adresse postale sur un
    ...  établissement permanent.
    Depuis la page d'accueil  admin  admin

    &{adresse_postale_values} =  Create Dictionary
    ...  numero=350
    ...  complement=A
    ...  libelle=RUE DE LA ROYAUTÉ
    ${adresse_postale_id} =  Ajouter l'adresse postale  ${adresse_postale_values}

    &{perimetre_values} =  Create Dictionary
    ...  libelle=${testid}PERIMETRE01LIB
    ...  adresse_postale=350 - A - RUE DE LA ROYAUTÉ
    Depuis le formulaire d'ajout d'un périmètre
    Saisir le périmètre  ${perimetre_values}

    # Vérifie que les champs de l'adresse sont renseignés grâce à l'adresse
    # postale et qu'ils ne sont pas modifiable
    Form Value Should Be  numero_voie  350
    Form Value Should Be  complement  A
    Form Value Should Be  libelle_voie  RUE DE LA ROYAUTÉ
    Element Should Be Disabled  numero_voie
    Element Should Be Disabled  complement
    Element Should Be Disabled  libelle_voie

    # Vérifie les champs en saisie manuelle
    Set Checkbox  saisie_manuelle  true
    Form Value Should Be  autocomplete-adresse_postale-id  ${EMPTY}
    Element Should Be Disabled  autocomplete-adresse_postale-search
    Element Should Be Enabled  numero_voie
    Element Should Be Enabled  complement
    Element Should Be Enabled  libelle_voie

    # Vérifie le passage de la saisie manuelle vers la saisie automatique
    Set Checkbox  saisie_manuelle  false
    Wait Until Element Is Visible  css=div.ui-dialog
    Element Should Contain  css=div.ui-dialog  Cette action videra les champs de l'adresse du périmètre d'exclusion.
    Click Element  id=ui-button-valid
    Wait Until Element Is Not Visible  css=div.ui-dialog
    Form Value Should Be  autocomplete-adresse_postale-id  ${EMPTY}
    Form Value Should Be  numero_voie  ${EMPTY}
    Form Value Should Be  complement  ${EMPTY}
    Form Value Should Be  libelle_voie  ${EMPTY}
    Element Should Be Enabled  autocomplete-adresse_postale-search
    Element Should Be Disabled  numero_voie
    Element Should Be Disabled  complement
    Element Should Be Disabled  libelle_voie

    # Valide le formulaire pour vérifier les champs en consultation
    ${perimetre_id} =  Ajouter le périmètre  ${perimetre_values}
    Form Static Value Should Be  numero_voie  350
    Form Static Value Should Be  complement  A
    Form Static Value Should Be  libelle_voie  RUE DE LA ROYAUTÉ

    ##Verification de geom
    &{perimetre_values_geom} =  Create Dictionary
    ...  libelle=${testid}PERIMETRE02LIB
    ...  adresse_postale=45 - B - RUE DE LA RÉVOLUTION
    ${perimetre_id} =  Ajouter le périmètre  ${perimetre_values_geom}
    ${geom} =  Get Value  css=#geom
    Should Not Be Equal  ${geom}  ${EMPTY}
    &{perimetre_values_geom2} =  Create Dictionary
    ...  libelle=${testid}PERIMETRE02LIB
    ...  adresse_postale=45 - B - RUE ALEXIS LEPÈRE
    Modifier le périmètre  ${perimetre_id}  ${perimetre_values_geom2}
    ${geom2} =  Get Value  css=#geom
    Should Not Be Equal  ${geom2}  ${geom}
