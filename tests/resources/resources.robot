*** Settings ***
Documentation     Ressources (librairies, keywords et variables)

# Mots-clefs framework
Library  openmairie.robotframework.Library

# Mots-clefs métier
Resource  app${/}utils.robot
Resource  app${/}adresse_postale.robot
Resource  app${/}common.robot
Resource  app${/}etablissement.robot
Resource  app${/}perimetre.robot
# Dépendance :
# On inclut la ressource 'demande_licence' après la ressource 'etablissement'
Resource  app${/}demande_licence.robot
Resource  app${/}dossier.robot
Resource  app${/}courrier.robot
Resource  app${/}type_licence.robot

*** Variables ***
${ADMIN_PASSWORD}  admin
${ADMIN_USER}      admin
${BROWSER}         firefox
${DELAY}           0
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}
${PROJECT_NAME}    opendebitdeboisson
${PROJECT_URL}     http://${SERVER}/${PROJECT_NAME}/
${SERVER}          localhost
${TITLE}           :: openMairie :: openDébitDeBoisson
${SESSION_COOKIE}  openelec

*** Keywords ***
For Suite Setup
    Reload Library  openmairie.robotframework.Library
    # Les keywords définit dans le resources.robot sont prioritaires
    Set Library Search Order  resources
    Ouvrir le navigateur
    Tests Setup

For Suite Teardown
    Fermer le navigateur
