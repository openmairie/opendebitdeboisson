*** Settings ***
Documentation  Mots-clés rattachés aux types de licence.

*** Keywords ***
Depuis le contexte du type de licence
    [Documentation]  ...
    [Tags]  type de licence
    [Arguments]  ${type_licence}
    Depuis le listing  type_licence
    Use Simple Search  libellé  ${type_licence}
    Click On Link  ${type_licence}

Depuis le formulaire d'ajout du type de licence
    [Documentation]  ...
    [Tags]  type de licence
    Depuis le listing  type_licence
    Click On Add Button
    Page Title Should Contain  Administration & Paramétrage > Tables De Référence > Types De Licences
    La page ne doit pas contenir d'erreur

Ajouter le type de licence
    [Documentation]
    [Tags]  type de licence
    [Arguments]  ${values}
    Depuis le formulaire d'ajout du type de licence
    Saisir le type de licence  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${type_licence_id} =  Get Text  css=div.form-content span#type_licence
    [Return]  ${type_licence_id}

Modifier le type de licence
    [Documentation]  ...
    [Tags]  type de licence
    [Arguments]  ${type_licence}  ${values}
    Depuis le contexte du type de licence  ${type_licence}
    Click On Form Portlet Action  type_licence  modifier
    Saisir le type de licence  ${values}
    Click On Submit Button

Saisir le type de licence
    [Documentation]  ...
    [Tags]  type de licence
    [Arguments]  ${values}
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "contrainte_proximite" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
