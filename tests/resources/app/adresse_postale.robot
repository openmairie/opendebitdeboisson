*** Settings ***
Documentation  Mots-clefs rattachés à l'adresse postale.

*** Keywords ***
Depuis le contexte de l'adresse postale
    [Documentation]  Accède au formulaire
    [Arguments]  ${adresse_postale}

    # On accède au tableau
    Depuis le listing  adresse_postale
    # On recherche l'enregistrement
    Use Simple Search  adresse_postale  ${adresse_postale}
    # On clique sur le résultat
    Click On Link  ${adresse_postale}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter l'adresse postale
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing  adresse_postale
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir l'adresse postale  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${adresse_postale} =  Get Text  css=div.form-content span#adresse_postale
    # On le retourne
    [Return]  ${adresse_postale}

Modifier l'adresse postale
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${adresse_postale}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte de l'adresse postale  ${adresse_postale}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  adresse_postale  modifier
    # On saisit des valeurs
    Saisir l'adresse postale  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer l'adresse postale
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${adresse_postale}

    # On accède à l'enregistrement
    Depuis le contexte de l'adresse postale  ${adresse_postale}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  adresse_postale  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir l'adresse postale
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "rivoli" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire
