*** Settings ***
Documentation  Mots-clés rattachés aux établissements.

*** Keywords ***
Depuis le listing des établissements
    [Documentation]  ...
    [Tags]  établissement
    Depuis le listing  etablissement_permanent
    Page Title Should Be  Application > Établissements

Depuis le contexte de l'établissement
    [Documentation]  ...
    [Arguments]  ${etablissement_id}
    [Tags]  établissement
    Depuis le listing des établissements
    Input Text  recherche  ${etablissement_id}
    Click On Search Button
    Click On Link  ${etablissement_id}
    Page Title Should Contain  Application > Établissements > ${etablissement_id}
    La page ne doit pas contenir d'erreur

Depuis le formulaire d'ajout d'un établissement
    [Documentation]  ...
    [Tags]  établissement
    Depuis le listing des établissements
    Click On Add Button
    Page Title Should Contain  Application > Établissements
    La page ne doit pas contenir d'erreur

Depuis le listing des établissements temporaires
    [Documentation]  ...
    [Tags]  établissement
    Depuis le listing  etablissement_temporaire
    Page Title Should Be  Application > Établissements Temporaires

Depuis le contexte de l'établissement temporaire
    [Documentation]  ...
    [Arguments]  ${etablissement_id}
    [Tags]  établissement
    Depuis le listing des établissements temporaires
    Input Text  recherche  ${etablissement_id}
    Click On Search Button
    Click On Link  ${etablissement_id}
    Page Title Should Contain  Application > Établissements Temporaires > ${etablissement_id}
    La page ne doit pas contenir d'erreur

Depuis le formulaire d'ajout d'un établissement temporaire
    [Documentation]  ...
    [Tags]  établissement
    Depuis le listing des établissements temporaires
    Click On Add Button
    Page Title Should Contain  Application > Établissements Temporaires
    La page ne doit pas contenir d'erreur

Ajouter l'établissement
    [Documentation]  ...
    [Arguments]  ${values}
    [Tags]  établissement
    Depuis le formulaire d'ajout d'un établissement
    Saisir les informations dans le formulaire de l'établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de confirmation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_id} =  Get Text  css=div.form-content span#etablissement
    [Return]  ${etablissement_id}

Modifier l'établissement
    [Documentation]  ...
    [Arguments]  ${etablissement_id}  ${values}
    [Tags]  établissement
    Depuis le contexte de l'établissement  ${etablissement_id}
    Click On Form Portlet Action  etablissement_permanent  modifier
    Si "adresse_postale" existe dans "${values}" on execute "Désactiver la saisie manuelle de l'adresse de l'établissement" sans argument
    Saisir les informations dans le formulaire de l'établissement  ${values}
    Click On Submit Button

Désactiver la saisie manuelle de l'adresse de l'établissement
    [Documentation]  ...
    [Tags]  établissement
    Set Checkbox  saisie_manuelle  false
    Wait Until Element Is Visible  css=div.ui-dialog
    Element Should Contain  css=div.ui-dialog  Cette action videra les champs de l'adresse de l'établissement.
    Click Element  id=ui-button-valid
    Wait Until Element Is Not Visible  css=div.ui-dialog

Ajouter l'établissement temporaire
    [Documentation]  ...
    [Arguments]  ${values}
    [Tags]  établissement
    Depuis le formulaire d'ajout d'un établissement temporaire
    Saisir les informations dans le formulaire de l'établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de confirmation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_id} =  Get Text  css=div.form-content span#etablissement
    [Return]  ${etablissement_id}

Depuis l'onglet 'Demandes de licence' de l'établissement
    [Documentation]  ...
    [Arguments]  ${etablissement_id}
    [Tags]  établissement
    Depuis le contexte de l'établissement  ${etablissement_id}
    On clique sur l'onglet  demande_licence  demande de licence

Depuis l'onglet 'Demandes de licence' de l'établissement temporaire
    [Documentation]  ...
    [Arguments]  ${etablissement_id}
    [Tags]  établissement
    Depuis le contexte de l'établissement temporaire  ${etablissement_id}
    On clique sur l'onglet  demande_licence  demande de licence

Depuis l'onglet 'Dossier' de l'établissement
    [Documentation]  ...
    [Arguments]  ${etablissement_id}
    [Tags]  établissement
    Depuis le contexte de l'établissement  ${etablissement_id}
    On clique sur l'onglet  dossier  dossier

Depuis l'onglet 'Courrier' de l'établissement
    [Documentation]  ...
    [Arguments]  ${etablissement_id}
    [Tags]  établissement
    Depuis le contexte de l'établissement  ${etablissement_id}
    On clique sur l'onglet  courrier  courrier

Saisir les informations dans le formulaire de l'établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    [Tags]  établissement
    Si "raison_sociale" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "enseigne" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "no_siret" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "forme_juridique" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "type_etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "permis_exploitation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "date_permis_exploitation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "permis_exploitation_nuit" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "date_permis_exploitation_nuit" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "numero_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_etablissement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_etablissement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "civilite_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom_marital_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_naissance_exploitant" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "ville_naissance_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_naissance_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "qualite_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nationalite_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "particularite_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "civilite_co_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom_marital_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_naissance_co_exploitant" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "ville_naissance_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_naissance_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nationalite_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "qualite_co_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "particularite_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom_ancien_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom_ancien_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "qualite_ancien_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "civilite_proprietaire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "profession_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_fermeture" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_liquidation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "adresse_postale" existe dans "${values}" on sélectionne la valeur sur l'autocomplete "adresse_postale" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ancien_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
