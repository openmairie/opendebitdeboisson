*** Settings ***
Documentation  Mots-clés rattachés aux dossiers.

*** Keywords ***

Depuis le contexte du dossier dans l'établissement
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier}

    # On accède au tableau
    Depuis l'onglet 'Dossier' de l'établissement  ${values.etablissement_id}
    # On recherche l'enregistrement
    Use Simple Search  dossier  ${dossier}
    # On clique sur le résultat
    Click On Link  ${dossier}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter le dossier depuis l'établissement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    # On accède au tableau
    Depuis l'onglet 'Dossier' de l'établissement  ${values.etablissement_id}
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir le dossier depuis l'établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier} =  Get Text  css=div.form-content span#dossier
    # On le retourne
    [Return]  ${dossier}

Modifier le dossier depuis l'établissement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte du dossier dans l'établissement  ${dossier}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier  modifier
    # On saisit des valeurs
    Saisir le dossier depuis l'établissement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer le dossier depuis l'établissement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier}

    # On accède à l'enregistrement
    Depuis le contexte du dossier dans l'établissement  ${dossier}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir le dossier depuis l'établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "date_piece" existe dans "${values}" on execute "Input Text" dans "dossier"
    Si "fichier_upload" existe dans "${values}" on execute "Add File" sur "fichier"
    Si "observation" existe dans "${values}" on execute "Input Text" dans "dossier"
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans "dossier"