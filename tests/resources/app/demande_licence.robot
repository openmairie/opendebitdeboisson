*** Settings ***
Documentation  Mots-clés rattachés aux demandes de licence.

*** Keywords ***
Depuis le contexte de la demande de licence sur l'établissement
    [Documentation]  ...
    [Tags]  demande de licence
    [Arguments]  ${etablissement_id}  ${demande_licence_id}
    Depuis l'onglet 'Demandes de licence' de l'établissement  ${etablissement_id}
    Input Text  recherchedyn  ${demande_licence_id}
    Click On Link  ${demande_licence_id}

Depuis le contexte de la demande de licence sur l'établissement temporaire
    [Documentation]  ...
    [Tags]  demande de licence
    [Arguments]  ${etablissement_id}  ${demande_licence_id}
    Depuis l'onglet 'Demandes de licence' de l'établissement temporaire  ${etablissement_id}
    Input Text  recherchedyn  ${demande_licence_id}
    Click On Link  ${demande_licence_id}

Depuis le formulaire d'ajout d'une demande de licence sur l'établissement
    [Documentation]  ...
    [Tags]  demande de licence
    [Arguments]  ${etablissement_id}
    Depuis l'onglet 'Demandes de licence' de l'établissement  ${etablissement_id}
    Click On Add Button JS
    La page ne doit pas contenir d'erreur

Depuis le formulaire d'ajout d'une demande de licence sur l'établissement temporaire
    [Documentation]  ...
    [Tags]  demande de licence
    [Arguments]  ${etablissement_id}
    Depuis l'onglet 'Demandes de licence' de l'établissement temporaire  ${etablissement_id}
    Click On Add Button JS
    La page ne doit pas contenir d'erreur

Ajouter la demande de licence sur l'établissement
    [Documentation]  ...
    [Tags]  demande de licence
    [Arguments]  ${values}
    Depuis le formulaire d'ajout d'une demande de licence sur l'établissement  ${values.etablissement_id}
    Saisir les informations dans le formulaire de demande de licence  ${values}
    # On valide le formulaire
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    # On récupère l'ID du nouvel enregistrement
    ${demande_licence} =  Get Text  css=div.form-content span#demande_licence
    [Return]  ${demande_licence}

Ajouter la demande de licence sur l'établissement temporaire
    [Documentation]  ...
    [Tags]  demande de licence
    [Arguments]  ${values}
    Depuis le formulaire d'ajout d'une demande de licence sur l'établissement temporaire  ${values.etablissement_id}
    Saisir les informations dans le formulaire de demande de licence  ${values}
    # On valide le formulaire
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    # On récupère l'ID du nouvel enregistrement
    ${demande_licence} =  Get Text  css=div.form-content span#demande_licence
    [Return]  ${demande_licence}

Modifier la demande de licence sur un établissement
    [Documentation]  ...
    [Tags]  demande de licence
    [Arguments]  ${demande_licence_id}  ${values}
    Depuis le contexte de l'établissement  ${values.etablissement_id}
    Depuis l'onglet 'Demandes de licence' de l'établissement  ${values.etablissement_id}
    Click On Link  ${demande_licence_id}
    Click On SubForm Portlet Action  demande_licence  modifier
    Saisir les informations dans le formulaire de demande de licence  ${values}
    Click On Submit Button In Subform

Saisir les informations dans le formulaire de demande de licence
    [Documentation]  Remplit le formulaire
    [Tags]  demande de licence
    [Arguments]  ${values}
    Si "date_demande_licence" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_ancienne_demande" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "type_demande" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "type_licence" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "statut_demande" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "numero_licence" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "occasion" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "particularite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terme" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_debut_validite" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure_debut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_fin_validite" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure_fin" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire