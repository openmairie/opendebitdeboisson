*** Settings ***
Documentation  Fonctions et méthodes de traitement

*** Keywords ***
Si "${key}" existe dans "${collection}" on execute "${keyword}" sans argument
    [Documentation]  Ce mot-clé sert dans le cas d'un appel à un mot-clé
    ...  ${keyword} sans argument
    [Tags]  utils
    ${exist} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${collection}  ${key}
    Run Keyword If  ${exist} == True  ${keyword}
