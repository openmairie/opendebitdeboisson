*** Settings ***
Documentation    CRUD de la table titre_de_civilite
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/09/2018 11:09

*** Keywords ***

Depuis le contexte titre de civilité
    [Documentation]  Accède au formulaire
    [Arguments]  ${titre_de_civilite}

    # On accède au tableau
    Go To Tab  titre_de_civilite
    # On recherche l'enregistrement
    Use Simple Search  titre de civilité  ${titre_de_civilite}
    # On clique sur le résultat
    Click On Link  ${titre_de_civilite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter titre de civilité
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  titre_de_civilite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir titre de civilité  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${titre_de_civilite} =  Get Text  css=div.form-content span#titre_de_civilite
    # On le retourne
    [Return]  ${titre_de_civilite}

Modifier titre de civilité
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${titre_de_civilite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte titre de civilité  ${titre_de_civilite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  titre_de_civilite  modifier
    # On saisit des valeurs
    Saisir titre de civilité  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer titre de civilité
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${titre_de_civilite}

    # On accède à l'enregistrement
    Depuis le contexte titre de civilité  ${titre_de_civilite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  titre_de_civilite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir titre de civilité
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire