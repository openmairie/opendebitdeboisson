*** Settings ***
Documentation    CRUD de la table type_etablissement
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/09/2018 11:09

*** Keywords ***

Depuis le contexte type d'établissement
    [Documentation]  Accède au formulaire
    [Arguments]  ${type_etablissement}

    # On accède au tableau
    Go To Tab  type_etablissement
    # On recherche l'enregistrement
    Use Simple Search  type d'établissement  ${type_etablissement}
    # On clique sur le résultat
    Click On Link  ${type_etablissement}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type d'établissement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  type_etablissement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type d'établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${type_etablissement} =  Get Text  css=div.form-content span#type_etablissement
    # On le retourne
    [Return]  ${type_etablissement}

Modifier type d'établissement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${type_etablissement}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type d'établissement  ${type_etablissement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  type_etablissement  modifier
    # On saisit des valeurs
    Saisir type d'établissement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type d'établissement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${type_etablissement}

    # On accède à l'enregistrement
    Depuis le contexte type d'établissement  ${type_etablissement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  type_etablissement  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type d'établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire