*** Settings ***
Documentation    CRUD de la table demande_licence
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/09/2018 11:09

*** Keywords ***

Depuis le contexte demande de licence
    [Documentation]  Accède au formulaire
    [Arguments]  ${demande_licence}

    # On accède au tableau
    Go To Tab  demande_licence
    # On recherche l'enregistrement
    Use Simple Search  demande de licence  ${demande_licence}
    # On clique sur le résultat
    Click On Link  ${demande_licence}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter demande de licence
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  demande_licence
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir demande de licence  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${demande_licence} =  Get Text  css=div.form-content span#demande_licence
    # On le retourne
    [Return]  ${demande_licence}

Modifier demande de licence
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${demande_licence}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte demande de licence  ${demande_licence}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  demande_licence  modifier
    # On saisit des valeurs
    Saisir demande de licence  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer demande de licence
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${demande_licence}

    # On accède à l'enregistrement
    Depuis le contexte demande de licence  ${demande_licence}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  demande_licence  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir demande de licence
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "date_demande_licence" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_ancienne_demande" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "type_demande" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "type_licence" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "compte_rendu" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "statut_demande" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "numero_licence" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "occasion" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "particularite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terme" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_debut_validite" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure_debut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_fin_validite" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure_fin" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire