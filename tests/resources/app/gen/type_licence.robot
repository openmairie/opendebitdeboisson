*** Settings ***
Documentation    CRUD de la table type_licence
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/09/2018 11:09

*** Keywords ***

Depuis le contexte type de licence
    [Documentation]  Accède au formulaire
    [Arguments]  ${type_licence}

    # On accède au tableau
    Go To Tab  type_licence
    # On recherche l'enregistrement
    Use Simple Search  type de licence  ${type_licence}
    # On clique sur le résultat
    Click On Link  ${type_licence}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type de licence
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  type_licence
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type de licence  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${type_licence} =  Get Text  css=div.form-content span#type_licence
    # On le retourne
    [Return]  ${type_licence}

Modifier type de licence
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${type_licence}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type de licence  ${type_licence}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  type_licence  modifier
    # On saisit des valeurs
    Saisir type de licence  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type de licence
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${type_licence}

    # On accède à l'enregistrement
    Depuis le contexte type de licence  ${type_licence}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  type_licence  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type de licence
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "contrainte_proximite" existe dans "${values}" on execute "Set Checkbox" dans le formulaire