*** Settings ***
Documentation    CRUD de la table perimetre
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/09/2018 11:09

*** Keywords ***

Depuis le contexte périmètre
    [Documentation]  Accède au formulaire
    [Arguments]  ${perimetre}

    # On accède au tableau
    Go To Tab  perimetre
    # On recherche l'enregistrement
    Use Simple Search  périmètre  ${perimetre}
    # On clique sur le résultat
    Click On Link  ${perimetre}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter périmètre
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  perimetre
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir périmètre  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${perimetre} =  Get Text  css=div.form-content span#perimetre
    # On le retourne
    [Return]  ${perimetre}

Modifier périmètre
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${perimetre}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte périmètre  ${perimetre}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  perimetre  modifier
    # On saisit des valeurs
    Saisir périmètre  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer périmètre
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${perimetre}

    # On accède à l'enregistrement
    Depuis le contexte périmètre  ${perimetre}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  perimetre  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir périmètre
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "longueur_exclusion_metre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire