*** Settings ***
Documentation    CRUD de la table rivoli
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/07/2018 01:07

*** Keywords ***

Depuis le contexte rivoli
    [Documentation]  Accède au formulaire
    [Arguments]  ${rivoli}

    # On accède au tableau
    Go To Tab  rivoli
    # On recherche l'enregistrement
    Use Simple Search  rivoli  ${rivoli}
    # On clique sur le résultat
    Click On Link  ${rivoli}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter rivoli
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  rivoli
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir rivoli  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${rivoli} =  Get Text  css=div.form-content span#rivoli
    # On le retourne
    [Return]  ${rivoli}

Modifier rivoli
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${rivoli}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte rivoli  ${rivoli}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  rivoli  modifier
    # On saisit des valeurs
    Saisir rivoli  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer rivoli
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${rivoli}

    # On accède à l'enregistrement
    Depuis le contexte rivoli  ${rivoli}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  rivoli  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir rivoli
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire