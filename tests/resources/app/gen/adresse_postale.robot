*** Settings ***
Documentation    CRUD de la table adresse_postale
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/09/2018 11:09

*** Keywords ***

Depuis le contexte adresse postale
    [Documentation]  Accède au formulaire
    [Arguments]  ${adresse_postale}

    # On accède au tableau
    Go To Tab  adresse_postale
    # On recherche l'enregistrement
    Use Simple Search  adresse postale  ${adresse_postale}
    # On clique sur le résultat
    Click On Link  ${adresse_postale}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter adresse postale
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  adresse_postale
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir adresse postale  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${adresse_postale} =  Get Text  css=div.form-content span#adresse_postale
    # On le retourne
    [Return]  ${adresse_postale}

Modifier adresse postale
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${adresse_postale}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte adresse postale  ${adresse_postale}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  adresse_postale  modifier
    # On saisit des valeurs
    Saisir adresse postale  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer adresse postale
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${adresse_postale}

    # On accède à l'enregistrement
    Depuis le contexte adresse postale  ${adresse_postale}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  adresse_postale  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir adresse postale
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "rivoli" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire