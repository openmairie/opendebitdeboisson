*** Settings ***
Documentation    CRUD de la table etablissement
...    @author  generated
...    @package openDébitDeBoisson
...    @version 10/07/2019 16:07

*** Keywords ***

Depuis le contexte établissement
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement}

    # On accède au tableau
    Go To Tab  etablissement
    # On recherche l'enregistrement
    Use Simple Search  établissement  ${etablissement}
    # On clique sur le résultat
    Click On Link  ${etablissement}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter établissement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement} =  Get Text  css=div.form-content span#etablissement
    # On le retourne
    [Return]  ${etablissement}

Modifier établissement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte établissement  ${etablissement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement  modifier
    # On saisit des valeurs
    Saisir établissement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer établissement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement}

    # On accède à l'enregistrement
    Depuis le contexte établissement  ${etablissement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "raison_sociale" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "enseigne" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "no_siret" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "forme_juridique" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "type_etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "permis_exploitation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "date_permis_exploitation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "permis_exploitation_nuit" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "date_permis_exploitation_nuit" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "numero_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_etablissement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_etablissement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "civilite_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom_marital_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_naissance_exploitant" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "ville_naissance_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_naissance_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "qualite_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nationalite_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "particularite_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "civilite_co_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom_marital_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_naissance_co_exploitant" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "ville_naissance_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_naissance_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nationalite_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "qualite_co_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "particularite_co_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom_ancien_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom_ancien_exploitant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "qualite_ancien_exploitant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "civilite_proprietaire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "profession_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_fermeture" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_liquidation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ancien_proprietaire" existe dans "${values}" on execute "Input Text" dans le formulaire