*** Settings ***
Documentation    CRUD de la table type_demande
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/09/2018 11:09

*** Keywords ***

Depuis le contexte type de demande
    [Documentation]  Accède au formulaire
    [Arguments]  ${type_demande}

    # On accède au tableau
    Go To Tab  type_demande
    # On recherche l'enregistrement
    Use Simple Search  type de demande  ${type_demande}
    # On clique sur le résultat
    Click On Link  ${type_demande}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type de demande
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  type_demande
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type de demande  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${type_demande} =  Get Text  css=div.form-content span#type_demande
    # On le retourne
    [Return]  ${type_demande}

Modifier type de demande
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${type_demande}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type de demande  ${type_demande}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  type_demande  modifier
    # On saisit des valeurs
    Saisir type de demande  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type de demande
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${type_demande}

    # On accède à l'enregistrement
    Depuis le contexte type de demande  ${type_demande}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  type_demande  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type de demande
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire