*** Settings ***
Documentation    CRUD de la table courrier
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/07/2018 01:07

*** Keywords ***

Depuis le contexte courrier
    [Documentation]  Accède au formulaire
    [Arguments]  ${courrier}

    # On accède au tableau
    Go To Tab  courrier
    # On recherche l'enregistrement
    Use Simple Search  courrier  ${courrier}
    # On clique sur le résultat
    Click On Link  ${courrier}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter courrier
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  courrier
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir courrier  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${courrier} =  Get Text  css=div.form-content span#courrier
    # On le retourne
    [Return]  ${courrier}

Modifier courrier
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${courrier}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte courrier  ${courrier}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  courrier  modifier
    # On saisit des valeurs
    Saisir courrier  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer courrier
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${courrier}

    # On accède à l'enregistrement
    Depuis le contexte courrier  ${courrier}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  courrier  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir courrier
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "date_courrier" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "modele_lettre_type" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "objet" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "corps_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "fichier" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fichier_finalise" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire