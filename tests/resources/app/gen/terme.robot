*** Settings ***
Documentation    CRUD de la table terme
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/07/2018 01:07

*** Keywords ***

Depuis le contexte terme
    [Documentation]  Accède au formulaire
    [Arguments]  ${terme}

    # On accède au tableau
    Go To Tab  terme
    # On recherche l'enregistrement
    Use Simple Search  terme  ${terme}
    # On clique sur le résultat
    Click On Link  ${terme}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter terme
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  terme
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir terme  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${terme} =  Get Text  css=div.form-content span#terme
    # On le retourne
    [Return]  ${terme}

Modifier terme
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${terme}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte terme  ${terme}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  terme  modifier
    # On saisit des valeurs
    Saisir terme  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer terme
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${terme}

    # On accède à l'enregistrement
    Depuis le contexte terme  ${terme}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  terme  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir terme
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire