*** Settings ***
Documentation    CRUD de la table statut_demande
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/09/2018 11:09

*** Keywords ***

Depuis le contexte statut de la demande
    [Documentation]  Accède au formulaire
    [Arguments]  ${statut_demande}

    # On accède au tableau
    Go To Tab  statut_demande
    # On recherche l'enregistrement
    Use Simple Search  statut de la demande  ${statut_demande}
    # On clique sur le résultat
    Click On Link  ${statut_demande}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter statut de la demande
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  statut_demande
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir statut de la demande  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${statut_demande} =  Get Text  css=div.form-content span#statut_demande
    # On le retourne
    [Return]  ${statut_demande}

Modifier statut de la demande
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${statut_demande}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte statut de la demande  ${statut_demande}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  statut_demande  modifier
    # On saisit des valeurs
    Saisir statut de la demande  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer statut de la demande
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${statut_demande}

    # On accède à l'enregistrement
    Depuis le contexte statut de la demande  ${statut_demande}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  statut_demande  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir statut de la demande
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire