*** Settings ***
Documentation    CRUD de la table dossier
...    @author  generated
...    @package openDébitDeBoisson
...    @version 13/07/2018 01:07

*** Keywords ***

Depuis le contexte dossier
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier}

    # On accède au tableau
    Go To Tab  dossier
    # On recherche l'enregistrement
    Use Simple Search  dossier  ${dossier}
    # On clique sur le résultat
    Click On Link  ${dossier}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter dossier
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  dossier
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir dossier  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier} =  Get Text  css=div.form-content span#dossier
    # On le retourne
    [Return]  ${dossier}

Modifier dossier
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte dossier  ${dossier}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier  modifier
    # On saisit des valeurs
    Saisir dossier  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer dossier
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier}

    # On accède à l'enregistrement
    Depuis le contexte dossier  ${dossier}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir dossier
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "date_piece" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "fichier" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire