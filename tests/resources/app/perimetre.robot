*** Settings ***
Documentation  Mots-clés rattachés aux périmètres d'exclusion.

*** Keywords ***
Depuis le listing des périmètres
    [Documentation]  ...
    [Tags]  périmètre
    Depuis le listing  perimetre
    Page Title Should Be  Application > Périmètres D'exclusion

Depuis le contexte du périmètre
    [Documentation]  ...
    [Arguments]  ${perimetre_id}
    [Tags]  périmètre
    Depuis le listing des périmètres
    Input Text  recherche  ${perimetre_id}
    Click On Search Button
    Click On Link  ${perimetre_id}
    Page Title Should Contain  Application > Périmètres D'exclusion > ${perimetre_id}
    La page ne doit pas contenir d'erreur

Depuis le formulaire d'ajout d'un périmètre
    [Documentation]  ...
    [Tags]  périmètre
    Depuis le listing des périmètres
    Click On Add Button
    Page Title Should Contain  Application > Périmètres D'exclusion
    La page ne doit pas contenir d'erreur

Ajouter le périmètre
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au formulaire d'ajout
    Depuis le formulaire d'ajout d'un périmètre
    # On saisit des valeurs
    Saisir le périmètre  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${perimetre} =  Get Text  css=div.form-content span#perimetre
    # On le retourne
    [Return]  ${perimetre}

Modifier le périmètre
    [Arguments]  ${perimetre_id}  ${values}
    [Tags]  périmètre
    Depuis le contexte du périmètre  ${perimetre_id}
    Click On Form Portlet Action  perimetre  modifier
    Si "adresse_postale" existe dans "${values}" on execute "Désactiver la saisie manuelle de l'adresse du périmètre" sans argument
    Saisir le périmètre  ${values}
    Click On Submit Button

Désactiver la saisie manuelle de l'adresse du périmètre
    [Documentation]  ...
    [Tags]  périmètre
    Set Checkbox  saisie_manuelle  false
    Wait Until Element Is Visible  css=div.ui-dialog
    Element Should Contain  css=div.ui-dialog  Cette action videra les champs de l'adresse du périmètre d'exclusion.
    Click Element  id=ui-button-valid
    Wait Until Element Is Not Visible  css=div.ui-dialog

Saisir le périmètre
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "longueur_exclusion_metre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_postale" existe dans "${values}" on sélectionne la valeur sur l'autocomplete "adresse_postale" dans le formulaire
