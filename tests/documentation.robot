*** Settings ***
Documentation     TestSuite "Documentation" : cette suite permet d'extraire
...    automatiquement les captures à destination de la documentation.
# On inclut les mots-clefs
Resource    resources/resources.robot
# On ouvre et on ferme le navigateur respectivement au début et à la fin
# du Test Suite.
Suite Setup    For Suite Setup
Suite Teardown    For Suite Teardown
# A chaque début de Test Case on positionne la taille de la fenêtre
# pour obtenir des captures homogènes
Test Setup    Set Window Size  ${1280}  ${1024}

*** Keywords ***
Highlight heading
    [Arguments]  ${locator}

    #Update element style  ${locator}  margin-top  0.75em
    Highlight  ${locator}


*** Test Cases ***
Prérequis
    [Documentation]  L'objet de ce 'Test Case' est de respecter les prérequis
    ...    nécessaires aux captures d'écran.
    [Tags]  doc
    # Création des répertoires destinés à recevoir les captures d'écran
    # selon le respect de l'architecture de la documentation
    Create Directory  results/screenshots
    Create Directory  results/screenshots/administration
    Create Directory  results/screenshots/ergonomie
    Create Directory  results/screenshots/instruction


Constitution d'un jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de de
    ...    données cohérent pour les scénarios fonctionnels qui suivent.
    [Tags]  doc
    # On veut que la version du logiciel affichée sur les captures soit la majeure
    Copy File  ${EXECDIR}${/}binary_files${/}doc_version.inc.php  ${EXECDIR}${/}..${/}dyn${/}version.inc.php

    #
    Depuis la page d'accueil  admin  admin
    #
    &{etablissement_temporaire_01} =  Create Dictionary
    ...  raison_sociale=ETS TEMPORAIRE 01 RS
    ...  enseigne=ETS TEMPORAIRE 01 ENSEIGNE
    ${etablissement_temporaire_01_id} =  Ajouter l'établissement temporaire  ${etablissement_temporaire_01}
    Set Suite Variable  ${etablissement_temporaire_01}
    Set Suite Variable  ${etablissement_temporaire_01_id}
    #
    &{etablissement_permanent_01} =  Create Dictionary
    ...  raison_sociale=ETS PERMANENT 01 RS
    ...  enseigne=ETS PERMANENT 01 ENSEIGNE
    ...  adresse_postale=68 - BIS - RUE ALEXIS LEPÈRE
    ${etablissement_permanent_01_id} =  Ajouter l'établissement  ${etablissement_permanent_01}
    Set Suite Variable  ${etablissement_permanent_01}
    Set Suite Variable  ${etablissement_permanent_01_id}
    #
    &{demande_licence_01} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_01_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_01_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_01}
    Set Suite Variable  ${demande_licence_01}
    Set Suite Variable  ${demande_licence_01_id}
    #
    &{demande_licence_temporaire_01} =  Create Dictionary
    ...  etablissement_id=${etablissement_temporaire_01_id}
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_temporaire_01_id} =  Ajouter la demande de licence sur l'établissement temporaire  ${demande_licence_temporaire_01}
    Set Suite Variable  ${demande_licence_temporaire_01}
    Set Suite Variable  ${demande_licence_temporaire_01_id}

Ergonomie
    [Documentation]  Section 'Ergonomie'.
    [Tags]  doc
    # Les méthodes Suite Setup et Suite Teardown gèrent l'ouverture et la
    # fermeture du navigateur. Dans le cas de ce TestSuite on a besoin de
    # travailler sur un navigateur fraichement ouvert pour être sûr que la
    # variable de session est neuve.
    Fermer le navigateur
    Ouvrir le navigateur
    Depuis la page de login
    Capture viewport screenshot  screenshots/ergonomie/a_connexion_formulaire.png
    #
    Input Username    admin
    Input Password    plop
    Click Button    login.action.connect
    Wait Until Keyword Succeeds    ${TIMEOUT}    ${RETRY_INTERVAL}    Error Message Should Be    Votre identifiant ou votre mot de passe est incorrect.
    Capture and crop page screenshot  screenshots/ergonomie/a_connexion_message_erreur.png
    ...  css=div.message
    #
    Input Username    admin
    Input Password    admin
    Click Button    login.action.connect
    Wait Until Element Is Visible    css=#actions a.actions-logout
    Capture and crop page screenshot  screenshots/ergonomie/a_connexion_message_ok.png
    ...  css=div.message
    #
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_actions_globales.png
    ...  footer
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_actions_personnelles.png
    ...  actions
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_logo.png
    ...  logo
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_raccourcis.png
    ...  shortlinks
    #
    Highlight heading  css=li.actions-logout
    Capture and crop page screenshot  screenshots/ergonomie/a_deconnexion_action.png
    ...  header
    #
    Go To Dashboard
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_menu.png
    ...  menu
    #
    Go To Dashboard
    Se déconnecter
    Capture and crop page screenshot  screenshots/ergonomie/a_deconnexion_message_ok.png
    ...  css=div.message
    #
    Depuis la page d'accueil  admin  admin
    Go To Dashboard
    Remove element  dashboard
    Update element style  css=#content  height  300px
    Add pointy note  css=#logo  Logo  position=right
    Highlight heading  css=#menu
    Add note  css=#menu  Menu  position=right
    Add pointy note  css=#shortlinks  Raccourcis  position=bottom
    Add pointy note  css=#actions  Actions personnelles  position=left
    Highlight heading  css=#footer
    Add note  css=#footer  Actions globales  position=top
    Capture viewport screenshot  screenshots/ergonomie/a_ergonomie_generale_detail.png
    #
    Set Window Size  ${1280}  ${1024}
    Go To Dashboard
    Capture and crop page screenshot  screenshots/ergonomie/a_tableau-de-bord-exemple.png
    ...  content
    Capture and crop page screenshot  screenshots/ergonomie/a_widget_dernieres_demandes.png
    ...  css=div.widget_dernieres_demandes
    Capture and crop page screenshot  screenshots/ergonomie/a_widget_statistiques_licences_par_type.png
    ...  css=div.widget_statistiques_licences_par_type


Instruction
    [Documentation]
    [Tags]  doc
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des établissements
    Capture and crop page screenshot  screenshots/instruction/a_instruction-etablissement-listing.png
    ...  content
    #
    Depuis le listing des établissements
    Click Element  css=#toggle-advanced-display
    Capture and crop page screenshot  screenshots/instruction/a_instruction-etablissement-listing-recherche-avancee.png
    ...  advanced-form
    #
    Depuis le formulaire d'ajout d'un établissement
    Capture and crop page screenshot  screenshots/instruction/a_instruction-etablissement-formulaire-ajout.png
    ...  content
    Input Text  css=#autocomplete-adresse_postale-search  66 - BIS - RUE ALEXIS LEPÈRE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link  66 - BIS - RUE ALEXIS LEPÈRE
    Sleep  1
    Capture and crop page screenshot  screenshots/instruction/a_instruction-etablissement-formulaire-adresse_postale.png
    ...  css=.etablissement-bloc-adresse
    #
    Depuis le formulaire d'ajout d'un établissement temporaire
    Capture and crop page screenshot  screenshots/instruction/a_instruction-etablissement_temporaire-formulaire-ajout.png
    ...  content
    #
    Depuis le contexte de l'établissement  1
    On clique sur l'onglet  demande_licence  demande de licence
    Capture and crop page screenshot  screenshots/instruction/a_instruction-demande_licence-listing.png
    ...  content
    #
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/instruction/a_instruction-demande_licence-formulaire-ajout.png
    ...  content
    #
    Depuis le contexte de la demande de licence sur l'établissement  1  1
    Capture and crop page screenshot  screenshots/instruction/a_instruction-demande_licence-permanent-formulaire-actions.png
    ...  css=div#sousform-container div#portlet-actions
    #
    Depuis le contexte de la demande de licence sur l'établissement temporaire  ${etablissement_temporaire_01_id}  ${demande_licence_temporaire_01_id}
    Capture and crop page screenshot  screenshots/instruction/a_instruction-demande_licence-temporaire-formulaire-actions.png
    ...  css=div#sousform-container div#portlet-actions
    #Permis d'exploitation
    Modifier la demande de licence sur un établissement  ${demande_licence_01_id}  ${demande_licence_01}
    Capture and crop page screenshot  screenshots/instruction/a_instruction-demande_licence-formulaire-message-permis.png
    ...  css=div.formEntete div.message
    Depuis le contexte de l'établissement  ${etablissement_permanent_01_id}
    Click On Form Portlet Action  etablissement_permanent  modifier
    Set Checkbox  permis_exploitation  true
    Click On Submit Button
    #Vérification du traitement des contraintes de proximité avec un périmètre d'exclusion
    &{perimetre_values} =  Create Dictionary
    ...  libelle=PERIMETRE01LIBDEMANDEL
    ...  adresse_postale=71 - BIS - RUE ALEXIS LEPÈRE
    ${perimetre_id} =  Ajouter le périmètre  ${perimetre_values}

    &{type_licence_values} =  Create Dictionary
    ...  contrainte_proximite=true
    Modifier le type de licence  ${demande_licence_01.type_licence}  ${type_licence_values}

    Modifier la demande de licence sur un établissement  ${demande_licence_01_id}  ${demande_licence_01}
    Capture and crop page screenshot  screenshots/instruction/a_instruction-demande_licence-formulaire-message-perimetre-exclusion.png
    ...  css=div.formEntete div.message
    #Vérification du traitement des contraintes de proximité avec un autre établissement
    &{etablissement_permanent_02} =  Create Dictionary
    ...  raison_sociale=ETS PERMANENT 02 RS
    ...  enseigne=ETS PERMANENT 02 ENSEIGNE
    ...  adresse_postale=23 - Q - RUE ANATOLE FRANCE
    ...  permis_exploitation=true
    ${etablissement_permanent_02_id} =  Ajouter l'établissement  ${etablissement_permanent_02}

    &{etablissement_permanent_03} =  Create Dictionary
    ...  raison_sociale=ETS PERMANENT 03 RS
    ...  enseigne=ETS PERMANENT 03 ENSEIGNE
    ...  adresse_postale=23 - Q - RUE ANATOLE FRANCE
    ...  permis_exploitation=true
    ${etablissement_permanent_03_id} =  Ajouter l'établissement  ${etablissement_permanent_03}

    &{demande_licence_02} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_02_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_02_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_02}

    &{demande_licence_03} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_03_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Permanente
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=10/01/2018
    ${demande_licence_03_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_03}

    Modifier la demande de licence sur un établissement  ${demande_licence_03_id}  ${demande_licence_03}
    Capture and crop page screenshot  screenshots/instruction/a_instruction-demande_licence-formulaire-message-proximite-etablissement.png
    ...  css=div.formEntete div.message
    #Limite demande licence temporaire et liée à un terrain de sport
    &{etablissement_permanent_04} =  Create Dictionary
    ...  raison_sociale=ETS PERMANENT 04 RS
    ...  enseigne=ETS PERMANENT 04 ENSEIGNE
    ...  permis_exploitation=true
    ${etablissement_permanent_04_id} =  Ajouter l'établissement  ${etablissement_permanent_04}

    &{demande_licence_04} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_04_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=01/01/2017
    ...  date_fin_validite=05/01/2017
    ${demande_licence_04_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_04}

    &{demande_licence_05} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_04_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=05/01/2017
    ...  date_fin_validite=10/01/2017
    ${demande_licence_05_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_05}

    &{demande_licence_06} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_04_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=10/01/2017
    ...  date_fin_validite=15/01/2017
    ${demande_licence_06_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_06}

    &{demande_licence_07} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_04_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=15/01/2017
    ...  date_fin_validite=20/01/2017
    ${demande_licence_07_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_07}

    &{demande_licence_08} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_04_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=25/01/2017
    ...  date_fin_validite=30/01/2017
    ${demande_licence_08_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_08}

    &{demande_licence_09} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_04_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Temporaire
    ...  date_debut_validite=01/02/2017
    ...  date_fin_validite=05/02/2017
    ${demande_licence_09_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_09}

    Modifier la demande de licence sur un établissement  ${demande_licence_09_id}  ${demande_licence_09}
    Capture and crop page screenshot  screenshots/instruction/a_instruction-demande_licence-formulaire-message-limite-temporaire.png
    ...  css=div.formEntete div.message

    &{demande_licence_10} =  Create Dictionary
    ...  etablissement_id=${etablissement_permanent_04_id}
    ...  type_demande=D'OUVERTURE
    ...  type_licence=Licence restaurant
    ...  terme=Liée à un terrain de sport
    ...  date_debut_validite=01/02/2017
    ...  date_fin_validite=05/02/2017
    ${demande_licence_10_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_10}

    Modifier la demande de licence sur un établissement  ${demande_licence_09_id}  ${demande_licence_10}
    Modifier la demande de licence sur un établissement  ${demande_licence_08_id}  ${demande_licence_10}
    Modifier la demande de licence sur un établissement  ${demande_licence_07_id}  ${demande_licence_10}
    Modifier la demande de licence sur un établissement  ${demande_licence_06_id}  ${demande_licence_10}
    Modifier la demande de licence sur un établissement  ${demande_licence_05_id}  ${demande_licence_10}
    Modifier la demande de licence sur un établissement  ${demande_licence_04_id}  ${demande_licence_10}
    ${demande_licence_11_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_10}
    ${demande_licence_12_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_10}
    ${demande_licence_13_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_10}
    ${demande_licence_14_id} =  Ajouter la demande de licence sur l'établissement  ${demande_licence_10}
    Modifier la demande de licence sur un établissement  ${demande_licence_14_id}  ${demande_licence_10}
    Capture and crop page screenshot  screenshots/instruction/a_instruction-demande_licence-formulaire-message-limite-sport.png
    ...  css=div.formEntete div.message

    #Vue synthétique
    Go to  ${PROJECT_URL}${OM_ROUTE_MAP}&mode=tab_sig&obj=carte_globale
    Sleep  2
    Capture and crop page screenshot  screenshots/instruction/a_carte_globale.png
    ...  content
    Click Element  css=image[id^="OpenLayers_Geometry_Point_"]
    # Click Element  css=div#map-getfeatures div#cssmenu li a[onclick="affiche_getfeatures()"]
    Execute Javascript  affiche_getfeatures()
        Capture and crop page screenshot  screenshots/instruction/a_carte_globale_infos.png
    ...  content
    Click Element  css=div#map-getfeatures-markers a
    Sleep  2
    Capture and crop page screenshot  screenshots/instruction/a_carte_globale_overlay.png
    ...  content

    #
    Depuis le contexte de l'établissement  1
    On clique sur l'onglet  dossier  dossier
    Capture and crop page screenshot  screenshots/instruction/a_instruction-dossier-listing.png
    ...  content
    #
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/instruction/a_instruction-dossier-formulaire-ajout.png
    ...  content
    #
    Depuis le contexte de l'établissement  1
    On clique sur l'onglet  courrier  courrier
    Capture and crop page screenshot  screenshots/instruction/a_instruction-courrier-listing.png
    ...  content
    #
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/instruction/a_instruction-courrier-formulaire-ajout.png
    ...  content
    #
    Depuis le listing des périmètres
    Capture and crop page screenshot  screenshots/instruction/a_instruction-perimetre-listing.png
    ...  content
    #
    Depuis le listing des périmètres
    Click Element  css=#toggle-advanced-display
    Capture and crop page screenshot  screenshots/instruction/a_instruction-perimetre-listing-recherche-avancee.png
    ...  advanced-form
    #
    Depuis le formulaire d'ajout d'un périmètre
    Capture and crop page screenshot  screenshots/instruction/a_instruction-perimetre-formulaire-ajout.png
    ...  content
    Input Text  css=#autocomplete-adresse_postale-search  66 - BIS - RUE ALEXIS LEPÈRE
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link  66 - BIS - RUE ALEXIS LEPÈRE
    Sleep  1
    Capture and crop page screenshot  screenshots/instruction/a_instruction-perimetre-formulaire-adresse_postale.png
    ...  content


Administration & Paramétrage
    [Documentation]
    [Tags]  doc
    Depuis la page d'accueil  admin  admin
    #
    Highlight heading  css=li.shortlinks-settings
    Capture and crop page screenshot  screenshots/administration/a_administration_parametrage-action.png
    ...  header
    #
    Depuis le menu 'Administration & Paramétrage'
    Capture and crop page screenshot  screenshots/administration/a_administration_parametrage-menu.png
    ...    css=#content


Déconstitution du jeu de données
    [Documentation]
    [Tags]  doc
    #
    Remove File  ${EXECDIR}${/}..${/}dyn${/}version.inc.php

