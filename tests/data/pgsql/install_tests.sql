--------------------------------------------------------------------------------
-- Script d'installation des jeux de données pour les tests
--
-- ATTENTION ce script est prévu pour être appliqué après le install.sql
--
-- @package opendebitdeboisson
-- @version SVN : $Id$
--------------------------------------------------------------------------------
--
START TRANSACTION;
--
\set schema 'opendebitdeboisson'
--
SET search_path = :schema, public, pg_catalog;
--
INSERT INTO "om_sig_flux" ("om_sig_flux", "libelle", "om_collectivite", "id", "attribution", "chemin", "couches", "cache_type", "cache_gfi_chemin", "cache_gfi_couches") VALUES
(nextval('om_sig_flux_seq'),
    'Cadastre - Section',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_section',
    NULL,
    'http://inspire.cadastre.gouv.fr/scpc/93048.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857& ',
    'AMORCES_CAD',
    NULL,   NULL,   NULL),
(nextval('om_sig_flux_seq'),
    'Cadastre - Parcellaire',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_parcellaire',
    NULL,
    'http://inspire.cadastre.gouv.fr/scpc/93048.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857&',
    'CP.CadastralParcel',
    NULL,   NULL,   NULL),
(nextval('om_sig_flux_seq'),
    'Cadastre - Bâti',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_bati',
    NULL,
    'http://inspire.cadastre.gouv.fr/scpc/93048.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857&',
    'BU.Building',
    NULL,   NULL,   NULL),
(nextval('om_sig_flux_seq'),
    'Cadastre - Autres',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_autres',
    NULL,
    'http://inspire.cadastre.gouv.fr/scpc/93048.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857&',
    'LIEUDIT,SUBFISCAL,CLOTURE,DETAIL_TOPO,HYDRO,VOIE_COMMUNICATION,BORNE_REPERE',
    NULL,   NULL,   NULL),
(nextval('om_sig_flux_seq'),
    'Cadastre - Toutes les couches',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_toutes',
    NULL,
    'http://inspire.cadastre.gouv.fr/scpc/93048.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857&',
    'AMORCES_CAD,LIEUDIT,CP.CadastralParcel,SUBFISCAL,CLOTURE,DETAIL_TOPO,HYDRO,VOIE_COMMUNICATION,BU.Building,BORNE_REPERE',
    NULL,   NULL,   NULL)
;
--
INSERT INTO om_sig_extent (om_sig_extent, nom, extent, valide) VALUES
(nextval('om_sig_extent_seq'), 'LIBREVILLE', '2.4054,48.8471,2.4926,48.8805', TRUE);
--
INSERT INTO om_sig_map (
    om_sig_map,
    om_collectivite,
    id,
    libelle,
    actif,
    zoom,
    fond_osm,
    fond_bing,
    fond_sat,
    layer_info,
    projection_externe,
    url,
    om_sql,
    retour,
    util_idx,
    util_reqmo,
    util_recherche,
    source_flux,
    fond_default,
    om_sig_extent,
    restrict_extent,
    sld_marqueur,
    sld_data,
    point_centrage
) VALUES (
    nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'carte_flux_generiques',
    'Carte pour flux génériques',
    false,
    '0',
    true,
    false,
    false,
    false,
    'EPSG:2154',
    '...',
    '...',
    '...',
    false,
    false,
    false,
    null,
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null
);
--
INSERT INTO om_sig_map (
    om_sig_map,
    om_collectivite,
    id,
    libelle,
    actif,
    zoom,
    fond_osm,
    fond_bing,
    fond_sat,
    layer_info,
    projection_externe,
    url,
    om_sql,
    retour,
    util_idx,
    util_reqmo,
    util_recherche,
    source_flux,
    fond_default,
    om_sig_extent,
    restrict_extent,
    sld_marqueur,
    sld_data,
    point_centrage
) VALUES (
    nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'perimetre',
    'perimetre',
    true,
    '12',
    true,
    false,
    false,
    false,
    'EPSG:2154',
    '...',
    'select ST_AsText(geom) as geom, (numero_voie||'' ''||libelle_voie) as titre,libelle as description, ''perimetre'' as class, longueur_exclusion_metre as rayon,perimetre as idx, ''./img/new.gif'' as img, ''./img/intero.gif'' as img_hover from &DB_PREFIXEperimetre',
    '...',
    true,
    false,
    true,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null
);
--
INSERT INTO om_sig_map (
    om_sig_map,
    om_collectivite,
    id,
    libelle,
    actif,
    zoom,
    fond_osm,
    fond_bing,
    fond_sat,
    layer_info,
    projection_externe,
    url,
    om_sql,
    retour,
    util_idx,
    util_reqmo,
    util_recherche,
    source_flux,
    fond_default,
    om_sig_extent,
    restrict_extent,
    sld_marqueur,
    sld_data,
    point_centrage
) VALUES (
    nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'etablissement_permanent',
    'etablissement_permanent',
    true,
    '12',
    true,
    false,
    false,
    false,
    'EPSG:2154',
    '...',
    'select ST_AsText(geom) as geom, (numero_voie||'' ''||libelle_voie) as titre, raison_sociale as description,etablissement as idx from &DB_PREFIXEetablissement order by geom,etablissement',
    '...',
    true,
    false,
    true,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null
);
--
INSERT INTO om_sig_map (
    om_sig_map,
    om_collectivite,
    id,
    libelle,
    actif,
    zoom,
    fond_osm,
    fond_bing,
    fond_sat,
    layer_info,
    projection_externe,
    url,
    om_sql,
    retour,
    util_idx,
    util_reqmo,
    util_recherche,
    source_flux,
    fond_default,
    om_sig_extent,
    restrict_extent,
    sld_marqueur,
    sld_data,
    point_centrage
) VALUES (
    nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'adresse_postale',
    'adresse_postale',
    true,
    '12',
    true,
    false,
    false,
    false,
    'EPSG:2154',
    '...',
    'SELECT ST_asText(''01010000206A080000C6DE4AFF7E552B412CF66CF750D35741'') as geom, ''2'' as titre, ''3'' as description, 4 as idx, ''5'' as plop',
    '...',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null
);
--
INSERT INTO om_sig_map (
    om_sig_map,
    om_collectivite,
    id,
    libelle,
    actif,
    zoom,
    fond_osm,
    fond_bing,
    fond_sat,
    layer_info,
    projection_externe,
    url,
    om_sql,
    retour,
    util_idx,
    util_reqmo,
    util_recherche,
    source_flux,
    fond_default,
    om_sig_extent,
    restrict_extent,
    sld_marqueur,
    sld_data,
    point_centrage
) VALUES (
    nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'carte_globale',
    'carte_globale',
    true,
    '12',
    true,
    false,
    false,
    false,
    'EPSG:2154',
    '../app/index.php?module=form&obj=etablissement_permanent&action=12&idx=',
    'select ST_AsText(geom) as geom, raison_sociale as titre,raison_sociale as description, ''etablissement'' as class,''0'' as rayon,etablissement as idx, ''./img/punaise.png'' as img, ''./img/punaise_hover.png'' as img_hover from &DB_PREFIXEetablissement',
    '...',
    false,
    false,
    true,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null
);
--
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='perimetre'),
    'centre du périmètre d''exclusion',
    0,
    true,
    true,
    'point',
    'perimetre',
    'geom',
    'perimetre',
    'perimetre'
),
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='etablissement_permanent'),
    'point représentant l''établissement',
    0,
    true,
    true,
    'point',
    'etablissement',
    'geom',
    'etablissement',
    'etablissement'
),
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='adresse_postale'),
    'point représentant l''adresse',
    0,
    true,
    true,
    'point',
    'adresse_postale',
    'geom',
    'adresse_postale',
    'adresse_postale'
)
;
--
--
INSERT INTO "om_sig_map_flux" ("om_sig_map_flux", "om_sig_flux", "om_sig_map", "ol_map", "ordre", "visibility", "panier", "pa_nom", "pa_layer", "pa_attribut", "pa_encaps", "pa_sql", "pa_type_geometrie", "sql_filter", "baselayer", "singletile", "maxzoomlevel") VALUES
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_section'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Section',
    15,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL),
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_parcellaire'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Parcellaire',
    20,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL),
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_bati'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Bâti',
    25,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL),
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_autres'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Autres',
    30,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL),
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_toutes'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Toutes',
    50,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL)
;
--
INSERT INTO om_widget (om_widget, libelle, lien, texte, type, script, arguments) VALUES
(nextval('om_widget_seq'), 'Carte globale', '../app/index.php?module=map&mode=tab_sig&obj=carte_globale', '<iframe style=''width:100%;border: 0 none; height: 350px;'' src=''../app/index.php?module=map&mode=tab_sig&obj=carte_globale&popup=1''></iframe>', 'web', '', '');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type, script, arguments) VALUES
(nextval('om_widget_seq'), 'Statistiques', '', '', 'file', 'statistiques_licences_par_type', '');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type, script, arguments) VALUES
(nextval('om_widget_seq'), 'Les 10 dernières demandes de licence', '', '', 'file', 'dernieres_demandes', '');
--
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES
(nextval('om_dashboard_seq'),
    (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
    'C1',
    1,
    (SELECT om_widget FROM om_widget WHERE libelle='Carte globale')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES
(nextval('om_dashboard_seq'),
    (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
    'C2',
    1,
    (SELECT om_widget FROM om_widget WHERE libelle='Les 10 dernières demandes de licence')
);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES
(nextval('om_dashboard_seq'),
    (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
    'C3',
    1,
    (SELECT om_widget FROM om_widget WHERE libelle='Statistiques')
);
--
INSERT INTO perimetre (perimetre, libelle, longueur_exclusion_metre, geom) VALUES
(nextval('perimetre_seq'), 'CIMETIERE', 150, '01010000206A0800002D9DD540592224416431280EB22D5A41'),
(nextval('perimetre_seq'), 'CRECHE', 400, '01010000206A080000EC902CBFA323244186EBBD69742E5A41'),
(nextval('perimetre_seq'), 'ECOLE MATERNELLE', 300, '01010000206A080000C1A9CD481619244136838F88982E5A41')
;
--
INSERT INTO qualite_exploitant (qualite_exploitant, libelle) VALUES
(nextval('qualite_exploitant_seq'), 'Gérant'),
(nextval('qualite_exploitant_seq'), 'Employé'),
(nextval('qualite_exploitant_seq'), 'Propriétaire')
;
--
INSERT INTO terme (terme, code, libelle) VALUES
(nextval('terme_seq'), 'PERM', 'Permanente'),
(nextval('terme_seq'), 'TEMP', 'Temporaire'),
(nextval('terme_seq'), 'SPOR', 'Liée à un terrain de sport')
;
--
INSERT INTO type_licence (type_licence, libelle, contrainte_proximite) VALUES
(nextval('type_licence_seq'), 'Licence de 3ème catégorie', true),
(nextval('type_licence_seq'), 'Licence de 4ème catégorie', true),
(nextval('type_licence_seq'), 'Petite licence à emporter', false),
(nextval('type_licence_seq'), 'Licence à emporter', false),
(nextval('type_licence_seq'), 'Petite licence restaurant', false),
(nextval('type_licence_seq'), 'Licence restaurant', false)
;
--
INSERT INTO type_demande (type_demande, code, libelle) VALUES
(nextval('type_demande_seq'), 'OUV', 'D''OUVERTURE'),
(nextval('type_demande_seq'), 'TRS', 'DE TRANSFERT'),
(nextval('type_demande_seq'), 'MUT', 'DE MUTATION')
;
--
INSERT INTO type_etablissement (type_etablissement, libelle) VALUES
(nextval('type_etablissement_seq'), 'Restaurant'),
(nextval('type_etablissement_seq'), 'Vente à emporter'),
(nextval('type_etablissement_seq'), 'Discothèque')
;
--
INSERT INTO statut_demande (statut_demande, libelle) VALUES
(nextval('statut_demande_seq'), 'Valide'),
(nextval('statut_demande_seq'), 'En cours de complétude'),
(nextval('statut_demande_seq'), 'Irrecevable')
;
--
INSERT INTO titre_de_civilite (titre_de_civilite, code, libelle) VALUES
(nextval('titre_de_civilite_seq'), 'M.', 'Monsieur'),
(nextval('titre_de_civilite_seq'), 'Mme', 'Madame'),
(nextval('titre_de_civilite_seq'), 'Sté', 'Société')
;
--
INSERT INTO etablissement (etablissement, nature, raison_sociale, enseigne, geom, permis_exploitation, permis_exploitation_nuit) VALUES
(nextval('etablissement_seq'), 'permanent', 'CAFE DU CENTRE', '-', null, false, false),
(nextval('etablissement_seq'), 'permanent', 'RESTAURANT CHEZ LUIGGI', '-', '01010000206A080000947BFF6CA61A2441617A0B97EA2D5A41', false, false),
(nextval('etablissement_seq'), 'permanent', 'BAR DU MARCHE', '-', '01010000206A0800006E12FAAEC71C2441CA7CC210A12D5A41', false, false),
(nextval('etablissement_seq'), 'permanent', 'LA TAVERNE DE DIETER', '-', '01010000206A080000E6A0EBA50819244157FEDEF3802D5A41', false, false),
(nextval('etablissement_seq'), 'permanent', 'LE RELAIS CORSE', '-', '01010000206A080000102C047270172441072E1DC1BD2D5A41', false, false),
(nextval('etablissement_seq'), 'permanent', 'LA CAVE A WHISKY', '-', '01010000206A080000D4B5C599762224417E16174F012E5A41', false, false)
;
INSERT INTO demande_licence (demande_licence, etablissement, date_demande_licence, terme, type_licence, type_demande, date_debut_validite, heure_debut, date_fin_validite, heure_fin) VALUES
    -- date_demande_licence DATE NOT NULL,
    -- date_debut_validite DATE NOT NULL,
    -- heure_debut TIME NOT NULL,
    -- date_fin_validite DATE NOT NULL,
    -- heure_fin TIME NOT NULL,
(nextval('demande_licence_seq'),
    (SELECT etablissement FROM etablissement WHERE raison_sociale='CAFE DU CENTRE'),
    '2018-05-01',
    (SELECT terme FROM terme WHERE code='PERM'),
    (SELECT type_licence FROM type_licence WHERE libelle='Licence de 3ème catégorie'),
    (SELECT type_demande FROM type_demande WHERE code='OUV'),
    '2018-05-15',
    '00:00:00',
    '2038-05-15',
    '23:59:59'
),
(nextval('demande_licence_seq'),
    (SELECT etablissement FROM etablissement WHERE raison_sociale='RESTAURANT CHEZ LUIGGI'),
    '2018-04-12',
    (SELECT terme FROM terme WHERE code='PERM'),
    (SELECT type_licence FROM type_licence WHERE libelle='Licence de 4ème catégorie'),
    (SELECT type_demande FROM type_demande WHERE code='OUV'),
    '2018-04-27',
    '00:00:00',
    '2038-04-27',
    '23:59:59'
),
(nextval('demande_licence_seq'),
    (SELECT etablissement FROM etablissement WHERE raison_sociale='LE RELAIS CORSE'),
    '2018-03-22',
    (SELECT terme FROM terme WHERE code='PERM'),
    (SELECT type_licence FROM type_licence WHERE libelle='Petite licence restaurant'),
    (SELECT type_demande FROM type_demande WHERE code='OUV'),
    '2018-04-05',
    '00:00:00',
    '2038-04-05',
    '23:59:59'
),
(nextval('demande_licence_seq'),
    (SELECT etablissement FROM etablissement WHERE raison_sociale='LA CAVE A WHISKY'),
    '2018-02-12',
    (SELECT terme FROM terme WHERE code='PERM'),
    (SELECT type_licence FROM type_licence WHERE libelle='Petite licence restaurant'),
    (SELECT type_demande FROM type_demande WHERE code='OUV'),
    '2018-02-27',
    '00:00:00',
    '2038-02-27',
    '23:59:59'
),
(nextval('demande_licence_seq'),
    (SELECT etablissement FROM etablissement WHERE raison_sociale='BAR DU MARCHE'),
    '2018-03-07',
    (SELECT terme FROM terme WHERE code='PERM'),
    (SELECT type_licence FROM type_licence WHERE libelle='Petite licence restaurant'),
    (SELECT type_demande FROM type_demande WHERE code='OUV'),
    '2018-03-22',
    '00:00:00',
    '2038-03-22',
    '23:59:59'
)
;
--
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES
(nextval('om_requete_seq'), 'aucune',   'Aucune REQUÊTE',   NULL,   'SELECT
    '''' as aucune_valeur

FROM
    &DB_PREFIXEom_collectivite',    '[aucune_valeur]',  'sql',  NULL,   NULL);
--
INSERT INTO om_etat (om_etat, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, se_font, se_couleurtexte, margeleft, margetop, margeright, margebottom, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES
(nextval('om_etat_seq'), (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),  'cerfa_declaration',    'CERFA de déclaration', 't',    'P',    'A4',   NULL,   10, 25, '<p>Texte du titre CERFA de déclaration</p>',    105,    25, 95, 10, '0',    '<p>Texte du corps CERFA de déclaration</p>',    (SELECT om_requete FROM om_requete WHERE code = 'aucune'),  'helvetica',    '0-0-0',    10, 25, 10, 25, '', 10, '<p style=''text-align: center; font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),
(nextval('om_etat_seq'), (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),  'recepisse',    'Récépissé',    't',    'P',    'A4',   NULL,   10, 25, '<p>Texte du titre Récépissé</p>',    105,    25, 95, 10, '0',    '<p>Texte du corps Récépissé</p>',    (SELECT om_requete FROM om_requete WHERE code = 'aucune'),  'helvetica',    '0-0-0',    10, 25, 10, 25, '', 10, '<p style=''text-align: center; font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),
(nextval('om_etat_seq'), (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),  'recepisse_co_exploitant',  'Récépissé du co-exploitant',   't',    'P',    'A4',   NULL,   10, 25, '<p>Texte du titre Récépissé du co-exploitant</p>',    105,    25, 95, 10, '0',    '<p>Texte du corps Récépissé du co-exploitant</p>',    (SELECT om_requete FROM om_requete WHERE code = 'aucune'),  'helvetica',    '0-0-0',    10, 25, 10, 25, '', 10, '<p style=''text-align: center; font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),
(nextval('om_etat_seq'), (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),  'demande_temp_reponse', 'Demande de licence temporaire avec réponse du Maire',  't',    'P',    'A4',   NULL,   10, 25, '<p>Texte du titre Demande de licence temporaire avec réponse du Maire</p>',    105,    25, 95, 10, '0',    '<p>Texte du corps Demande de licence temporaire avec réponse du Maire</p>',    1,  'helvetica',    '0-0-0',    10, 25, 10, 25, '', 10, '<p style=''text-align: center; font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></p>', 12);
--
\i tests/data/pgsql/adresse_postale_montreuil.sql
--
COMMIT;

